open Framework
open Glutil
open Tgl3

let defaults display_mode = display_mode
let vertex_positions = ba_float32_of_array [|
    0.25;  0.25; 0.0; 1.0;
    0.25; -0.25; 0.0; 1.0;
    -0.25; -0.25; 0.0; 1.0;
  |]

let initialize_vertex_buffer () =
  let position_buffer_object = get_int (Gl.gen_buffers 1) in
  let bytes = Gl.bigarray_byte_size vertex_positions in
  Gl.bind_buffer Gl.array_buffer position_buffer_object;
  (* FIXME: original has stream_draw here... *)
  Gl.buffer_data Gl.array_buffer bytes (Some vertex_positions) Gl.static_draw;
  Gl.bind_buffer Gl.array_buffer 0;
  position_buffer_object

let init () =
  let vao = get_int (Gl.gen_vertex_arrays 1) in
  Gl.bind_vertex_array vao;
  vao

class tutorial = object
  inherit framework defaults as framework
  val the_program =
    Framework.create_program [
      Framework.load_shader Gl.vertex_shader "03/calcOffset.vert";
      Framework.load_shader Gl.fragment_shader "03/calcColor.frag"
    ];

  val mutable elapsed_time_uniform = -1

  initializer
    elapsed_time_uniform <- Gl.get_uniform_location the_program "time";

    let loop_duration_unf = Gl.get_uniform_location the_program "loopDuration" in
    let frag_loop_duration_unf = Gl.get_uniform_location the_program "fragLoopDuration" in

    Gl.use_program the_program;
    Gl.uniform1f loop_duration_unf 5.0;
    Gl.uniform1f frag_loop_duration_unf 10.0;
    Gl.use_program 0

  val position_buffer_object = initialize_vertex_buffer ()
  val vao = init ()

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear Gl.color_buffer_bit;

    Gl.use_program the_program;

    Gl.uniform1f elapsed_time_uniform (framework#elapsed_time /. 1000.);

    Gl.bind_buffer Gl.array_buffer position_buffer_object;
    Gl.enable_vertex_attrib_array 0;
    Gl.vertex_attrib_pointer 0 4 Gl.float false 0 (`Offset 0);

    Gl.draw_arrays Gl.triangles 0 3;

    Gl.disable_vertex_attrib_array 0;
    Gl.use_program 0;
    framework#swap_buffers;
    framework#post_redisplay

  method reshape ~w ~h =
    Gl.viewport 0 0 w h

  method keyboard keycode =
    if keycode = 27 then framework#leave_main_loop
end
let () = Framework.run (new tutorial)
