open Framework
open Glutil
open Tgl3
open Gg

let vertex_positions = ba_float32_of_array [|
    0.25;  0.25; 0.0; 1.0;
    0.25; -0.25; 0.0; 1.0;
    -0.25; -0.25; 0.0; 1.0;
  |]

let defaults display_mode = display_mode
let initialize_vertex_buffer () =
  let position_buffer_object = get_int (Gl.gen_buffers 1) in
  let bytes = Gl.bigarray_byte_size vertex_positions in
  Gl.bind_buffer Gl.array_buffer position_buffer_object;
  Gl.buffer_data Gl.array_buffer bytes (Some vertex_positions) Gl.stream_draw;
  Gl.bind_buffer Gl.array_buffer 0;
  position_buffer_object

let init () =
  let vao = get_int (Gl.gen_vertex_arrays 1) in
  Gl.bind_vertex_array vao;
  vao

class tutorial = object(self)
  inherit framework defaults as framework
  val the_program =
    Framework.create_program [
      Framework.load_shader Gl.vertex_shader "03/standard.vert";
      Framework.load_shader Gl.fragment_shader "03/standard.frag"
    ]

  val position_buffer_object = initialize_vertex_buffer ()
  val vao = init ()

  method private compute_position_offsets =
    let loop_duration = 5. in
    let scale = Float.pi *. 2. /. loop_duration in
    let elapsed_time = framework#elapsed_time /. 1000. in
    let curr_time_through_loop = mod_float elapsed_time loop_duration in
    (cos (curr_time_through_loop *. scale)) *. 0.5,
    (sin (curr_time_through_loop *. scale)) *. 0.5

  method private adjust_vertex_data x_offset y_offset =
    let new_data = ba_copy vertex_positions in
    for i = 0 to (Bigarray.Array1.dim new_data) / 4 - 1 do
      new_data.{i * 4} <- new_data.{i*4} +. x_offset;
      new_data.{i * 4 + 1} <- new_data.{i*4 + 1} +. y_offset
    done;
    Gl.bind_buffer Gl.array_buffer position_buffer_object;
    let bytes = Gl.bigarray_byte_size new_data in
    Gl.buffer_sub_data Gl.array_buffer 0 bytes (Some new_data);
    Gl.bind_buffer Gl.array_buffer 0

  method display =
    let x_offset, y_offset = self#compute_position_offsets in
    self#adjust_vertex_data x_offset y_offset;
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear Gl.color_buffer_bit;

    Gl.use_program the_program;

    Gl.bind_buffer Gl.array_buffer position_buffer_object;
    Gl.enable_vertex_attrib_array 0;
    Gl.vertex_attrib_pointer 0 4 Gl.float false 0 (`Offset 0);

    Gl.draw_arrays Gl.triangles 0 3;

    Gl.disable_vertex_attrib_array 0;
    Gl.use_program 0;
    framework#swap_buffers;
    framework#post_redisplay

  method reshape ~w ~h =
    Gl.viewport 0 0 w h

  method keyboard keycode =
    if keycode = 27 then framework#leave_main_loop
end
let () = Framework.run (new tutorial)
