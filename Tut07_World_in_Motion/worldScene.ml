open Framework
open Glutil
open Tgl3
open Gg

type program_data = {
  the_program: int;
  model_to_world_matrix_unif: int;
  world_to_camera_matrix_unif: int;
  camera_to_clip_matrix_unif: int;
  base_color_unif: int
}

type tree_data = {
  x_pos: float;
  z_pos: float;
  trunk_height: float;
  cone_height: float
}

let tree (x_pos, z_pos, trunk_height, cone_height) =
  { x_pos; z_pos; trunk_height; cone_height }

let defaults display_mode = display_mode
let load_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  {
    the_program;
    model_to_world_matrix_unif =
      Gl.get_uniform_location the_program "modelToWorldMatrix";
    world_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "worldToCameraMatrix";
    camera_to_clip_matrix_unif =
      Gl.get_uniform_location the_program "cameraToClipMatrix";
    base_color_unif = Gl.get_uniform_location the_program "baseColor"
  }

class tutorial = object(self)
  inherit framework defaults as framework
  val z_near = 1.0
  val z_far = 1000.0

  val uniform_color =
    load_program "07/PosOnlyWorldTransform.vert" "07/ColorUniform.frag";

  val object_color =
    load_program "07/PosColorWorldTransform.vert" "07/ColorPassthrough.frag";

  val uniform_color_tint =
    load_program "07/PosColorWorldTransform.vert" "07/ColorMultUniform.frag";

  method private calc_look_at_matrix camera_pt look_pt up_pt =
    let look_dir = V3.unit (V3.sub look_pt camera_pt) in
    let up_dir = V3.unit up_pt in

    (* TODO: use M4.rot_map *)

    let right_dir = V3.unit (V3.cross look_dir up_dir) in
    let perp_up_dir = V3.cross right_dir look_dir in

    let rot_mat = M4.of_rows
        (V4.of_v3 right_dir ~w:0.)
        (V4.of_v3 perp_up_dir ~w:0.)
        (V4.of_v3 (V3.neg look_dir) ~w:0.)
        V4.ow
    in

    let trans_mat = M4.of_cols
        V4.ox
        V4.oy
        V4.oz
        (V4.of_v3 (V3.neg camera_pt) ~w:1.0)
    in

    M4.mul rot_mat trans_mat

  val cone_mesh = Framework.mesh "07/UnitConeTint.xml";
  val cylinder_mesh = Framework.mesh "07/UnitCylinderTint.xml"
  val cube_tint_mesh = Framework.mesh "07/UnitCubeTint.xml";
  val cube_color_mesh = Framework.mesh "07/UnitCubeColor.xml";
  val plane_mesh = Framework.mesh "07/UnitPlane.xml";

  initializer
    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range 0.0 1.0;
    Gl.enable Gl.depth_clamp

  val mutable y_angle = 0.0
  val mutable x_angle = 0.0

  method private render4 model_matrix mesh program r g b a =
    Gl.use_program program.the_program;
    Gl.uniform_matrix4fv program.model_to_world_matrix_unif 1 false
      (value_ptr_m4 model_matrix);
    Gl.uniform4f program.base_color_unif r g b a;
    mesh#render;
    Gl.use_program 0

  method private render model_matrix mesh program =
    Gl.use_program program.the_program;
    Gl.uniform_matrix4fv program.model_to_world_matrix_unif 1 false
      (value_ptr_m4 model_matrix);
    mesh#render;
    Gl.use_program 0

  (* Trees are 3x3 in X/Z, and trunk_height + cone_height in the Y *)
  method private draw_tree orig_model_matrix trunk_height cone_height =
    (* draw trunk *)
    let model_matrix = M4.mul orig_model_matrix (M4.mul
                                                   (M4.scale3 (V3.v 1.0 trunk_height 1.0))
                                                   (M4.move3 (V3.v 0.0 0.5 0.0))
                                                ) in
    self#render4 model_matrix cylinder_mesh uniform_color 0.694 0.4 0.106 1.0;

    (* draw the treetop *)
    let model_matrix = M4.mul orig_model_matrix (M4.mul
                                                   (M4.move3 (V3.v 0.0 trunk_height 0.0))
                                                   (M4.scale3 (V3.v 3.0 cone_height 3.0))
                                                ) in
    self#render4 model_matrix cone_mesh uniform_color_tint 0.0 1.0 0.0 1.0;

  val column_base_height = 0.25

  (* Columns are 1x1 in the X/Z, and height units in the Y *)
  method private draw_column orig_model_matrix height =
    (* draw the bottom of the column *)
    let model_matrix = M4.mul orig_model_matrix (M4.mul
                                                   (M4.scale3 (V3.v 1.0 column_base_height 1.0))
                                                   (M4.move3 (V3.v 0.0 0.5 0.0))
                                                ) in
    self#render4 model_matrix cube_tint_mesh uniform_color_tint 1.0 1.0 1.0 1.0;

    (* draw the top of the column *)
    let model_matrix = M4.mul orig_model_matrix (M4.mul (M4.mul
                                                           (M4.move3 (V3.v 0.0 (height -. column_base_height) 0.0))
                                                           (M4.scale3 (V3.v 1.0 column_base_height 1.0)))
                                                   (M4.move3 (V3.v 0.0 0.5 0.0))
                                                ) in
    self#render4 model_matrix cube_tint_mesh uniform_color_tint 0.9 0.9 0.9 0.9;

    (* draw the main column *)
    let model_matrix = M4.mul orig_model_matrix (M4.mul (M4.mul
                                                           (M4.move3 (V3.v 0.0 column_base_height 0.0))
                                                           (M4.scale3 (V3.v 0.8 (height -. column_base_height *. 2.0) 0.8)))
                                                   (M4.move3 (V3.v 0.0 0.5 0.0))
                                                ) in
    self#render4 model_matrix cylinder_mesh uniform_color_tint 0.9 0.9 0.9 0.9;

  val parthenon_width = 14.0
  val parthenon_length = 20.0
  val parthenon_column_height = 5.0
  val parthenon_base_height = 1.0
  val parthenon_top_height = 2.0

  method private draw_parthenon orig_model_matrix =
    (* draw base *)
    let model_matrix = M4.mul orig_model_matrix (M4.mul
                                                   (M4.scale3 (V3.v parthenon_width parthenon_base_height parthenon_length))
                                                   (M4.move3 (V3.v 0.0 0.5 0.0))
                                                ) in
    self#render4 model_matrix cube_tint_mesh uniform_color_tint 0.9 0.9 0.9 0.9;

    (* draw top *)
    let model_matrix = M4.mul orig_model_matrix (M4.mul (M4.mul
                                                           (M4.move3 (V3.v 0.0 (parthenon_column_height +. parthenon_base_height) 0.0))
                                                           (M4.scale3 (V3.v parthenon_width parthenon_top_height parthenon_length)))
                                                   (M4.move3 (V3.v 0.0 0.5 0.0))
                                                ) in
    self#render4 model_matrix cube_tint_mesh uniform_color_tint 0.9 0.9 0.9 0.9;

    (* Draw columns *)
    let front_z_val = parthenon_length /. 2.0 -. 1.0 in
    let right_x_val = parthenon_width /. 2.0 -. 1.0 in
    for column_num = 0 to int_of_float (parthenon_width /. 2.0) - 1 do
      let model_matrix = M4.mul orig_model_matrix (
          M4.move3 (V3.v
                      ((2.0 *. float column_num) -. parthenon_width /. 2.0 +. 1.0)
                      parthenon_base_height
                      front_z_val
                   )) in
      self#draw_column model_matrix parthenon_column_height;

      let model_matrix = M4.mul orig_model_matrix (
          M4.move3 (V3.v
                      ((2.0 *. float column_num) -. parthenon_width /. 2.0 +. 1.0)
                      parthenon_base_height
                      (-.front_z_val)
                   )) in
      self#draw_column model_matrix parthenon_column_height;
    done;

    (* Don't draw the first or last columns, since they've been drawn already *)
    for column_num = 1 to int_of_float ((parthenon_length -. 2.0) /. 2.0) -1 do
      let model_matrix = M4.mul orig_model_matrix (
          M4.move3 (V3.v
                      right_x_val
                      parthenon_base_height
                      ((2.0 *. float column_num) -. parthenon_length /. 2.0 +. 1.0)
                   )) in
      self#draw_column model_matrix parthenon_column_height;

      let model_matrix = M4.mul orig_model_matrix (
          M4.move3 (V3.v
                      (-.right_x_val)
                      parthenon_base_height
                      ((2.0 *. float column_num) -. parthenon_length /. 2.0 +. 1.0)
                   )) in
      self#draw_column model_matrix parthenon_column_height;
    done;

    (* Draw interior *)
    let model_matrix = M4.mul orig_model_matrix (M4.mul (M4.mul
                                                           (M4.move3 (V3.v 0.0 1.0 0.0))
                                                           (M4.scale3 (V3.v
                                                                         (parthenon_width -. 6.0)
                                                                         parthenon_column_height
                                                                         (parthenon_length -. 6.0))))
                                                   (M4.move3 (V3.v 0.0 0.5 0.0))
                                                ) in
    self#render model_matrix cube_color_mesh object_color;

    (* Draw headpiece *)
    let model_matrix = M4.mul orig_model_matrix (M4.mul
                                                   (M4.move3 (V3.v
                                                                0.0
                                                                (parthenon_column_height +. parthenon_base_height +.
                                                                 parthenon_top_height /. 2.0)
                                                                (parthenon_length /. 2.0)
                                                             ))
                                                   (M4.rot3_zyx (V3.v (Float.rad_of_deg (-135.0)) (Float.rad_of_deg 45.0) 0.0))
                                                ) in
    self#render model_matrix cube_color_mesh object_color

  val forest = [|
    tree (-45.0, -40.0, 2.0, 3.0);
    tree (-42.0, -35.0, 2.0, 3.0);
    tree (-39.0, -29.0, 2.0, 4.0);
    tree (-44.0, -26.0, 3.0, 3.0);
    tree (-40.0, -22.0, 2.0, 4.0);
    tree (-36.0, -15.0, 3.0, 3.0);
    tree (-41.0, -11.0, 2.0, 3.0);
    tree (-37.0, -6.0, 3.0, 3.0);
    tree (-45.0, 0.0, 2.0, 3.0);
    tree (-39.0, 4.0, 3.0, 4.0);
    tree (-36.0, 8.0, 2.0, 3.0);
    tree (-44.0, 13.0, 3.0, 3.0);
    tree (-42.0, 17.0, 2.0, 3.0);
    tree (-38.0, 23.0, 3.0, 4.0);
    tree (-41.0, 27.0, 2.0, 3.0);
    tree (-39.0, 32.0, 3.0, 3.0);
    tree (-44.0, 37.0, 3.0, 4.0);
    tree (-36.0, 42.0, 2.0, 3.0);

    tree (-32.0, -45.0, 2.0, 3.0);
    tree (-30.0, -42.0, 2.0, 4.0);
    tree (-34.0, -38.0, 3.0, 5.0);
    tree (-33.0, -35.0, 3.0, 4.0);
    tree (-29.0, -28.0, 2.0, 3.0);
    tree (-26.0, -25.0, 3.0, 5.0);
    tree (-35.0, -21.0, 3.0, 4.0);
    tree (-31.0, -17.0, 3.0, 3.0);
    tree (-28.0, -12.0, 2.0, 4.0);
    tree (-29.0, -7.0, 3.0, 3.0);
    tree (-26.0, -1.0, 2.0, 4.0);
    tree (-32.0, 6.0, 2.0, 3.0);
    tree (-30.0, 10.0, 3.0, 5.0);
    tree (-33.0, 14.0, 2.0, 4.0);
    tree (-35.0, 19.0, 3.0, 4.0);
    tree (-28.0, 22.0, 2.0, 3.0);
    tree (-33.0, 26.0, 3.0, 3.0);
    tree (-29.0, 31.0, 3.0, 4.0);
    tree (-32.0, 38.0, 2.0, 3.0);
    tree (-27.0, 41.0, 3.0, 4.0);
    tree (-31.0, 45.0, 2.0, 4.0);
    tree (-28.0, 48.0, 3.0, 5.0);

    tree (-25.0, -48.0, 2.0, 3.0);
    tree (-20.0, -42.0, 3.0, 4.0);
    tree (-22.0, -39.0, 2.0, 3.0);
    tree (-19.0, -34.0, 2.0, 3.0);
    tree (-23.0, -30.0, 3.0, 4.0);
    tree (-24.0, -24.0, 2.0, 3.0);
    tree (-16.0, -21.0, 2.0, 3.0);
    tree (-17.0, -17.0, 3.0, 3.0);
    tree (-25.0, -13.0, 2.0, 4.0);
    tree (-23.0, -8.0, 2.0, 3.0);
    tree (-17.0, -2.0, 3.0, 3.0);
    tree (-16.0, 1.0, 2.0, 3.0);
    tree (-19.0, 4.0, 3.0, 3.0);
    tree (-22.0, 8.0, 2.0, 4.0);
    tree (-21.0, 14.0, 2.0, 3.0);
    tree (-16.0, 19.0, 2.0, 3.0);
    tree (-23.0, 24.0, 3.0, 3.0);
    tree (-18.0, 28.0, 2.0, 4.0);
    tree (-24.0, 31.0, 2.0, 3.0);
    tree (-20.0, 36.0, 2.0, 3.0);
    tree (-22.0, 41.0, 3.0, 3.0);
    tree (-21.0, 45.0, 2.0, 3.0);

    tree (-12.0, -40.0, 2.0, 4.0);
    tree (-11.0, -35.0, 3.0, 3.0);
    tree (-10.0, -29.0, 1.0, 3.0);
    tree (-9.0, -26.0, 2.0, 2.0);
    tree (-6.0, -22.0, 2.0, 3.0);
    tree (-15.0, -15.0, 1.0, 3.0);
    tree (-8.0, -11.0, 2.0, 3.0);
    tree (-14.0, -6.0, 2.0, 4.0);
    tree (-12.0, 0.0, 2.0, 3.0);
    tree (-7.0, 4.0, 2.0, 2.0);
    tree (-13.0, 8.0, 2.0, 2.0);
    tree (-9.0, 13.0, 1.0, 3.0);
    tree (-13.0, 17.0, 3.0, 4.0);
    tree (-6.0, 23.0, 2.0, 3.0);
    tree (-12.0, 27.0, 1.0, 2.0);
    tree (-8.0, 32.0, 2.0, 3.0);
    tree (-10.0, 37.0, 3.0, 3.0);
    tree (-11.0, 42.0, 2.0, 2.0);


    tree (15.0, 5.0, 2.0, 3.0);
    tree (15.0, 10.0, 2.0, 3.0);
    tree (15.0, 15.0, 2.0, 3.0);
    tree (15.0, 20.0, 2.0, 3.0);
    tree (15.0, 25.0, 2.0, 3.0);
    tree (15.0, 30.0, 2.0, 3.0);
    tree (15.0, 35.0, 2.0, 3.0);
    tree (15.0, 40.0, 2.0, 3.0);
    tree (15.0, 45.0, 2.0, 3.0);

    tree (25.0, 5.0, 2.0, 3.0);
    tree (25.0, 10.0, 2.0, 3.0);
    tree (25.0, 15.0, 2.0, 3.0);
    tree (25.0, 20.0, 2.0, 3.0);
    tree (25.0, 25.0, 2.0, 3.0);
    tree (25.0, 30.0, 2.0, 3.0);
    tree (25.0, 35.0, 2.0, 3.0);
    tree (25.0, 40.0, 2.0, 3.0);
    tree (25.0, 45.0, 2.0, 3.0);
  |]

  method draw_forest orig_model_matrix =
    Array.iter (fun curr_tree ->
        let model_matrix = M4.mul orig_model_matrix (
            M4.move3 (V3.v curr_tree.x_pos 0.0 curr_tree.z_pos)
          ) in
        self#draw_tree model_matrix curr_tree.trunk_height curr_tree.cone_height
      ) forest

  val mutable draw_lookat_point = false
  val mutable cam_target = V3.v 0.0 0.4 0.0

  (* in spherical coordinates *)
  val mutable sphere_cam_rel_pos = V3.v 67.5 (-46.0) 150.0

  method private resolve_cam_position =
    let phi = Float.rad_of_deg (V3.x sphere_cam_rel_pos) in
    let theta = Float.rad_of_deg (V3.y sphere_cam_rel_pos +. 90.0) in

    let sin_theta = sin theta in
    let cos_theta = cos theta in
    let cos_phi = cos phi in
    let sin_phi = sin phi in

    let dir_to_camera =
      V3.v (sin_theta *. cos_phi) cos_theta (sin_theta *. sin_phi) in
    V3.add
      (V3.smul (V3.z sphere_cam_rel_pos) dir_to_camera)
      cam_target

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    let cam_pos = self#resolve_cam_position in
    let cam_matrix = self#calc_look_at_matrix cam_pos cam_target (V3.v 0.0 1.0 0.0)
    in
    Gl.use_program uniform_color.the_program;
    Gl.uniform_matrix4fv uniform_color.world_to_camera_matrix_unif 1 false
      (value_ptr_m4 cam_matrix);
    Gl.use_program object_color.the_program;
    Gl.uniform_matrix4fv object_color.world_to_camera_matrix_unif 1 false
      (value_ptr_m4 cam_matrix);
    Gl.use_program uniform_color_tint.the_program;
    Gl.uniform_matrix4fv uniform_color_tint.world_to_camera_matrix_unif 1 false
      (value_ptr_m4 cam_matrix);
    Gl.use_program 0;

    (* Render the ground plane *)
    let model_matrix = M4.scale3 (V3.v 100.0 1.0 100.0) in
    self#render4 model_matrix plane_mesh uniform_color 0.302 0.416 0.0589 1.0;

    (* Draw the trees *)
    self#draw_forest M4.id;

    (* Draw the building *)
    let model_matrix = M4.move3 (V3.v 20.0 0.0 (-10.0)) in
    self#draw_parthenon model_matrix;

    if draw_lookat_point then begin
      Gl.disable Gl.depth_test;
      let camera_aim_vec = V3.sub cam_target cam_pos in

      let model_matrix = M4.mul
          (M4.move3 (V3.v 0.0 0.0 (-.(V3.norm camera_aim_vec))))
          (M4.scale3 (V3.v 1.0 1.0 1.0)) in (* noop scale? *)

      Gl.use_program object_color.the_program;
      Gl.uniform_matrix4fv object_color.model_to_world_matrix_unif 1 false
        (value_ptr_m4 model_matrix);
      Gl.uniform_matrix4fv object_color.world_to_camera_matrix_unif 1 false
        (value_ptr_m4 M4.id);
      cube_color_mesh#render;
      Gl.use_program 0;
      Gl.enable Gl.depth_test
    end;

    framework#swap_buffers

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 45.) ~near:z_near ~far:z_far ~aspect in

    Gl.use_program uniform_color.the_program;
    Gl.uniform_matrix4fv uniform_color.camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 camera_to_clip_matrix);
    Gl.use_program object_color.the_program;
    Gl.uniform_matrix4fv object_color.camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 camera_to_clip_matrix);
    Gl.use_program uniform_color_tint.the_program;
    Gl.uniform_matrix4fv uniform_color_tint.camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 camera_to_clip_matrix);
    Gl.use_program 0;
    Gl.viewport 0 0 w h

  method private handle_keycode = function
    | 'w' -> cam_target <- V3.sub cam_target (V3.v 0.0 0.0 4.0)
    | 's' -> cam_target <- V3.add cam_target (V3.v 0.0 0.0 4.0)
    | 'd' -> cam_target <- V3.sub cam_target (V3.v 4.0 0.0 0.0)
    | 'a' -> cam_target <- V3.add cam_target (V3.v 4.0 0.0 0.0)
    | 'e' -> cam_target <- V3.sub cam_target (V3.v 0.0 4.0 0.0)
    | 'q' -> cam_target <- V3.add cam_target (V3.v 0.0 4.0 0.0)
    | 'W' -> cam_target <- V3.sub cam_target (V3.v 0.0 0.0 0.4)
    | 'S' -> cam_target <- V3.add cam_target (V3.v 0.0 0.0 0.4)
    | 'D' -> cam_target <- V3.sub cam_target (V3.v 0.4 0.0 0.0)
    | 'A' -> cam_target <- V3.add cam_target (V3.v 0.4 0.0 0.0)
    | 'E' -> cam_target <- V3.sub cam_target (V3.v 0.0 0.4 0.0)
    | 'Q' -> cam_target <- V3.add cam_target (V3.v 0.0 0.4 0.0)
    | 'i' ->
      sphere_cam_rel_pos <- V3.sub sphere_cam_rel_pos (V3.v 0. 11.25 0.0)
    | 'k' ->
      sphere_cam_rel_pos <- V3.add sphere_cam_rel_pos (V3.v 0. 11.25 0.0)
    | 'j' ->
      sphere_cam_rel_pos <- V3.sub sphere_cam_rel_pos (V3.v 11.25 0.0 0.0)
    | 'l' ->
      sphere_cam_rel_pos <- V3.add sphere_cam_rel_pos (V3.v 11.25 0.0 0.0)
    | 'o' ->
      sphere_cam_rel_pos <- V3.sub sphere_cam_rel_pos (V3.v 0.0 0.0 5.0)
    | 'u' ->
      sphere_cam_rel_pos <- V3.add sphere_cam_rel_pos (V3.v 0.0 0.0 5.0)
    | 'I' ->
      sphere_cam_rel_pos <- V3.sub sphere_cam_rel_pos (V3.v 0. 1.125 0.0)
    | 'K' ->
      sphere_cam_rel_pos <- V3.add sphere_cam_rel_pos (V3.v 0. 1.125 0.0)
    | 'J' ->
      sphere_cam_rel_pos <- V3.sub sphere_cam_rel_pos (V3.v 1.125 0.0 0.0)
    | 'L' ->
      sphere_cam_rel_pos <- V3.add sphere_cam_rel_pos (V3.v 1.125 0.0 0.0)
    | 'O' ->
      sphere_cam_rel_pos <- V3.sub sphere_cam_rel_pos (V3.v 0.0 0.0 0.5)
    | 'U' ->
      sphere_cam_rel_pos <- V3.add sphere_cam_rel_pos (V3.v 0.0 0.0 0.5)
    | ' ' ->
      draw_lookat_point <- not draw_lookat_point;
      Format.printf "@[<v>Target: %a@,Position:%a@]@."
        V3.pp cam_target V3.pp sphere_cam_rel_pos
    | _ -> ()

  method keyboard keycode =
    if keycode = 27 then begin
      cone_mesh#delete;
      cylinder_mesh#delete;
      cube_tint_mesh#delete;
      cube_color_mesh#delete;
      plane_mesh#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then self#handle_keycode (Char.chr keycode);

    sphere_cam_rel_pos <- V3.v
        (V3.x sphere_cam_rel_pos)
        (Float.clamp ~min:(-78.75) ~max:(-1.0) (V3.y sphere_cam_rel_pos))
        (max (V3.z sphere_cam_rel_pos) 5.0);
    cam_target <- V3.v
        (V3.x cam_target)
        (max (V3.y cam_target) 0.0)
        (V3.z cam_target);
    framework#post_redisplay
end
let () = Framework.run (new tutorial)
