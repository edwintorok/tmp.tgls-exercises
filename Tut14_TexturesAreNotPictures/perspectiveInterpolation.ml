open Framework
open Glutil
open Tgl3
open Gg

type program_data = {
  the_program: int;
  camera_to_clip_matrix_unif: int;
}

let z_near = 1.0
let z_far = 1000.0

let sizeof_float = 4

let defaults display_mode = display_mode
let load_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  {
    the_program;
    camera_to_clip_matrix_unif =
      Gl.get_uniform_location the_program "cameraToClipMatrix";
  }

class tutorial = object
  inherit framework defaults as framework
  val smooth_interp =
    load_program "14/SmoothVertexColors.vert" "14/SmoothVertexColors.frag"
  val linear_interp =
    load_program "14/NoCorrectVertexColors.vert" "14/NoCorrectVertexColors.frag"

  val pers_matrix = persp_fov ~fov:(Float.rad_of_deg 60.0) ~near:z_near ~far:z_far ~aspect:1.0
  val mutable real_hallway = Framework.mesh "14/RealHallway.xml"
  val mutable faux_hallway = Framework.mesh "14/FauxHallway.xml"
  initializer
    Gl.use_program smooth_interp.the_program;
    Gl.uniform_matrix4fv smooth_interp.camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 pers_matrix);
    Gl.use_program linear_interp.the_program;
    Gl.uniform_matrix4fv linear_interp.camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 pers_matrix);
    Gl.use_program 0

  val mutable use_fake_hallway = false
  val mutable use_smooth_interpolation = true

  method display =
    Gl.clear_color 0.0 0.0 0.0 0.0;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    if use_smooth_interpolation then
      Gl.use_program smooth_interp.the_program
    else
      Gl.use_program linear_interp.the_program;

    if use_fake_hallway then
      faux_hallway#render
    else
      real_hallway#render;

    Gl.use_program 0;
    framework#swap_buffers

  method reshape ~w ~h =
    Gl.viewport 0 0 w h;
    framework#post_redisplay

  method keyboard keycode =
    if keycode = 27 then begin
      real_hallway#delete;
      faux_hallway#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then begin match Char.chr keycode with
      | 's' | 'S' ->
        use_fake_hallway <- not use_fake_hallway;
        if use_fake_hallway then
          print_endline "Fake Hallway"
        else
          print_endline "Real Hallway"
      | 'p' | 'P' ->
        use_smooth_interpolation <- not use_smooth_interpolation;
        if use_smooth_interpolation then
          print_endline "Perspective correct interpolation"
        else
          print_endline "Just linear interpolation"
      | ' ' ->
        (* Reload *)
        real_hallway#delete;
        faux_hallway#delete;
        real_hallway <- Framework.mesh "14/RealHallway.xml";
        faux_hallway <- Framework.mesh "14/FauxHallway.xml"
      | _ -> ()
    end;
    framework#post_redisplay
end
let () = Framework.run (new tutorial)
