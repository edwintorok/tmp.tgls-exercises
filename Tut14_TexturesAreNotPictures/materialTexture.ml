open Framework
open Glutil
open Tgl3
open Gg
open MousePoles

type program_data = {
  the_program: int;
  model_to_camera_matrix_unif: int;
  normal_model_to_camera_matrix_unif: int;
}

type unlit_prog_data = {
  the_program: int;
  object_color_unif: int;
  model_to_camera_matrix_unif: int;
}

let z_near = 1.0
let z_far = 1000.0

type shader_mode = MODE_FIXED | MODE_TEXTURED | MODE_TEXTURED_COMPUTE
type shader_pairs = {
  vert_shader: string;
  frag_shader: string;
}

let shader_pairs = function
  | MODE_FIXED ->
    { vert_shader = "14/PN.vert"; frag_shader = "14/FixedShininess.frag" }
  | MODE_TEXTURED ->
    { vert_shader = "14/PNT.vert"; frag_shader = "14/TextureShininess.frag" }
  | MODE_TEXTURED_COMPUTE ->
    { vert_shader = "14/PNT.vert"; frag_shader = "14/TextureCompute.frag" }

let initial_view_data = {
  target_pos = V3.v 0.0 0.5 0.0;
  orient = Quat.v 0.3826834 0.0 0.0 0.92387953;
  radius = 10.0;
  deg_spin_rotation = 0.0;
}

let view_scale = {
  min_radius = 1.5; max_radius = 70.0;
  large_radius_delta = 1.5; small_radius_delta = 0.5;
  large_pos_offset = 0.0; small_pos_offset = 0.0; (* no camera movement *)
  rotation_scale = 90.0 /. 250.0
}

let initial_object_data = {
  position = V3.v 0.0 0.5 0.0;
  orientation = Quat.v 0.0 0.0 0.0 1.0
}

let vec4_write v4 =
  let x,y,z,w = V4.to_tuple v4 in
  Ctypes.CArray.of_list Ctypes.float [x;y;z;w]

let vec4 = Ctypes.(view (array 4 float)
                     ~write:vec4_write ~read:(fun _ -> assert false))

let sizeof_float = 4
let ba_of_struct s =
  let sptr = Ctypes.addr s in
  let styp = Ctypes.reference_type sptr in
  let n = Ctypes.sizeof styp / sizeof_float in
  let floatptr = Ctypes.(coerce (ptr styp) (ptr float) sptr) in
  Ctypes.bigarray_of_ptr Ctypes.array1 n Bigarray.float32 floatptr

type per_light
let per_light : per_light Ctypes.structure Ctypes.typ = Ctypes.structure "PerLight"
let camera_space_light_pos = Ctypes.field per_light "cameraSpaceLightPos" vec4
let light_intensity = Ctypes.field per_light "lightIntensity" vec4
let () = Ctypes.seal per_light

let number_of_lights = 2

type light_block
let light_block : light_block Ctypes.structure Ctypes.typ = Ctypes.structure "LightBlock"
let ambient_intensity = Ctypes.field light_block "ambientIntensity" vec4
let light_attenuation = Ctypes.(field light_block "lightAttenuation" float)
let _padding = Ctypes.(field light_block "padding" (array 3 float))
let lights = Ctypes.(field light_block "lights" (array number_of_lights
                                                   per_light))
let () = Ctypes.seal light_block

type material_block
let material_block : material_block Ctypes.structure Ctypes.typ = Ctypes.structure "MaterialBlock"
let diffuse_color = Ctypes.field material_block "diffuseColor" vec4
let specular_color = Ctypes.field material_block "specularColor" vec4
let specular_shininess = Ctypes.(field material_block "specularShininess" float)
let padding = Ctypes.(field material_block "padding" (array 3 float))
let () = Ctypes.seal material_block

let defaults display_mode = display_mode
let material_block_index = 0
let light_block_index = 1
let projection_block_index = 2
let gauss_tex_unit = 0
let shine_tex_unit = 1
let load_standard_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let material_block = Gl.get_uniform_block_index the_program "Material" in
  let light_block = Gl.get_uniform_block_index the_program "Light" in
  let projection_block = Gl.get_uniform_block_index the_program "Projection"
  in
  Gl.uniform_block_binding the_program material_block material_block_index;
  Gl.uniform_block_binding the_program light_block light_block_index;
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  let gaussian_texture_unif =
    Gl.get_uniform_location the_program "gaussianTexture" in
  let shininess_texture_unif =
    Gl.get_uniform_location the_program "shininessTexture" in
  Gl.use_program the_program;
  Gl.uniform1i gaussian_texture_unif gauss_tex_unit;
  Gl.uniform1i shininess_texture_unif shine_tex_unit;
  Gl.use_program 0;
  {
    the_program;
    model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "modelToCameraMatrix";
    normal_model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "normalModelToCameraMatrix";
  }

let load_unlit_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let projection_block = Gl.get_uniform_block_index the_program "Projection" in
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  {
    the_program;
    model_to_camera_matrix_unif = Gl.get_uniform_location the_program
        "modelToCameraMatrix";
    object_color_unif = Gl.get_uniform_location the_program "objectColor";
  }

let num_gauss_textures = 4
let half_light_distance = 25.0
let g_light_attenuation = 1.0 /. (half_light_distance *. half_light_distance)

let load_program mode =
  let pair = (shader_pairs mode) in
  load_standard_program pair.vert_shader pair.frag_shader

class tutorial = object(self)
  inherit framework defaults as framework
  val programs =
    let fixed = load_program MODE_FIXED
    and textured = load_program MODE_TEXTURED
    and compute = load_program MODE_TEXTURED_COMPUTE in
    function
    | MODE_FIXED -> fixed
    | MODE_TEXTURED -> textured
    | MODE_TEXTURED_COMPUTE -> compute

  val unlit =
    load_unlit_program "14/Unlit.vert" "14/Unlit.frag"

  val lit_shader_prog =
    load_standard_program "14/PN.vert" "14/ShaderGaussian.frag";
  val lit_texture_prog =
    load_standard_program "14/PN.vert" "14/TextureGaussian.frag"
  val view_pole = new view_pole initial_view_data view_scale ~action_button:MB_LEFT_BTN
  val objt_pole = new object_pole initial_object_data (90.0/.250.0) ~action_button:MB_RIGHT_BTN None

  method! mouse_motion x y =
    framework#forward_mouse_motion (view_pole :> mouse_pole) x y;
    framework#forward_mouse_motion (objt_pole :> mouse_pole) x y;
    framework#post_redisplay

  method! mouse_button button state x y =
    framework#forward_mouse_button (view_pole :> mouse_pole) button state x y;
    framework#forward_mouse_button (objt_pole :> mouse_pole) button state x y;
    framework#post_redisplay

  method! mouse_wheel wheel direction x y =
    framework#forward_mouse_wheel (view_pole :> mouse_pole) wheel direction x y;
    framework#forward_mouse_wheel (objt_pole :> mouse_pole) wheel direction x y;
    framework#post_redisplay

  method private build_gaussian_data cos_angle_resolution shininess_resolution =
    let ba = Bigarray.(Array1.create int8_unsigned c_layout
                         (cos_angle_resolution * shininess_resolution)) in
    for shin = 1 to shininess_resolution do
      let shininess = float shin /. float shininess_resolution in
      for i = 0 to cos_angle_resolution - 1 do
        let cos_ang = float i /. (float (cos_angle_resolution - 1)) in
        let angle = acos cos_ang in
        let exponent = angle /. shininess in
        let exponent = -.(exponent *. exponent) in
        let gaussian_term = exp exponent in
        ba.{i} <- int_of_float (gaussian_term *. 255.)
      done;
    done;
    ba

  method private create_gaussian_texture cos_angle_resolution shininess_resolution =
    let texture_data = self#build_gaussian_data cos_angle_resolution shininess_resolution in
    let gauss_texture = get_int (Gl.gen_textures 1) in
    Gl.bind_texture Gl.texture_2d gauss_texture;
    Gl.tex_image2d Gl.texture_2d 0 Gl.r8 cos_angle_resolution
      shininess_resolution 0 Gl.red Gl.unsigned_byte (`Data texture_data);
    Gl.tex_parameteri Gl.texture_2d Gl.texture_base_level 0;
    Gl.tex_parameteri Gl.texture_2d Gl.texture_max_level 0;
    Gl.bind_texture Gl.texture_2d 0;
    gauss_texture

  method private calc_cos_angle_resolution level =
    let cos_angle_start = 64.0 in
    int_of_float (cos_angle_start *. (2.0 ** (float level)))

  val gauss_textures = Array.init num_gauss_textures (fun _ -> -1)
  val mutable texture_sampler = -1

  method private create_gaussian_textures =
    for loop = 0 to num_gauss_textures-1 do
      let cos_angle_resolution = self#calc_cos_angle_resolution loop in
      gauss_textures.(loop) <- self#create_gaussian_texture cos_angle_resolution 128
    done;
    texture_sampler <- get_int (Gl.gen_samplers 1);
    Gl.sampler_parameteri texture_sampler Gl.texture_mag_filter Gl.nearest;
    Gl.sampler_parameteri texture_sampler Gl.texture_min_filter Gl.nearest;
    Gl.sampler_parameteri texture_sampler Gl.texture_wrap_s Gl.clamp_to_edge;
    Gl.sampler_parameteri texture_sampler Gl.texture_wrap_t Gl.clamp_to_edge

  val mutable shine_texture = 0
  method private create_shininess_texture =
    let filename = File.find_or_throw "main.dds" in
    let imageset = failwith "TODO" in
    let image = imageset#get_image 0 0 0 in
    let dims = image#get_dimensions in
    shine_texture <- get_int (Gl.gen_textures 1);
    Gl.bind_texture Gl.texture_2d shine_texture;
    Gl.tex_image2d Gl.texture_2d 0 Gl.r8
      (Size2.w dims |> int_of_float) (Size2.h dims |> int_of_float) 0
      Gl.red Gl.unsigned_byte image#get_image_data;
    Gl.tex_parameteri Gl.texture_2d Gl.texture_base_level 0;
    Gl.tex_parameteri Gl.texture_2d Gl.texture_base_level 0;
    Gl.bind_texture Gl.texture_2d 0

  val object_mesh = Framework.mesh "14/Infinity.xml"
  val cube_mesh = Framework.mesh "14/UnitCube.xml"
  val plane_mesh = Framework.mesh "14/UnitPlane.xml"

  val mutable material_offset = 0
  val mutable material_uniform_buffer = -1
  val mutable light_uniform_buffer = -1
  val mutable projection_uniform_buffer = -1
  val sizeof_projection_block = 16 * sizeof_float
  val g_specular_shininess = 0.2

  initializer
    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    let depth_z_near = 0.0
    and depth_z_far = 1.0 in

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range depth_z_near depth_z_far;
    Gl.enable Gl.depth_clamp;

    let mtl = Ctypes.make material_block in
    Ctypes.setf mtl diffuse_color (V4.v 1.0 0.673 0.043 1.0);
    Ctypes.setf mtl specular_color (V4.smul 0.4 (V4.v 1.0 0.673 0.043 1.0));
    Ctypes.setf mtl specular_shininess g_specular_shininess;

    material_uniform_buffer <- get_int (Gl.gen_buffers 1);
    Gl.bind_buffer Gl.uniform_buffer material_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer (Ctypes.sizeof material_block)
      (Some (ba_of_struct mtl)) Gl.static_draw;

    light_uniform_buffer <- get_int (Gl.gen_buffers 1);
    Gl.bind_buffer Gl.uniform_buffer light_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer (Ctypes.sizeof light_block)
      None Gl.dynamic_draw;

    projection_uniform_buffer <- get_int (Gl.gen_buffers 1);
    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer sizeof_projection_block None
      Gl.dynamic_draw;

    Gl.bind_buffer_range Gl.uniform_buffer light_block_index
      light_uniform_buffer 0 (Ctypes.sizeof light_block);

    Gl.bind_buffer_range Gl.uniform_buffer projection_block_index
      projection_uniform_buffer 0 sizeof_projection_block;

    Gl.bind_buffer_range Gl.uniform_buffer material_block_index
      material_uniform_buffer 0  (Ctypes.sizeof material_block);

    Gl.bind_buffer Gl.uniform_buffer 0;
    self#create_gaussian_textures

  val mutable draw_camera_pos = false
  val mutable draw_lights = true
  val mutable use_texture = false
  val mutable curr_texture = 0

  val light_timer = new Framework.timer Framework.TT_LOOP 6.0

  val light_height = 1.0
  val light_radius = 3.0

  method private calc_light_position =
    let scale = Float.pi *. 2.0 in
    let time_through_loop = light_timer#get_alpha in
    V4.v
      ((cos (time_through_loop *. scale)) *. light_radius)
      light_height
      ((sin (time_through_loop *. scale)) *. light_radius)
      1.0

  val light_data = Ctypes.make light_block

  method display =
    ignore (light_timer#update);
    Gl.clear_color 0.75 0.75 1.0 1.0;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    let model_matrix = view_pole#calc_matrix in
    let world_to_cam_mat = model_matrix in

    Ctypes.setf light_data ambient_intensity (V4.v 0.2 0.2 0.2 1.0);
    Ctypes.setf light_data light_attenuation g_light_attenuation;

    let global_light_direction = V3.v 0.707 0.707 0.0 in
    let lights = Ctypes.getf light_data lights in
    let lights0 = Ctypes.CArray.get lights 0 in
    Ctypes.setf lights0 camera_space_light_pos (V4.ltr world_to_cam_mat
                                                  (V4.of_v3 global_light_direction ~w:0.0));
    Ctypes.setf lights0 light_intensity (V4.v 0.6 0.6 0.6 1.0);

    let lights1 = Ctypes.CArray.get lights 1 in
    Ctypes.setf lights1 camera_space_light_pos (V4.ltr world_to_cam_mat
                                                  self#calc_light_position);
    Ctypes.setf lights1 light_intensity (V4.v 0.4 0.4 0.4 1.0);

    Gl.bind_buffer Gl.uniform_buffer light_uniform_buffer;
    Gl.buffer_sub_data Gl.uniform_buffer 0 (Ctypes.sizeof light_block)
      (Some (ba_of_struct light_data));
    Gl.bind_buffer Gl.uniform_buffer 0;


    begin
      Gl.bind_buffer_range Gl.uniform_buffer material_block_index
        material_uniform_buffer 0 (Ctypes.sizeof material_block);
      let model_matrix = M4.mul (M4.mul model_matrix objt_pole#calc_matrix)
          (M4.scale3 (V3.v 2.0 2.0 2.0)) in
      let norm_matrix = M3.transpose (M3.inv (M3.of_m4 model_matrix)) in
      let prog = if use_texture then lit_texture_prog else lit_shader_prog in
      Gl.use_program prog.the_program;
      Gl.uniform_matrix4fv prog.model_to_camera_matrix_unif 1 false
        (value_ptr_m4 model_matrix);
      Gl.uniform_matrix3fv prog.normal_model_to_camera_matrix_unif 1 false
        (value_ptr_m3 norm_matrix);

      Gl.active_texture (Gl.texture0 + gauss_tex_unit);
      Gl.bind_texture Gl.texture_2d gauss_textures.(curr_texture);
      Gl.bind_sampler gauss_tex_unit texture_sampler;

      object_mesh#render_mesh "lit";

      Gl.bind_sampler gauss_tex_unit 0;
      Gl.bind_texture Gl.texture_1d 0;
      Gl.use_program 0;
      Gl.bind_buffer_base Gl.uniform_buffer material_block_index 0;
    end;

    if draw_lights then begin
      begin
        let model_matrix = M4.mul (M4.mul model_matrix
                                     (M4.move3 (V3.of_v4 self#calc_light_position)))
            (M4.scale3 (V3.v 0.25 0.25 0.25)) in
        Gl.use_program unlit.the_program;
        Gl.uniform_matrix4fv unlit.model_to_camera_matrix_unif 1 false
          (value_ptr_m4 model_matrix);

        let light_color = V4.v 1.0 1.0 1.0 1.0 in
        Gl.uniform4fv unlit.object_color_unif 1 (value_ptr_v4 light_color);
        cube_mesh#render_mesh "flat";
      end;
      let model_matrix = M4.mul (M4.mul model_matrix
                                   (M4.move3 (V3.smul 100.0 global_light_direction)))
          (M4.scale3 (V3.v 5.0 5.0 5.0)) in
      Gl.uniform_matrix4fv unlit.model_to_camera_matrix_unif 1 false
        (value_ptr_m4 model_matrix);
      cube_mesh#render_mesh "flat";
      Gl.use_program 0;
    end;

    if draw_camera_pos then begin
      let model_matrix = M4.mul
          (M4.move3 (V3.v 0.0 0.0 (-.view_pole#get_view.radius)))
          (M4.scale3 (V3.v 0.25 0.25 0.25)) in
      Gl.disable Gl.depth_test;
      Gl.depth_mask false;
      Gl.use_program unlit.the_program;
      Gl.uniform_matrix4fv unlit.model_to_camera_matrix_unif 1 false
        (value_ptr_m4 model_matrix);
      Gl.uniform4f unlit.object_color_unif 0.25 0.25 0.25 1.0;
      cube_mesh#render_mesh "flat";
      Gl.depth_mask true;
      Gl.enable Gl.depth_test;
      Gl.uniform4f unlit.object_color_unif 1.0 1.0 1.0 1.0;
      cube_mesh#render_mesh "flat"
    end;
    framework#post_redisplay;
    framework#swap_buffers

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 45.0) ~near:z_near ~far:z_far ~aspect in

    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_sub_data Gl.uniform_buffer 0 sizeof_projection_block
      (Some (value_ptr_m4 camera_to_clip_matrix));
    Gl.bind_buffer Gl.uniform_buffer 0;
    Gl.viewport 0 0 w h;
    framework#post_redisplay

  method keyboard keycode =
    if keycode = 27 then begin
      object_mesh#delete;
      cube_mesh#delete;
      plane_mesh#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then begin match Char.chr keycode with
      | 'p' -> ignore (light_timer#toggle_pause)
      | '-' -> light_timer#rewind 0.5
      | '=' -> light_timer#fast_forward 0.5
      | 't' -> draw_camera_pos <- not draw_camera_pos
      | 'g' -> draw_lights <- not draw_lights
      | ' ' ->
        use_texture <- not use_texture;
        if use_texture then
          print_endline "Texture"
        else
          print_endline "Shader"
      | c ->
        if c >= '1' && c <= '9' then begin
          let number = Char.code c - (Char.code '1') in
          if number < num_gauss_textures then begin
            Printf.printf "Angle resolution: %i\n%!"
              (self#calc_cos_angle_resolution number);
            curr_texture <- number
          end
        end
    end;
    framework#post_redisplay
end
let () = Framework.run (new tutorial)
