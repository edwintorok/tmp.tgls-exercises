open Framework
open Glutil
open Tgl3

let vertex_data = ba_float32_of_array [|
    0.25;  0.25; -1.25; 1.0;
    0.25; -0.25; -1.25; 1.0;
    -0.25;  0.25; -1.25; 1.0;

    0.25; -0.25; -1.25; 1.0;
    -0.25; -0.25; -1.25; 1.0;
    -0.25;  0.25; -1.25; 1.0;

    0.25;  0.25; -2.75; 1.0;
    -0.25;  0.25; -2.75; 1.0;
    0.25; -0.25; -2.75; 1.0;

    0.25; -0.25; -2.75; 1.0;
    -0.25;  0.25; -2.75; 1.0;
    -0.25; -0.25; -2.75; 1.0;

    -0.25;  0.25; -1.25; 1.0;
    -0.25; -0.25; -1.25; 1.0;
    -0.25; -0.25; -2.75; 1.0;

    -0.25;  0.25; -1.25; 1.0;
    -0.25; -0.25; -2.75; 1.0;
    -0.25;  0.25; -2.75; 1.0;

    0.25;  0.25; -1.25; 1.0;
    0.25; -0.25; -2.75; 1.0;
    0.25; -0.25; -1.25; 1.0;

    0.25;  0.25; -1.25; 1.0;
    0.25;  0.25; -2.75; 1.0;
    0.25; -0.25; -2.75; 1.0;

    0.25;  0.25; -2.75; 1.0;
    0.25;  0.25; -1.25; 1.0;
    -0.25;  0.25; -1.25; 1.0;

    0.25;  0.25; -2.75; 1.0;
    -0.25;  0.25; -1.25; 1.0;
    -0.25;  0.25; -2.75; 1.0;

    0.25; -0.25; -2.75; 1.0;
    -0.25; -0.25; -1.25; 1.0;
    0.25; -0.25; -1.25; 1.0;

    0.25; -0.25; -2.75; 1.0;
    -0.25; -0.25; -2.75; 1.0;
    -0.25; -0.25; -1.25; 1.0;




    0.0; 0.0; 1.0; 1.0;
    0.0; 0.0; 1.0; 1.0;
    0.0; 0.0; 1.0; 1.0;

    0.0; 0.0; 1.0; 1.0;
    0.0; 0.0; 1.0; 1.0;
    0.0; 0.0; 1.0; 1.0;

    0.8; 0.8; 0.8; 1.0;
    0.8; 0.8; 0.8; 1.0;
    0.8; 0.8; 0.8; 1.0;

    0.8; 0.8; 0.8; 1.0;
    0.8; 0.8; 0.8; 1.0;
    0.8; 0.8; 0.8; 1.0;

    0.0; 1.0; 0.0; 1.0;
    0.0; 1.0; 0.0; 1.0;
    0.0; 1.0; 0.0; 1.0;

    0.0; 1.0; 0.0; 1.0;
    0.0; 1.0; 0.0; 1.0;
    0.0; 1.0; 0.0; 1.0;

    0.5; 0.5; 0.0; 1.0;
    0.5; 0.5; 0.0; 1.0;
    0.5; 0.5; 0.0; 1.0;

    0.5; 0.5; 0.0; 1.0;
    0.5; 0.5; 0.0; 1.0;
    0.5; 0.5; 0.0; 1.0;

    1.0; 0.0; 0.0; 1.0;
    1.0; 0.0; 0.0; 1.0;
    1.0; 0.0; 0.0; 1.0;

    1.0; 0.0; 0.0; 1.0;
    1.0; 0.0; 0.0; 1.0;
    1.0; 0.0; 0.0; 1.0;

    0.0; 1.0; 1.0; 1.0;
    0.0; 1.0; 1.0; 1.0;
    0.0; 1.0; 1.0; 1.0;

    0.0; 1.0; 1.0; 1.0;
    0.0; 1.0; 1.0; 1.0;
    0.0; 1.0; 1.0; 1.0;

  |]

let initialize_vertex_buffer () =
  let vertex_buffer_object = get_int (Gl.gen_buffers 1) in
  let bytes = Gl.bigarray_byte_size vertex_data in
  Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
  Gl.buffer_data Gl.array_buffer bytes (Some vertex_data) Gl.static_draw;
  Gl.bind_buffer Gl.array_buffer 0;
  vertex_buffer_object

let defaults display_mode = display_mode
let init () =
  let vao = get_int (Gl.gen_vertex_arrays 1) in
  Gl.bind_vertex_array vao;
  vao

class tutorial = object
  inherit framework defaults as framework
  val the_program =
    Framework.create_program [
      Framework.load_shader Gl.vertex_shader "04/MatrixPerspective.vert";
      Framework.load_shader Gl.fragment_shader "04/StandardColors.frag"
    ];

  val mutable offset_uniform = -1
  val mutable perspective_matrix_unif = -1
  val perspective_matrix = Bigarray.(Array1.create float32 c_layout 16)
  val frustum_scale = 1.0

  initializer
    offset_uniform <- Gl.get_uniform_location the_program "offset";
    perspective_matrix_unif <- Gl.get_uniform_location the_program "perspective_matrix";

    let z_near = 0.5 and z_far = 3.0 in
    Bigarray.Array1.fill perspective_matrix 0.;
    perspective_matrix.{0} <- frustum_scale;
    perspective_matrix.{5} <- frustum_scale;
    perspective_matrix.{10} <- (z_far +. z_near) /. (z_near -. z_far);
    perspective_matrix.{14} <- (2.0 *. z_far *. z_near) /. (z_near -. z_far);
    perspective_matrix.{11} <- -1.0;

    Gl.use_program the_program;
    Gl.uniform_matrix4fv perspective_matrix_unif 1 false perspective_matrix;
    Gl.use_program 0;

    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw

  val vertex_buffer_object = initialize_vertex_buffer ()
  val vao = init ()

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear Gl.color_buffer_bit;

    Gl.use_program the_program;

    Gl.uniform2f offset_uniform 1.5 0.5;

    let color_data = Gl.bigarray_byte_size vertex_data / 2 in
    Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
    Gl.enable_vertex_attrib_array 0;
    Gl.enable_vertex_attrib_array 1;
    Gl.vertex_attrib_pointer 0 4 Gl.float false 0 (`Offset 0);
    Gl.vertex_attrib_pointer 1 4 Gl.float false 0 (`Offset color_data);

    Gl.draw_arrays Gl.triangles 0 36;

    Gl.disable_vertex_attrib_array 0;
    Gl.disable_vertex_attrib_array 1;
    Gl.use_program 0;
    framework#swap_buffers;
    framework#post_redisplay

  method reshape ~w ~h =
    perspective_matrix.{0} <- frustum_scale /. (float w /. float h);
    perspective_matrix.{5} <- frustum_scale;

    Gl.use_program the_program;
    Gl.uniform_matrix4fv perspective_matrix_unif 1 false perspective_matrix;
    Gl.viewport 0 0 w h

  method keyboard keycode =
    if keycode = 27 then framework#leave_main_loop
end
let () = Framework.run (new tutorial)
