open Tgl3
open Framework
open Glutil

let create_shader shader_type shader_file =
  let shader = Gl.create_shader shader_type in
  Gl.shader_source shader shader_file;
  Gl.compile_shader shader;
  if get_int (Gl.get_shaderiv shader Gl.compile_status) = Gl.false_ then begin
    let len = get_int (Gl.get_shaderiv shader Gl.info_log_length) in
    let info_log = get_string len (Gl.get_shader_info_log shader len None) in
    let shader_type =
      if shader_type = Gl.vertex_shader then "vertex"
      else if shader_type = Gl.geometry_shader then "geometry"
      else if shader_type = Gl.fragment_shader then "fragment"
      else "??" in
    Gl.delete_shader shader;
    failwith (Printf.sprintf "Compile failure in %s shader: %s" shader_type info_log);
  end;
  shader

let create_program shader_list =
  let program = Gl.create_program () in
  List.iter (fun shader ->
      Gl.attach_shader program shader
    ) shader_list;
  Gl.link_program program;
  if (get_int (Gl.get_programiv program Gl.link_status)) = Gl.false_ then begin
    let len = get_int (Gl.get_programiv program Gl.info_log_length) in
    let info_log = get_string len (Gl.get_program_info_log program len None) in
    Gl.delete_program program;
    failwith (Printf.sprintf "Linker failure: %s" info_log)
  end;
  List.iter (fun shader ->
      Gl.detach_shader program shader
    ) shader_list;
  program

let defaults display_mode = display_mode

  (*
  let vertex_shader =
    "#version 330\n\
     	layout(location = 0) in vec4 position;\n\
     	void main()\n\
     	{\n\
     	   gl_Position = position;\n\
     	}\n"

  let fragment_shader =
    "#version 330\n\
     	out vec4 outputColor;\n\
     	void main()\n\
     	{\n\
     	   outputColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);\n\
     	}\n"
      *)

let vertex_shader =
  "#version 140\n\
   #extension GL_ARB_explicit_attrib_location: require\n\
   	layout(location = 0) in vec4 position;\n\
   	void main()\n\
   	{\n\
   	   gl_Position = position;\n\
   	}\n"

let fragment_shader =
  "#version 140\n\
   	out vec4 outputColor;\n\
   	void main()\n\
   	{\n\
   	   outputColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);\n\
   	}\n"

let initialize_program () =
  let shader_list = [
    create_shader Gl.vertex_shader vertex_shader;
    create_shader Gl.fragment_shader fragment_shader
  ] in
  let the_program = create_program shader_list in
  List.iter Gl.delete_shader shader_list;
  the_program

let vertex_positions = ba_float32_of_array [|
    0.75;  0.75; 0.; 1.;
    0.75; -0.75; 0.; 1.;
    -0.75; -0.75; 0.; 1.
  |]

let initialize_vertex_buffer () =
  let position_buffer_object = get_int (Gl.gen_buffers 1) in
  let bytes = Gl.bigarray_byte_size vertex_positions in
  Gl.bind_buffer Gl.array_buffer position_buffer_object;
  Gl.buffer_data Gl.array_buffer bytes (Some vertex_positions) Gl.static_draw;
  Gl.bind_buffer Gl.array_buffer 0;
  position_buffer_object

let init () =
  let vao = get_int (Gl.gen_vertex_arrays 1) in
  Gl.bind_vertex_array vao;
  vao

class tutorial = object
  inherit framework defaults as framework

  val the_program = initialize_program ()

  val position_buffer_object = initialize_vertex_buffer ()
  val vao = init ()

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear Gl.color_buffer_bit;
    Gl.use_program the_program;

    Gl.bind_buffer Gl.array_buffer position_buffer_object;
    Gl.enable_vertex_attrib_array 0;
    Gl.vertex_attrib_pointer 0 4 Gl.float false 0 (`Offset 0);

    Gl.draw_arrays Gl.triangles 0 3;

    Gl.disable_vertex_attrib_array 0;
    Gl.use_program 0;
    framework#swap_buffers

  method reshape ~w ~h =
    Gl.viewport 0 0 w h

  method keyboard keycode =
    if keycode = 27 then begin
      (*      Gl.delete_program the_program;
              Gl.bind_vertex_array 0;
              Gl.bind_buffer Gl.array_buffer 0;
              set_int (Gl.delete_buffers 1) position_buffer_object;
              set_int (Gl.delete_vertex_arrays 1) vao;*)
      framework#leave_main_loop
    end
end

let () = Framework.run (new tutorial)
