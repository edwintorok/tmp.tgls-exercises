open Framework
open Glutil
open Tgl3

let number_of_vertices = 36
let right_extent =  0.8
let left_extent = -.right_extent
let top_extent = 0.2
let middle_extent = 0.0
let bottom_extent = -.top_extent
let front_extent = -1.25
let rear_extent = -1.75

let green_color = [| 0.75; 0.75; 1.0; 1.0 |]
let blue_color = [| 0.0; 0.5; 0.0; 1.0 |]
let red_color = [| 1.0; 0.0; 0.0; 1.0 |]
let grey_color = [| 0.8; 0.8; 0.8; 1.0 |]
let brown_color = [| 0.5; 0.5; 0.0; 1.0 |]

let defaults display_mode = display_mode
class tutorial = object(self)
  inherit framework defaults as framework
  val the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader "05/Standard.vert";
      Framework.load_shader Gl.fragment_shader "05/Standard.frag"
    ];
  val mutable offset_uniform = -1

  val mutable perspective_matrix_unif = -1
  val perspective_matrix = Bigarray.(Array1.create float32 c_layout 16)
  val frustum_scale = 1.0

  method private initialize_program =
    offset_uniform <- Gl.get_uniform_location the_program "offset";
    perspective_matrix_unif <- Gl.get_uniform_location the_program "perspective_matrix";

    let z_near = 1.0 and z_far = 3.0 in
    Bigarray.Array1.fill perspective_matrix 0.;
    perspective_matrix.{0} <- frustum_scale;
    perspective_matrix.{5} <- frustum_scale;
    perspective_matrix.{10} <- (z_far +. z_near) /. (z_near -. z_far);
    perspective_matrix.{14} <- (2.0 *. z_far *. z_near) /. (z_near -. z_far);
    perspective_matrix.{11} <- -1.0;

    Gl.use_program the_program;
    Gl.uniform_matrix4fv perspective_matrix_unif 1 false perspective_matrix;
    Gl.use_program 0

  val vertex_data = ba_float32_of_array [|
      (* object 1 positions *)
      left_extent; top_extent; rear_extent;
      left_extent; middle_extent; front_extent;
      right_extent; middle_extent; front_extent;
      right_extent; top_extent; rear_extent;

      left_extent; bottom_extent; rear_extent;
      left_extent; middle_extent; front_extent;
      right_extent; middle_extent; front_extent;
      right_extent; bottom_extent; rear_extent;

      left_extent; top_extent; rear_extent;
      left_extent; middle_extent; front_extent;
      right_extent; bottom_extent; rear_extent;

      right_extent; top_extent; rear_extent;
      right_extent; middle_extent; front_extent;
      right_extent; bottom_extent; rear_extent;

      left_extent; bottom_extent; rear_extent;
      left_extent; middle_extent; rear_extent;
      right_extent; middle_extent; rear_extent;
      right_extent; bottom_extent; rear_extent;

      (* 0; 2; 1;
       * 3; 2; 0; *)

      (* object 2 positions *)
      top_extent; right_extent; rear_extent;
      middle_extent; right_extent; front_extent;
      middle_extent; left_extent; front_extent;
      top_extent; left_extent; rear_extent;

      bottom_extent; right_extent; rear_extent;
      middle_extent; right_extent; front_extent;
      middle_extent; left_extent; front_extent;
      bottom_extent; left_extent; rear_extent;

      top_extent; right_extent; rear_extent;
      middle_extent; right_extent; front_extent;
      bottom_extent; right_extent; rear_extent;

      top_extent; left_extent; rear_extent;
      middle_extent; left_extent; front_extent;
      bottom_extent; left_extent; rear_extent;

      bottom_extent; right_extent; rear_extent;
      top_extent; right_extent; rear_extent;
      top_extent; left_extent; rear_extent;
      bottom_extent; left_extent; rear_extent;

      (* object 1 colors *)
      (* green *)
      0.75; 0.75; 1.0; 1.0;
      0.75; 0.75; 1.0; 1.0;
      0.75; 0.75; 1.0; 1.0;
      0.75; 0.75; 1.0; 1.0;

      (* blue *)
      0.0; 0.5; 0.0; 1.0;
      0.0; 0.5; 0.0; 1.0;
      0.0; 0.5; 0.0; 1.0;
      0.0; 0.5; 0.0; 1.0;

      (* red *)
      1.0; 0.0; 0.0; 1.0;
      1.0; 0.0; 0.0; 1.0;
      1.0; 0.0; 0.0; 1.0;

      (* grey *)
      0.8; 0.8; 0.8; 1.0;
      0.8; 0.8; 0.8; 1.0;
      0.8; 0.8; 0.8; 1.0;

      (* brown *)
      0.5; 0.5; 0.0; 1.0;
      0.5; 0.5; 0.0; 1.0;
      0.5; 0.5; 0.0; 1.0;
      0.5; 0.5; 0.0; 1.0;

      (* object 2 colors *)
      (* red *)
      1.0; 0.0; 0.0; 1.0;
      1.0; 0.0; 0.0; 1.0;
      1.0; 0.0; 0.0; 1.0;
      1.0; 0.0; 0.0; 1.0;

      (* brown *)
      0.5; 0.5; 0.0; 1.0;
      0.5; 0.5; 0.0; 1.0;
      0.5; 0.5; 0.0; 1.0;
      0.5; 0.5; 0.0; 1.0;

      (* blue *)
      0.0; 0.5; 0.0; 1.0;
      0.0; 0.5; 0.0; 1.0;
      0.0; 0.5; 0.0; 1.0;

      (* green *)
      0.75; 0.75; 1.0; 1.0;
      0.75; 0.75; 1.0; 1.0;
      0.75; 0.75; 1.0; 1.0;

      (* grey *)
      0.8; 0.8; 0.8; 1.0;
      0.8; 0.8; 0.8; 1.0;
      0.8; 0.8; 0.8; 1.0;
      0.8; 0.8; 0.8; 1.0
    |]

  val index_data = ba_ushort_of_array [|
      0; 2; 1;
      3; 2; 0;

      4; 5; 6;
      6; 7; 4;

      8; 9; 10;
      11; 13; 12;

      14; 16; 15;
      17; 16; 14
    |]

  val mutable vertex_buffer_object = -1
  val mutable index_buffer_object = -1
  val mutable vao = -1

  method private initialize_vertex_buffer =
    vertex_buffer_object <- get_int (Gl.gen_buffers 1);
    let bytes = Gl.bigarray_byte_size vertex_data in
    Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
    Gl.buffer_data Gl.array_buffer bytes (Some vertex_data) Gl.static_draw;
    Gl.bind_buffer Gl.array_buffer 0;

    index_buffer_object <- get_int (Gl.gen_buffers 1);
    let bytes = Gl.bigarray_byte_size index_data in
    Gl.bind_buffer Gl.element_array_buffer index_buffer_object;
    Gl.buffer_data Gl.element_array_buffer bytes (Some index_data) Gl.static_draw;
    Gl.bind_buffer Gl.element_array_buffer 0

  val sizeof_float = 4

  (* called after the window and OpenGL are initialized. Called exactly onece,
   * before the main loop. *)
  initializer
    self#initialize_program;
    self#initialize_vertex_buffer;

    vao <- get_int (Gl.gen_vertex_arrays 1);
    Gl.bind_vertex_array vao;
    let color_data_offset = sizeof_float * 3 * number_of_vertices in
    Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
    Gl.enable_vertex_attrib_array 0;
    Gl.enable_vertex_attrib_array 1;
    Gl.vertex_attrib_pointer 0 3 Gl.float false 0 (`Offset 0);
    Gl.vertex_attrib_pointer 1 4 Gl.float false 0 (`Offset color_data_offset);
    Gl.bind_buffer Gl.element_array_buffer index_buffer_object;
    Gl.bind_vertex_array 0;

    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.less;
    Gl.depth_range 0.0 1.0

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    Gl.use_program the_program;

    Gl.bind_vertex_array vao;
    Gl.uniform3f offset_uniform 0. 0. 0.5;
    Gl.draw_elements Gl.triangles
      (Bigarray.Array1.dim index_data) Gl.unsigned_short (`Offset 0);

    Gl.uniform3f offset_uniform 0. 0. (-1.);
    Gl.draw_elements_base_vertex Gl.triangles (Bigarray.Array1.dim index_data)
      Gl.unsigned_short (`Offset 0)(number_of_vertices / 2);

    Gl.bind_vertex_array 0;

    Gl.disable_vertex_attrib_array 0;
    Gl.disable_vertex_attrib_array 1;
    Gl.use_program 0;
    framework#swap_buffers;
    framework#post_redisplay

  method reshape ~w ~h =
    perspective_matrix.{0} <- frustum_scale /. (float h /. float w);
    perspective_matrix.{5} <- frustum_scale;

    Gl.use_program the_program;
    Gl.uniform_matrix4fv perspective_matrix_unif 1 false perspective_matrix;
    Gl.viewport 0 0 w h

  val mutable depth_clamping_active = false
  method keyboard = function
    | 27 -> framework#leave_main_loop
    | 32 ->
      if depth_clamping_active then
        Gl.disable Gl.depth_clamp
      else
        Gl.enable Gl.depth_clamp;
      depth_clamping_active <- not depth_clamping_active;
      framework#post_redisplay
    | _ -> ()
end
let () = Framework.run (new tutorial)
