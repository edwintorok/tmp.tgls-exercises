open Tgl3.Gl
val debug_output_synchronous_arb : enum
val debug_next_logged_message_length_arb : enum
val debug_callback_function_arb : enum
val debug_callback_user_param_arb : enum
val debug_source_api_arb : enum
val debug_source_window_system_arb : enum
val debug_source_shader_compiler_arb : enum
val debug_source_third_party_arb : enum
val debug_source_application_arb : enum
val debug_source_other_arb : enum
val debug_type_error_arb : enum
val debug_type_deprecated_behavior_arb : enum
val debug_type_undefined_behavior_arb : enum
val debug_type_portability_arb : enum
val debug_type_performance_arb : enum
val debug_type_other_arb : enum
val max_debug_message_length_arb : enum
val max_debug_logged_messages_arb : enum
val debug_logged_messages_arb : enum
val debug_severity_high_arb : enum
val debug_severity_medium_arb : enum
val debug_severity_low_arb : enum
type debug_proc = enum -> enum -> int -> enum -> string -> unit
val debug_message_callback_arb :
  (enum -> enum -> enum -> enum -> string -> unit) -> unit
val debug_message_control_arb :
  enum -> enum -> enum -> int -> uint32_bigarray option -> bool -> unit
val debug_message_insert_arb :
  enum -> enum -> enum -> enum -> int -> string -> unit
val arb_debug_output_extension_supported : unit -> bool
