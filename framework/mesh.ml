open Tgl3
open Ezxmlm

type base_render_cmd = {
  prim_type: Gl.enum;
  start: int;
  elem_count: int;
}

type render_cmd = Arrays of base_render_cmd | IndexedCmd of base_render_cmd * Gl.enum
let render_cmd = function
  | IndexedCmd (cmd, index_data_type) ->
    Gl.draw_elements cmd.prim_type cmd.elem_count index_data_type (`Offset cmd.start)
  | Arrays cmd ->
    Gl.draw_arrays cmd.prim_type cmd.start cmd.elem_count

(* subset of Gg.Raster.buffer *)
type buffer = Gg.Ba.Buffer.t

type attrib = {
  normalized: bool;
  buf: buffer
}
let buffer_size attrib = Gg.Ba.Buffer.byte_length attrib.buf

let buffer_dim attrib = Gg.Ba.Buffer.length attrib.buf

let fill_bound_buffer_object buffer offset attrib =
  match attrib.buf with
  | `Float32 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)
  | `Float16 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)
  | `Int32 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)
  | `UInt32 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)
  | `Int16 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)
  | `UInt16 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)
  | `Int8 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)
  | `UInt8 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)
  | `Int64 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)
  | `UInt64 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)
  | `Float64 ba ->
    Gl.buffer_sub_data buffer offset (Gl.bigarray_byte_size ba) (Some ba)

let parse_float ba i str =
  try ba.{i} <- float_of_string str
  with Failure _ ->
    failwith (Printf.sprintf "Parse error in array data stream at '%s'" str)

let parse_int ba i str =
  try ba.{i} <- int_of_string str
  with Failure _ ->
    failwith (Printf.sprintf "Parse error in array data stream at '%s'" str)

let parse_int32 ba i str =
  try ba.{i} <- Int32.of_string str
  with Failure _ ->
    failwith (Printf.sprintf "Parse error in array data stream at '%s'" str)

let parse_generic f ctype values =
  let ba = Bigarray.(Array1.create ctype c_layout (Array.length values)) in
  Array.iteri (f ba) values;
  ba

let attrib_type_of_string values = function
  | "float" -> {
      normalized = false;
      buf = `Float32 (parse_generic parse_float Bigarray.float32 values)
    }
  | "half" ->
    failwith "TODO"
  | "int" -> {
      normalized = false;
      buf = `Int32 (parse_generic parse_int32 Bigarray.int32 values)
    }
  | "uint" -> {
      normalized = false;
      buf = `UInt32 (parse_generic parse_int32 Bigarray.int32 values)
    }
  | "norm-int" -> {
      normalized = true;
      buf = `Int32 (parse_generic parse_int32 Bigarray.int32 values)
    }
  | "norm-uint" -> {
      normalized = true;
      buf = `UInt32 (parse_generic parse_int32 Bigarray.int32 values)
    }
  | "short" -> {
      normalized = false;
      buf = `Int16 (parse_generic parse_int Bigarray.int16_signed values)
    }
  | "ushort" -> {
      normalized = false;
      buf = `UInt16 (parse_generic parse_int Bigarray.int16_unsigned values)
    }
  | "norm-short" -> {
      normalized = true;
      buf = `Int16 (parse_generic parse_int Bigarray.int16_signed values)
    }
  | "norm-ushort" -> {
      normalized = true;
      buf = `UInt16 (parse_generic parse_int Bigarray.int16_unsigned values)
    }
  | "byte" -> {
      normalized = false;
      buf = `Int8 (parse_generic parse_int Bigarray.int8_signed values)
    }
  | "ubyte" -> {
      normalized = false;
      buf = `UInt8 (parse_generic parse_int Bigarray.int8_unsigned values)
    }
  | "norm-byte" -> {
      normalized = true;
      buf = `Int8 (parse_generic parse_int Bigarray.int8_signed values)
    }
  | "norm-ubyte" -> {
      normalized = true;
      buf = `UInt8 (parse_generic parse_int Bigarray.int8_unsigned values)
    }
  | s ->
    failwith (Printf.sprintf "Unknown 'type' field: '%s'" s)

let gl_type_of_buf = function
  | `Float64 _-> failwith "TODO"
  | `Float32 _ -> Gl.float
  | `Float16 _ -> failwith "TODO"
  | `Int64 _ -> failwith "TODO"
  | `UInt64 _ -> failwith "TODO"
  | `Int32 _ -> Gl.int
  | `UInt32 _ -> Gl.unsigned_int
  | `Int16 _ -> Gl.short
  | `UInt16 _ -> Gl.unsigned_short
  | `Int8 _ -> Gl.byte
  | `UInt8 _ -> Gl.unsigned_byte

let gl_primitive_type_of_string = function
  | "triangles" -> Gl.triangles
  | "tri-strip" -> Gl.triangle_strip
  | "tri-fan" -> Gl.triangle_fan
  | "lines" -> Gl.lines
  | "line-strip" -> Gl.line_strip
  | "line-loop" -> Gl.line_loop
  | "points" -> Gl.points
  | s ->
    failwith (Printf.sprintf "Unknown 'cmd' field: %s" s)

let get_attrib_string name attrs =
  try get_attr name attrs
  with Not_found ->
    failwith (Printf.sprintf "Attribute '%s' not found" name)

let get_attrib_int name attrs =
  let v = get_attrib_string name attrs in
  try int_of_string v
  with Failure _ ->
    failwith (Printf.sprintf "Invalid integer attribute value: '%s'" v)

let get_attrib_bool name attrs =
  try
    let v = get_attr name attrs in
    try bool_of_string v
    with Failure _ ->
      failwith (Printf.sprintf "Invalid boolean attribute value: '%s'" v)
  with Not_found -> false

let re_space = Str.regexp "[ \t\r\n]+"
let rec split_values accum = function
  | `El _ :: tl -> split_values accum tl
  | `Data str :: tl ->
    split_values (List.rev_append (Str.split re_space str) accum) tl
  | [] -> Array.of_list (List.rev accum)


type attrib_data = {
  attribute_index: int;
  num_elements: int;
  vector_size: int;
  integral_attrib: bool;
  attrib: attrib;
}

let attribute_of_xml (attrs, children) =
  let attribute_index = get_attrib_int "index" attrs in
  if not (0 <= attribute_index && attribute_index < 16) then
    failwith (Printf.sprintf
                "Attribute index must be between 0 and 16: %d" attribute_index);
  let vector_size = get_attrib_int "size" attrs in
  if not (1 <= vector_size && vector_size < 5) then
    failwith (Printf.sprintf
                "Attribute size must be between 1 and 4: %d" vector_size);
  let elements = split_values [] children in
  let attrib_type = attrib_type_of_string elements (get_attrib_string "type" attrs)
  in
  let integral_attrib = get_attrib_bool "integral" attrs in
  if integral_attrib then begin
    if attrib_type.normalized then
      failwith "Attribute cannot be both 'integral' and a normalized 'type'";
    match attrib_type.buf with
    | `Float32 _ | `Float16 _ | `Float64 _ ->
      failwith "Attribute cannot be both 'integral' and a floating-point 'type'";
    | _ -> ()
  end;
  if Array.length elements = 0 then
    failwith "The attribute must have an array of values";
  if (Array.length elements mod vector_size) <> 0 then
    failwith "The attribute's data must be a multiple of its size in elements";
  {
    attribute_index;
    vector_size;
    num_elements = (Array.length elements) / vector_size;
    integral_attrib;
    attrib = attrib_type
  }

let setup_attribute_array a offset =
  Gl.enable_vertex_attrib_array a.attribute_index;
  if a.integral_attrib then
    Gl.vertex_attrib_ipointer a.attribute_index a.vector_size
      (gl_type_of_buf a.attrib.buf) 0 (`Offset offset)
  else
    Gl.vertex_attrib_pointer a.attribute_index a.vector_size
      (gl_type_of_buf a.attrib.buf) a.attrib.normalized 0 (`Offset offset)

let process_vao (vao_attrs, vao_children) =
  let name = get_attrib_string "name" vao_attrs in
  let attributes = List.fold_left (fun accum (source_attrs, _) ->
      get_attrib_int "attrib" source_attrs :: accum
    ) [] (members_with_attr "source" vao_children) in
  name, List.rev attributes

let index_data (index_elem_attrs, children) =
  let typ = get_attrib_string "type" index_elem_attrs in
  if typ <> "uint" && typ <> "ushort" && typ <> "ubyte" then
    failwith (Printf.sprintf "Improper 'type' attribute value on 'index' element: %s" typ);
  let elements = split_values [] children in
  if Array.length elements = 0 then
    failwith "The index element must have an array of values";
  attrib_type_of_string elements typ

let process_render_cmd = function
  | `Data _ -> assert false
  | `El (((_,elem_name),attrs),_children) ->
    let cmd_name = get_attrib_string "cmd" attrs in
    let prim_type = gl_primitive_type_of_string cmd_name in
    match elem_name with
    | "indices" ->
      IndexedCmd ({ prim_type; start = 0; elem_count = 0 }, Gl.invalid_enum)
    | "arrays" ->
      let start = get_attrib_int "start" attrs in
      if start < 0 then
        failwith "`array` 'start' index must be >= 0";
      let elem_count = get_attrib_int "count" attrs in
      if elem_count <= 0 then
        failwith "`array` 'count' must be > 0";
      Arrays { prim_type; start; elem_count }
    | _ ->
      failwith (Printf.sprintf "Bad command element '%s'. Must be 'indices' or 'arrays'" elem_name)

(* TODO: use with_resource *)
let from_file file =
  let ch = open_in file in
  let xml = from_channel ch in
  close_in ch;
  xml

module StringMap = Map.Make(String)

let load_mesh filename =
  let path = File.find_or_throw filename in
  let _, xml = from_file path in
  let root =
    try member "mesh" xml
    with Tag_not_found _ ->
      failwith (Printf.sprintf "`mesh` node not found in mesh file '%s'" path)
  in

  let attributes = members_with_attr "attribute" root in
  let dummy = `Float32 (Glutil.ba_create 16) in
  let attrib_index_map = Array.init 16 (fun _ -> {
        attribute_index = -1; vector_size = 0; integral_attrib = false;
        num_elements = 0; attrib = { normalized = false; buf = dummy }}) in
  List.iter (fun node ->
      let attrib = attribute_of_xml node in
      attrib_index_map.(attrib.attribute_index) <- attrib
    ) attributes;

  let vaos = members_with_attr "vao" root in
  let named_vao_list = List.fold_left (fun accum vao ->
      let name, value = process_vao vao in
      StringMap.add name value accum
    ) StringMap.empty vaos in
  let render_cmds = List.rev (List.fold_left (fun accum -> function
      | `El (((_,"attribute"),_),_) | `El (((_,"vao"),_),_) -> accum
      | `El _ as node -> process_render_cmd node :: accum
      | `Data _ -> accum
    ) [] root) in
  let index_data = Array.of_list (List.rev (List.fold_left (fun accum node ->
      index_data node :: accum
    ) [] (members_with_attr "indices" root))) in

  let attrib_start_locs = Array.init 16 (fun _ -> 0) in
  let attrb_buffer_size, _num_elements, _ =
    Array.fold_left (fun (attrb_buffer_size, num_elements, i) a ->
        if a.num_elements > 0 then
          let attrb_buffer_size =
            if attrb_buffer_size mod 16 <> 0 then
              attrb_buffer_size + (16 - attrb_buffer_size mod 16)
            else
              attrb_buffer_size in
          attrib_start_locs.(i) <- attrb_buffer_size;
          let buf_size = buffer_size a.attrib in
          if num_elements > 0 && num_elements <> a.num_elements then
            failwith (Printf.sprintf
                        "Some of the attribute arrays have different element counts: %d != %d" num_elements a.num_elements)
          else
            attrb_buffer_size + buf_size, a.num_elements, i+1
        else
          attrb_buffer_size, num_elements, i+1
      ) (0, 0, 0) attrib_index_map in

  (* Create the 'Everything' VAO *)
  let vao = Glutil.get_int (Gl.gen_vertex_arrays 1) in
  Gl.bind_vertex_array vao;

  (* Create the buffer object *)
  let attrib_arrays_buffer = Glutil.get_int (Gl.gen_buffers 1) in
  Gl.bind_buffer Gl.array_buffer attrib_arrays_buffer;
  Gl.buffer_data Gl.array_buffer attrb_buffer_size None Gl.static_draw;

  (* Fill in our data and set up the attribute arrays *)
  Array.iteri (fun i a ->
      if a.num_elements > 0 then begin
        let offset = attrib_start_locs.(i) in
        fill_bound_buffer_object Gl.array_buffer offset a.attrib;
        setup_attribute_array a offset
      end
    ) attrib_index_map;

  (* Fill the named VAOs *)
  let named_vaos = StringMap.fold (fun name attributes accum ->
      let vao = Glutil.get_int (Gl.gen_vertex_arrays 1) in
      Gl.bind_vertex_array vao;
      List.iter (fun attribute_index ->
          let attrib = attrib_index_map.(attribute_index) in
          if attrib.num_elements > 0 then
            setup_attribute_array attrib
              attrib_start_locs.(attribute_index)
        ) attributes;
      StringMap.add name vao accum
    ) named_vao_list StringMap.empty in

  Gl.bind_vertex_array 0;

  (* Get the size of our index buffer data *)
  let index_start_locs = Array.init 16 (fun _ -> 0) in
  let index_buffer_size, _ =
    Array.fold_left (fun (index_buffer_size, i) a ->
        let index_buffer_size =
          if index_buffer_size mod 16 <> 0 then
            index_buffer_size + (16 - index_buffer_size mod 16)
          else
            index_buffer_size in
        let buf_size = buffer_size a in
        index_start_locs.(i) <- index_buffer_size;
        index_buffer_size + buf_size, i+1
      ) (0, 0) index_data in
  (* Create the index buffer object *)
  if index_buffer_size > 0 then begin
    Gl.bind_vertex_array vao;
    let index_buffer = Glutil.get_int (Gl.gen_buffers 1) in
    Gl.bind_buffer Gl.element_array_buffer index_buffer;
    Gl.buffer_data Gl.element_array_buffer index_buffer_size None Gl.static_draw;

    (* Fill with data *)
    Array.iteri (fun i curr_data ->
        let offset = index_start_locs.(i) in
        fill_bound_buffer_object Gl.element_array_buffer offset curr_data
      ) index_data;

    (* Fill in indexed rendering commands *)
    let primitives = Array.mapi (fun i -> function
        | IndexedCmd (cmd, _) ->
          IndexedCmd ({ cmd with
                        start = index_start_locs.(i);
                        elem_count = buffer_dim index_data.(i);
                      }, gl_type_of_buf index_data.(i).buf)
        | Arrays cmd -> Arrays cmd
      ) (Array.of_list render_cmds) in

    StringMap.iter (fun _ data ->
        Gl.bind_vertex_array data;
        Gl.bind_buffer Gl.element_array_buffer index_buffer
      ) named_vaos;

    Gl.bind_vertex_array 0;
    attrib_arrays_buffer, Some index_buffer, vao, named_vaos, primitives
  end else
    attrib_arrays_buffer, None, vao, named_vaos, Array.of_list render_cmds;;

class mesh (filename:string) =
  let attrib_arrays_buffer, index_buffer, vao, named_vaos, primitives = load_mesh filename in
  object

    method render =
      Gl.bind_vertex_array vao;
      Array.iter render_cmd primitives;
      Gl.bind_vertex_array 0

    method render_mesh mesh_name =
      let vao = StringMap.find mesh_name named_vaos in
      Gl.bind_vertex_array vao;
      Array.iter render_cmd primitives;
      Gl.bind_vertex_array 0

    method delete =
      Glutil.set_int (Gl.delete_buffers 1) attrib_arrays_buffer;
      begin match index_buffer with
        | Some index_buffer ->
          Glutil.set_int (Gl.delete_buffers 1) index_buffer;
        | None -> ()
      end;
      Glutil.set_int (Gl.delete_vertex_arrays 1) vao;
      StringMap.iter (fun _ vao ->
          Glutil.set_int (Gl.delete_vertex_arrays 1) vao
        ) named_vaos;
  end
