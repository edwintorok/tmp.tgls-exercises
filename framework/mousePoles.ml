open Gg
type view_data = {
  mutable target_pos: V3.t;
  mutable orient: Quat.t;
  mutable radius: float;
  mutable deg_spin_rotation: float
}

type view_scale = {
  min_radius: float;
  max_radius: float;
  large_radius_delta: float;
  small_radius_delta: float;
  large_pos_offset: float;
  small_pos_offset: float;
  mutable rotation_scale: float;
}

type object_data = {
  position: V3.t;
  mutable orientation: Quat.t;
}

class virtual view_provider = object
  method virtual calc_matrix : M4.t
end

type mouse_buttons = MB_LEFT_BTN | MB_MIDDLE_BTN | MB_RIGHT_BTN
class type mouse_pole = object
  method mouse_move : V2.t -> unit
  method mouse_click : mouse_buttons -> bool -> int -> V2.t -> unit
  method mouse_wheel : int -> int -> V2.t -> unit
  method char_press : char -> unit
end

type rotate_mode = RM_DUAL_AXIS_ROTATE | RM_BIAXIAL_ROTATE | RM_XZ_AXIS_ROTATE | RM_Y_AXIS_ROTATE | RM_SPIN_VIEW_AXIS
type obj_rotate_mode = RM_DUAL_AXIS | RM_BIAXIAL | RM_SPIN
type dir = DIR_FORWARD | DIR_BACKWARD | DIR_RIGHT | DIR_LEFT | DIR_UP | DIR_DOWN
type axis = AXIS_X | AXIS_Y | AXIS_Z
(* TODO: use tsdl values directly *)
let mm_key_shift = 1
let mm_key_ctrl = 2
let mm_key_alt = 4

class object_pole ?(action_button = MB_LEFT_BTN) initial_data rot look_at_provider =
  object(self)
    val mutable rotate_scale = rot
    val mutable po = initial_data
    val mutable is_dragging = false
    val mutable start_drag_orient = Quat.id
    val mutable prev_mouse_pos = V2.zero
    val mutable rotate_mode = RM_DUAL_AXIS
    val mutable start_drag_mouse_pos = V2.zero

    method calc_matrix =
      M4.mul (M4.move3 po.position) (M4.of_quat po.orientation)

    method set_rotation_scale rot =
      rotate_scale <- rot

    method get_rotation_scale = rotate_scale
    method get_pos_orient = po
    method reset =
      if not is_dragging then
        po <- initial_data

    method private rotate_world_degrees rot from_initial =
      po.orientation <- Quat.unit (Quat.mul rot
                                     (if from_initial && is_dragging then start_drag_orient else po.orientation))

    method private rotate_local_degrees rot from_initial =
      po.orientation <- Quat.unit (Quat.mul
                                     (if from_initial && is_dragging then start_drag_orient else po.orientation)
                                     rot)

    method private rotate_view_degrees rot from_initial =
      match look_at_provider with
      | None ->
        self#rotate_world_degrees rot from_initial
      | Some view ->
        let view_quat = Quat.of_m4 view#calc_matrix in
        let inv_view_quat = Quat.conj view_quat in
        po.orientation <- Quat.unit (Quat.mul
                                       (Quat.mul (Quat.mul inv_view_quat rot) view_quat)
                                       (if from_initial && is_dragging then
                                          start_drag_orient
                                        else
                                          po.orientation))

    method private calc_rotation_quat axis deg_angle =
      let v = match axis with
        | AXIS_X -> V3.ox
        | AXIS_Y -> V3.oy
        | AXIS_Z -> V3.oz in
      Quat.rot3_axis v (Float.rad_of_deg deg_angle)

    method mouse_move position =
      if is_dragging then begin
        let i_diff = V2.sub position prev_mouse_pos in
        prev_mouse_pos <- position;
        match rotate_mode with
        | RM_DUAL_AXIS ->
          let rot = self#calc_rotation_quat AXIS_Y
              ((V2.x i_diff) *. rotate_scale) in
          let rot = Quat.unit (Quat.mul
                                 (self#calc_rotation_quat AXIS_X ((V2.y i_diff) *. rotate_scale))
                                 rot
                              ) in
          self#rotate_view_degrees rot false
        | RM_BIAXIAL ->
          let init_diff = V2.sub position start_drag_mouse_pos in
          let axis, deg_angle =
            if (abs_float (V2.x init_diff) > abs_float (V2.y init_diff)) then
              AXIS_Y, (V2.x init_diff) *. rotate_scale
            else
              AXIS_X, (V2.y init_diff) *. rotate_scale in
          let rot = self#calc_rotation_quat axis deg_angle in
          self#rotate_view_degrees rot true
        | RM_SPIN ->
          self#rotate_view_degrees (self#calc_rotation_quat AXIS_Z
                                      (-.(V2.x i_diff) *. rotate_scale)) false
      end

    method mouse_click button is_pressed modifiers position =
      if is_pressed then begin
        if not is_dragging && button = action_button then begin
          if modifiers land mm_key_alt > 0 then
            rotate_mode <- RM_SPIN
          else if modifiers land mm_key_ctrl > 0 then
            rotate_mode <- RM_BIAXIAL
          else
            rotate_mode <- RM_DUAL_AXIS;
          prev_mouse_pos <- position;
          start_drag_mouse_pos <- position;
          start_drag_orient <- po.orientation;
          is_dragging <- true
        end
      end else begin
        if is_dragging && button = action_button then begin
          self#mouse_move position;
          is_dragging <- false
        end
      end

    method mouse_wheel (_direction:int) (_modifiers:int) (_position:V2.t) = ()
    method char_press (_key : char) = ()
  end

class view_pole ?(action_button = MB_LEFT_BTN) ?(right_keyboard_ctrls = false)
    initial_view view_scale =
  object(self)
    inherit view_provider

    val mutable is_dragging = false
    val mutable curr_view = initial_view
    val mutable start_drag_orient = Quat.id
    val mutable deg_start_drag_spin = 0.0
    val mutable rotate_mode = RM_DUAL_AXIS_ROTATE
    val mutable start_drag_mouse_loc = V2.zero

    method calc_matrix =
      let the_mat = M4.move3 (V3.v 0.0 0.0 (-.curr_view.radius)) in

      let full_rotation = Quat.mul
          (Quat.rot3_axis (V3.v 0.0 0.0 1.0)
             (Float.rad_of_deg curr_view.deg_spin_rotation))
          curr_view.orient in
      let the_mat = M4.mul the_mat (M4.of_quat full_rotation) in
      M4.mul the_mat (M4.move3 (V3.neg curr_view.target_pos))

    method reset =
      if not is_dragging then
        curr_view <- initial_view

    method set_rotation_scale rotate_scale =
      view_scale.rotation_scale <- rotate_scale

    method private process_x_change x_diff =
      let deg_angle_diff = x_diff *. view_scale.rotation_scale in
      curr_view.orient <- Quat.mul start_drag_orient
          (Quat.rot3_axis (V3.v 0.0 1.0 0.0) (Float.rad_of_deg deg_angle_diff))

    method private process_y_change y_diff =
      let deg_angle_diff = y_diff *. view_scale.rotation_scale in
      curr_view.orient <- Quat.mul
          (Quat.rot3_axis (V3.v 1.0 0.0 0.0) (Float.rad_of_deg deg_angle_diff))
          start_drag_orient

    method private process_xy_change x_diff y_diff =
      let deg_x_angle_diff = x_diff *. view_scale.rotation_scale
      and deg_y_angle_diff = y_diff *. view_scale.rotation_scale in

      curr_view.orient <- Quat.mul start_drag_orient
          (Quat.rot3_axis (V3.v 0.0 1.0 0.0) (Float.rad_of_deg deg_x_angle_diff));
      curr_view.orient <- Quat.mul
          (Quat.rot3_axis (V3.v 0.0 1.0 0.0) (Float.rad_of_deg deg_y_angle_diff))
          curr_view.orient

    method private process_spin_axis x_diff _y_diff =
      let deg_spin_diff = x_diff *. view_scale.rotation_scale in
      curr_view.deg_spin_rotation <- deg_spin_diff +. deg_start_drag_spin

    method private begin_drag_rotate pt_start rot_mode =
      rotate_mode <- rot_mode;
      start_drag_mouse_loc <- pt_start;
      deg_start_drag_spin <- curr_view.deg_spin_rotation;
      start_drag_orient <- curr_view.orient;
      is_dragging <- true

    method private on_drag_rotate pt_curr =
      let i_diff = V2.sub pt_curr start_drag_mouse_loc in
      match rotate_mode with
      | RM_DUAL_AXIS_ROTATE ->
        self#process_xy_change (V2.x i_diff) (V2.y i_diff)
      | RM_BIAXIAL_ROTATE ->
        if (abs_float (V2.x i_diff) > abs_float (V2.y i_diff)) then
          self#process_y_change (V2.x i_diff)
        else
          self#process_y_change (V2.y i_diff)
      | RM_XZ_AXIS_ROTATE ->
        self#process_x_change (V2.x i_diff)
      | RM_Y_AXIS_ROTATE ->
        self#process_y_change (V2.y i_diff)
      | RM_SPIN_VIEW_AXIS ->
        self#process_spin_axis (V2.x i_diff) (V2.y i_diff)

    method private end_drag_rotate ?(keep_results=true) pt_end =
      if keep_results then
        self#on_drag_rotate pt_end
      else
        curr_view.orient <- start_drag_orient;
      is_dragging <- false

    method private move_closer large_step =
      if large_step then
        curr_view.radius <- curr_view.radius -. view_scale.large_radius_delta
      else
        curr_view.radius <- curr_view.radius -. view_scale.small_radius_delta;
      if curr_view.radius < view_scale.min_radius then
        curr_view.radius <- view_scale.min_radius

    method private move_away large_step =
      if large_step then
        curr_view.radius <- curr_view.radius +. view_scale.large_radius_delta
      else
        curr_view.radius <- curr_view.radius +. view_scale.small_radius_delta;
      if curr_view.radius > view_scale.max_radius then
        curr_view.radius <- view_scale.max_radius

    method mouse_move position =
      if is_dragging then begin
        self#on_drag_rotate position;
      end

    method mouse_click button is_pressed modifiers position =
      if is_pressed then begin
        if not is_dragging && button = action_button then begin
          if modifiers land mm_key_ctrl > 0 then
            self#begin_drag_rotate position RM_BIAXIAL_ROTATE
          else if modifiers land mm_key_alt > 0 then
            self#begin_drag_rotate position RM_SPIN_VIEW_AXIS
          else
            self#begin_drag_rotate position RM_DUAL_AXIS_ROTATE
        end
      end else begin
        if is_dragging && button = action_button then begin
          if rotate_mode = RM_DUAL_AXIS_ROTATE ||
             rotate_mode = RM_SPIN_VIEW_AXIS ||
             rotate_mode = RM_BIAXIAL_ROTATE then
            self#end_drag_rotate position
        end
      end

    method mouse_wheel direction modifiers (_position:V2.t) =
      if direction > 0 then
        self#move_closer (modifiers land mm_key_shift = 0)
      else
        self#move_away (modifiers land mm_key_shift = 0)

    method char_press key =
      if right_keyboard_ctrls then match key with
        | 'i' -> self#offset_target_pos DIR_FORWARD view_scale.large_pos_offset
        | 'k' -> self#offset_target_pos DIR_BACKWARD view_scale.large_pos_offset
        | 'l' -> self#offset_target_pos DIR_RIGHT view_scale.large_pos_offset
        | 'j' -> self#offset_target_pos DIR_LEFT view_scale.large_pos_offset
        | 'o' -> self#offset_target_pos DIR_UP view_scale.large_pos_offset
        | 'u' -> self#offset_target_pos DIR_DOWN view_scale.large_pos_offset
        | 'I' -> self#offset_target_pos DIR_FORWARD view_scale.small_pos_offset
        | 'K' -> self#offset_target_pos DIR_BACKWARD view_scale.small_pos_offset
        | 'L' -> self#offset_target_pos DIR_RIGHT view_scale.small_pos_offset
        | 'J' -> self#offset_target_pos DIR_LEFT view_scale.small_pos_offset
        | 'O' -> self#offset_target_pos DIR_UP view_scale.small_pos_offset
        | 'U' -> self#offset_target_pos DIR_DOWN view_scale.small_pos_offset
        | _ -> ()
      else match key with
        | 'w' -> self#offset_target_pos DIR_FORWARD view_scale.large_pos_offset
        | 's' -> self#offset_target_pos DIR_BACKWARD view_scale.large_pos_offset
        | 'd' -> self#offset_target_pos DIR_RIGHT view_scale.large_pos_offset
        | 'a' -> self#offset_target_pos DIR_LEFT view_scale.large_pos_offset
        | 'e' -> self#offset_target_pos DIR_UP view_scale.large_pos_offset
        | 'q' -> self#offset_target_pos DIR_DOWN view_scale.large_pos_offset
        | 'W' -> self#offset_target_pos DIR_FORWARD view_scale.small_pos_offset
        | 'S' -> self#offset_target_pos DIR_BACKWARD view_scale.small_pos_offset
        | 'D' -> self#offset_target_pos DIR_RIGHT view_scale.small_pos_offset
        | 'A' -> self#offset_target_pos DIR_LEFT view_scale.small_pos_offset
        | 'E' -> self#offset_target_pos DIR_UP view_scale.small_pos_offset
        | 'Q' -> self#offset_target_pos DIR_DOWN view_scale.small_pos_offset
        | _ -> ()

    method private offset_target_pos dir world_distance =
      let offset_dir = match dir with
        | DIR_UP -> V3.v 0.0 1.0 0.0
        | DIR_DOWN -> V3.v 0.0 (-1.0) 0.0
        | DIR_FORWARD -> V3.v 0.0 0.0 (-1.0)
        | DIR_BACKWARD -> V3.v 0.0 0.0 1.0
        | DIR_RIGHT -> V3.v 1.0 0.0 0.0
        | DIR_LEFT -> V3.v (-1.0) 0.0 0.0 in
      let camera_offset = V3.smul world_distance offset_dir in
      let curr_mat = self#calc_matrix in
      let orientation = Quat.of_m4 curr_mat in
      let inv_orient = Quat.conj orientation in
      let world_offset = Quat.apply3 inv_orient camera_offset in
      curr_view.target_pos <- V3.add curr_view.target_pos world_offset

    method get_rotation_scale = view_scale.rotation_scale

    method get_view = curr_view

  end

