(* TODO: use file reads instead of map_file *)
(* TODO: with_resource *)

let rec load_mipmaps accum fd mipmap_count width height mipmap pos =
  if mipmap < mipmap_count then
    let width = width lsr 1
    and height = height lsr 1
    and size = width * height * 4 (* TODO: from header *) in
    let next_pos = Int64.add pos (Int64.of_int size) in
    let ba = Bigarray.(Array1.map_file fd int8_unsigned c_layout false
                         ~pos size) in
    load_mipmaps ((ba,width,height) :: accum) fd mipmap_count width height (mipmap+1) next_pos
  else
    accum

let load_from_file file =
  let fd = Unix.openfile file [Unix.O_RDONLY] 0 in
  let header_dwords = 31 in
  let header = Bigarray.(Array1.map_file fd int32 c_layout false (1+header_dwords)) in
  let magic = header.{0} in
  if magic <>  0x20534444l then
    invalid_arg "invalid DDS file magic";
  let header_size = Int32.to_int header.{1} in
  if header_size <> header_dwords * 4 then
    invalid_arg "wrong header size";
  let flags = Int32.to_int header.{2}
  and height = Int32.to_int header.{3}
  and width = Int32.to_int  header.{4} in
  let mipmap_count =
    if flags land 0x20000 > 0 then Int32.to_int header.{7} else 1 in
  (* TODO: check/use other header fields *)
  let main_surface = (1 + header_dwords)*4 in
  (* TODO: compute these from the header fields *)
  let ba = load_mipmaps [] fd mipmap_count width height 0 (Int64.of_int main_surface) in
  Unix.close fd;
  Array.of_list (List.rev ba)
