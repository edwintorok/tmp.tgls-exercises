open Ctypes
open Foreign

type enum = int
let bool =
  view ~read:(fun u -> Unsigned.UChar.(compare u zero <> 0))
    ~write:(fun b -> Unsigned.UChar.(of_int (Pervasives.compare b false)))
    uchar
let stub = true
let int_as_uint =
  view ~read:Unsigned.UInt.to_int
    ~write:Unsigned.UInt.of_int
    uint
let ba_opt_as_uint32p =
  view ~read:(fun _ -> assert false)
    ~write:(function
        | None -> null
        | Some b -> to_voidp (bigarray_start array1 b))
    (ptr void)

(* GL_ARB_debug_output *)
let debug_output_synchronous_arb = 0x8242
let debug_next_logged_message_length_arb = 0x8243
let debug_callback_function_arb = 0x8244
let debug_callback_user_param_arb = 0x8245
let debug_source_api_arb = 0x8246
let debug_source_window_system_arb = 0x8247
let debug_source_shader_compiler_arb = 0x8248
let debug_source_third_party_arb = 0x8249
let debug_source_application_arb = 0x824A
let debug_source_other_arb = 0x824B
let debug_type_error_arb = 0x824C
let debug_type_deprecated_behavior_arb = 0x824D
let debug_type_undefined_behavior_arb = 0x824E
let debug_type_portability_arb = 0x824F
let debug_type_performance_arb = 0x8250
let debug_type_other_arb = 0x8251
let max_debug_message_length_arb = 0x9143
let max_debug_logged_messages_arb = 0x9144
let debug_logged_messages_arb = 0x9145
let debug_severity_high_arb = 0x9146
let debug_severity_medium_arb = 0x9147
let debug_severity_low_arb = 0x9148
type debug_proc = enum -> enum -> int -> enum -> string -> unit
let debug_message_callback_arb =
  foreign ~stub "glDebugMessageCallbackARB"
    ((funptr (int_as_uint @-> int_as_uint @-> int_as_uint @->
              int_as_uint @-> int @-> ptr char @-> ptr void @->
              returning void)) @->
     ptr void @-> returning void)

let callback = ref None
let debug_message_callback_arb f =
  let wrap_cb src typ id sev len msg _ =
    let s = Bytes.create len in
    for i = 0 to len - 1 do Bytes.set s i (!@ (msg +@ i)) done;
    f src typ id sev s
  in
  callback := Some wrap_cb; (* prevent Ffi_stubs.CallToExpiredClosure *)
  debug_message_callback_arb wrap_cb null

type ('a, 'b) bigarray = ('a,'b, Bigarray.c_layout) Bigarray.Array1.t
type uint32_bigarray = (int32, Bigarray.int32_elt) bigarray
let debug_message_control_arb :
  enum -> enum -> enum -> int -> uint32_bigarray option -> bool -> unit =
  foreign ~stub "glDebugMessageControlARB"
    (int_as_uint @-> int_as_uint @-> int_as_uint @-> int @->
     ba_opt_as_uint32p @-> bool @-> returning void)

let debug_message_insert_arb =
  foreign ~stub "glDebugMessageInsertARB"
    (int_as_uint @-> int_as_uint @-> int_as_uint @-> int_as_uint @->
     int @-> string @-> returning void)

let arb_debug_output_extension_supported () =
  Tsdl.Sdl.gl_extension_supported "GL_ARB_debug_output"
