open Typedgl

(* math *)
let persp_fov ~fov ~near ~far ~aspect =
  let h = 2. *. near *. (tan (fov /. 2.)) in
  let w = aspect *. h in
  Gg.M4.persp ~near ~far ~left:(-.w /. 2.) ~right:(w /. 2.)
    ~bot:(-.h /. 2.) ~top:(h /. 2.)

(* conversions *)
let ba_copy ba =
  let ba2 = Bigarray.(Array1.create (Array1.kind ba) c_layout (Array1.dim ba))
  in
  Bigarray.Array1.blit ba ba2;
  ba2

let ba_float32_of_array a =
  Bigarray.(Array1.of_array float32 c_layout a)

let ba_float32_of_array2d a =
  let a2d = Bigarray.(Array2.of_array float32 c_layout a) in
  let dim = Bigarray.Array2.(dim1 a2d * dim2 a2d) in
  Bigarray.(reshape_1 (genarray_of_array2 a2d) dim)

let ba_ushort_of_array a =
  Bigarray.(Array1.of_array int16_unsigned c_layout a)

let ba_create size = Bigarray.(Array1.create float32 c_layout size)

let value_ptr_m4 =
  let ba = ba_create 16 in
  fun m4 ->
    (* we want column-major order *)
    Gg.M4.iteri (fun i j v ->
        ba.{i + j * 4} <- v
      ) m4;
    ba

let value_ptr_m3 =
  let ba = ba_create 9 in
  fun m3 ->
    (* we want column-major order *)
    Gg.M3.iteri (fun i j v ->
        ba.{i + j * 3} <- v
      ) m3;
    ba

let value_ptr_i2 =
  let ba = Bigarray.(Array1.create int32 c_layout 2) in
  fun x y ->
    ba.{0} <- Int32.of_int x;
    ba.{1} <- Int32.of_int y;
    ba

let value_ptr_v3 =
  let ba = ba_create 3 in
  fun v3 ->
    (* we want column-major order *)
    Gg.V3.iteri (fun i v ->
        ba.{i} <- v
      ) v3;
    ba

let value_ptr_v4 =
  let ba = ba_create 4 in
  fun v4 ->
    (* we want column-major order *)
    Gg.V4.iteri (fun i v ->
        ba.{i} <- v
      ) v4;
    ba

let get_int = Typedgl.get_int
let get_float = Typedgl.get_float
let get_int_array = Typedgl.get_int_array

let set_int = Typedgl.set_int

let get_string = Typedgl.get_string

exception CompileLinkException of string

let compile_shader typ shader_text =
  let shader = Shader.create typ in
  Shader.source shader shader_text;
  Shader.compile shader;
  if Shader.compile_status shader then shader
  else begin
    let info_log = Shader.info_log shader in
    Shader.delete shader;
    Tsdl.(Sdl.log_verbose Sdl.Log.category_render "%s" shader_text);
    raise (CompileLinkException info_log)
  end

let link_program shaders =
  let program = Program.create () in
  List.iter (Program.attach_shader program) shaders;
  Program.link program;
  List.iter (Program.detach_shader program) shaders;
  if Program.link_status program then program
  else begin
    let info_log = Program.info_log program in
    Program.delete program;
    raise (CompileLinkException info_log)
  end
