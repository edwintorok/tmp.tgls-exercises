open Tgl3.Gl
val persp_fov : fov:float -> near:float -> far:float -> aspect:float -> Gg.m4
val ba_copy :
  ('a, 'b, Bigarray.c_layout) Bigarray.Array1.t ->
  ('a, 'b, Bigarray.c_layout) Bigarray.Array1.t
val ba_float32_of_array :
  float array ->
  (float, Bigarray.float32_elt) bigarray
val ba_float32_of_array2d :
  float array array ->
  (float, Bigarray.float32_elt) bigarray
val ba_ushort_of_array :
  int array ->
  (int, Bigarray.int16_unsigned_elt, Bigarray.c_layout) Bigarray.Array1.t
val ba_create :
  int -> (float, Bigarray.float32_elt) bigarray
val value_ptr_m4 :
  Gg.m4 -> (float, Bigarray.float32_elt) bigarray
val value_ptr_m3 :
  Gg.m3 -> (float, Bigarray.float32_elt) bigarray
val value_ptr_i2 :
  int -> int -> (int32, Bigarray.int32_elt) bigarray
val value_ptr_v3 :
  Gg.v3 -> (float, Bigarray.float32_elt) bigarray
val value_ptr_v4 :
  Gg.v4 -> (float, Bigarray.float32_elt) bigarray
val get_int : ((int32, Bigarray.int32_elt) bigarray -> unit) -> int
val get_float : ((float, Bigarray.float32_elt) bigarray -> unit) -> float
val get_int_array : int -> (int -> uint32_bigarray -> unit) -> int array
val set_int :
  ((int32, Bigarray.int32_elt) bigarray -> unit) -> int -> unit
val get_string :
  int ->
  ((char, Bigarray.int8_unsigned_elt) bigarray -> unit) -> string
exception CompileLinkException of string
val compile_shader : Tgl3.Gl.enum -> string -> Typedgl.Shader.t
val link_program : Typedgl.Shader.t list -> Typedgl.Program.t
