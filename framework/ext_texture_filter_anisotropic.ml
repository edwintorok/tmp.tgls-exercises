let ext_texture_filter_anisotropic_extension_supported () =
  Tsdl.Sdl.gl_extension_supported "GL_EXT_texture_filter_anisotropic"
let gl_texture_max_anistropy_ext = 0x84fe
let gl_max_texture_max_anistropy_ext = 0x84ff
