type view_data = {
  mutable target_pos : Gg.V3.t;
  mutable orient : Gg.Quat.t;
  mutable radius : float;
  mutable deg_spin_rotation : float;
}
type view_scale = {
  min_radius : float;
  max_radius : float;
  large_radius_delta : float;
  small_radius_delta : float;
  large_pos_offset : float;
  small_pos_offset : float;
  mutable rotation_scale : float;
}
type object_data = { position : Gg.V3.t; mutable orientation : Gg.Quat.t; }

val mm_key_shift : int
val mm_key_ctrl : int
val mm_key_alt : int

class virtual view_provider : object method virtual calc_matrix : Gg.M4.t end
type mouse_buttons = MB_LEFT_BTN | MB_MIDDLE_BTN | MB_RIGHT_BTN
class type mouse_pole =
  object
    method char_press : char -> unit
    method mouse_click : mouse_buttons -> bool -> int -> Gg.V2.t -> unit
    method mouse_move : Gg.V2.t -> unit
    method mouse_wheel : int -> int -> Gg.V2.t -> unit
  end
class object_pole :
  ?action_button:mouse_buttons ->
  object_data ->
  float ->
  #view_provider option ->
  object
    method calc_matrix : Gg.m4
    method char_press : char -> unit
    method get_pos_orient : object_data
    method get_rotation_scale : float
    method mouse_click : mouse_buttons -> bool -> int -> Gg.v2 -> unit
    method mouse_move : Gg.v2 -> unit
    method mouse_wheel : int -> int -> Gg.V2.t -> unit
    method reset : unit
    method set_rotation_scale : float -> unit
  end
class view_pole :
  ?action_button:mouse_buttons ->
  ?right_keyboard_ctrls:bool ->
  view_data ->
  view_scale ->
  object
    method calc_matrix : Gg.M4.t
    method char_press : char -> unit
    method get_rotation_scale : float
    method get_view : view_data
    method mouse_click : mouse_buttons -> bool -> int -> Gg.v2 -> unit
    method mouse_move : Gg.v2 -> unit
    method mouse_wheel : int -> int -> Gg.V2.t -> unit
    method reset : unit
    method set_rotation_scale : float -> unit
  end
