open Tgl3
let bigarray_create k len = Bigarray.(Array1.create k c_layout len)
let get_int =
  let a = bigarray_create Bigarray.int32 1 in
  fun f -> f a; Int32.to_int a.{0}

let get_int_array n =
  let a = bigarray_create Bigarray.int32 n in
  fun f -> f n a; Array.init n (fun i -> Int32.to_int a.{i})

let get_float =
  let a = bigarray_create Bigarray.float32 1 in
  fun f -> f a; a.{0}

let set_int =
  let a = bigarray_create Bigarray.int32 1 in
  fun f i -> a.{0} <- Int32.of_int i; f a; ()
let get_string len f =
  let a = bigarray_create Bigarray.char len in
  f a; Gl.string_of_bigarray a
module Shader = struct
  type t = int
  let create = Gl.create_shader
  let source = Gl.shader_source
  let compile = Gl.compile_shader
  let compile_status shader =
    get_int (Gl.get_shaderiv shader Gl.compile_status) = Gl.true_
  let info_log shader =
    let len = get_int (Gl.get_shaderiv shader Gl.info_log_length) in
    get_string len (Gl.get_shader_info_log shader len None)
  let delete = Gl.delete_shader
end
module Program = struct
  type t = int
  let create = Gl.create_program
  let attach_shader = Gl.attach_shader
  let detach_shader = Gl.detach_shader
  let delete = Gl.delete_program
  let link = Gl.link_program
  let link_status program =
    get_int (Gl.get_programiv program Gl.link_status) = Gl.true_
  let info_log program =
    let len = get_int (Gl.get_programiv program Gl.info_log_length) in
    get_string len (Gl.get_program_info_log program len None)
  let to_int x = x
end
