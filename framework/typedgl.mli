val get_int : ((int32, Bigarray.int32_elt) Tgl3.Gl.bigarray -> unit) -> int
val get_float : ((float, Bigarray.float32_elt) Tgl3.Gl.bigarray -> unit) -> float
val get_int_array : int -> (int -> Tgl3.Gl.uint32_bigarray -> unit) -> int array
val set_int : ((int32, Bigarray.int32_elt) Tgl3.Gl.bigarray -> unit) -> int -> unit
val get_string : int -> ((char, Bigarray.int8_unsigned_elt) Tgl3.Gl.bigarray -> unit) -> string
module Shader : sig
  type t = private int
  val create : Tgl3.Gl.enum -> t
  val source : t -> string -> unit
  val compile : t -> unit
  val delete : t -> unit
  val compile_status : t -> bool
  val info_log : t -> string
end
module Program : sig
  type t
  val create : unit -> t
  val attach_shader : t -> Shader.t -> unit
  val detach_shader : t -> Shader.t -> unit
  val delete : t -> unit
  val link : t -> unit
  val link_status : t -> bool
  val info_log : t -> string
  val to_int : t -> int
end
