type flags = DOUBLE | ALPHA | DEPTH | STENCIL | SRGB
type timer_type = TT_SINGLE | TT_LOOP | TT_INFINITE
class timer :
  timer_type ->
  float ->
  object
    val mutable abs_prev_time : float
    val mutable has_updated : bool
    val mutable is_paused : bool
    val mutable sec_accum_time : float
    method fast_forward : float -> unit
    method get_alpha : float
    method get_progression : float
    method get_time_since_start : float
    method is_paused : bool
    method reset : unit
    method rewind : float -> unit
    method set_pause : bool -> unit
    method toggle_pause : bool
    method update : bool
  end

type defaults = flags list -> flags list
val create_program : Typedgl.Shader.t list -> int
val load_shader : Tgl3.Gl.enum -> string -> Typedgl.Shader.t

val mesh : string -> Mesh.mesh

class virtual framework : defaults -> object
    (* tutorial interface *)
    method virtual display : unit
    method virtual reshape : w:int -> h:int -> unit
    method virtual keyboard : int -> unit
    method mouse_button :
      Tsdl.Sdl.uint8 -> Tsdl.Sdl.button_state -> int -> int -> unit
    method mouse_motion : int -> int -> unit
    method mouse_wheel : int -> int -> int -> int -> unit

    (* Glut emulation *)
    method leave_main_loop : unit
    method elapsed_time : float
    method post_redisplay : unit
    method swap_buffers : unit

    method main_loop : unit

    method forward_mouse_button :
      MousePoles.mouse_pole ->
      int -> Tsdl.Sdl.button_state -> int -> int -> unit
    method forward_mouse_motion : MousePoles.mouse_pole -> int -> int -> unit
    method forward_mouse_wheel :
      MousePoles.mouse_pole -> int -> int -> int -> int -> unit
  end
val run: <main_loop: unit; ..> -> unit
