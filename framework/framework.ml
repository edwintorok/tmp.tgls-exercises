open Tgl3
open Tsdl
open Arb_debug_output
open MousePoles

let check = function
  | Ok r -> r
  | Error (`Msg msg) -> failwith msg

type flags = DOUBLE | ALPHA | DEPTH | STENCIL | SRGB
type defaults = flags list -> flags list

let event e = Sdl.Event.(enum (get e typ))
let key_code e = Sdl.Event.(get e keyboard_keycode)
let window_event e = Sdl.Event.(window_event_enum (get e window_event_id))
(* let window_size e = Sdl.Event.(get e window_data1, get e window_data2) *)
let set attr v = check (Sdl.gl_set_attribute attr v)

(* Logging *)
let pp_opengl_info ppf () =
  let pp = Format.fprintf in
  let pp_opt ppf = function None -> pp ppf "error" | Some s -> pp ppf "%s" s in
  pp ppf "@[<v 6>";
  let v1, v2, v3 = Sdl.get_version () in
  pp ppf "SDL @[v@[%d.%d.%d@]@ " v1 v2 v3;
  pp ppf "Platform: @[%s@]@ " (Sdl.get_platform ());
  pp ppf "CPUs: %d@ " (Sdl.get_cpu_count ());
  pp ppf "RAM: %d MB@]@ " (Sdl.get_system_ram ());
  pp ppf "Renderer @[<v>@[%a@]@," pp_opt (Gl.get_string Gl.renderer);
  pp ppf "@[OpenGL %a / GLSL %a@]@]@,"
    pp_opt (Gl.get_string Gl.version)
    pp_opt (Gl.get_string Gl.shading_language_version);
  pp ppf "@]"

let string_of_source e =
  if e = debug_source_api_arb then "API"
  else if e = debug_source_window_system_arb then "Window System"
  else if e = debug_source_shader_compiler_arb then "Shader Compiler"
  else if e = debug_source_third_party_arb then "Third Party"
  else if e = debug_source_application_arb then "Application"
  else if e = debug_source_other_arb then "Other"
  else "??"

let string_of_type e =
  if e = debug_type_error_arb then "Error"
  else if e = debug_type_deprecated_behavior_arb then "Deprecated Functionality"
  else if e = debug_type_portability_arb then "Portability"
  else if e = debug_type_performance_arb then "Performance"
  else if e = debug_type_other_arb then "Other"
  else "??"

let string_of_severity e =
  if e = debug_severity_high_arb then Sdl.log_error Sdl.Log.category_application
  else if e = debug_severity_medium_arb then Sdl.log_warn Sdl.Log.category_application
  else if e = debug_severity_low_arb then Sdl.log_verbose Sdl.Log.category_application
  else Sdl.log

let debug_func src typ _id severity msg =
  let log = string_of_severity severity in
  log "%s from %s: %s" (string_of_type typ) (string_of_source src) msg

(* utilities *)
let init_display_mode flags =
  let set_flag sdl_flag flag =
    let value = if List.mem flag flags then 1 else 0 in
    set sdl_flag value in
  set_flag Sdl.Gl.doublebuffer DOUBLE;
  set_flag Sdl.Gl.alpha_size ALPHA;
  set_flag Sdl.Gl.depth_size DEPTH;
  set_flag Sdl.Gl.stencil_size STENCIL;
  set_flag Sdl.Gl.framebuffer_srgb_capable SRGB
(* TODO: fallback to using FBOs if no sRGB visuals are available *)

let gl_major = 3 and gl_minor = 3 (* TODO: from cmdline *)

let rec read_lines accum ch =
  let line = try Some (input_line ch) with End_of_file -> None in
  match line with
  | Some l -> read_lines (l :: accum) ch
  | None -> List.rev accum

let update_shader_ver = function
  | first :: tl ->
    if gl_minor < 3 then
      "#version 140\n#extension GL_ARB_explicit_attrib_location: require" ::
      tl
    else
      first :: tl
  | l -> l

(* FIXME: do not leak file *)
let read_file name =
  let ch = open_in_bin name in
  let lines = read_lines [] ch in
  close_in ch;
  String.concat "\n" (update_shader_ver lines)

let with_resource ~release x f =
  let result = try `Ok (f x) with e -> `Error e in
  let finally = try `Ok (release x) with e -> `Error e in
  match result, finally with
  | `Ok r, `Ok () -> r
  | `Error e, `Ok () -> raise e
  | `Error e1, `Error _e2 -> raise e1
  | `Ok _, `Error e2 -> raise e2

type timer_type = TT_SINGLE | TT_LOOP | TT_INFINITE
class timer typ sec_duration = object
  val mutable has_updated = false
  val mutable is_paused = false
  val mutable abs_prev_time = 0.0
  val mutable sec_accum_time = 0.0

  initializer
    if typ <> TT_INFINITE then
      assert (sec_duration > 0.0)

  method reset =
    has_updated <- false;
    sec_accum_time <- 0.0

  method toggle_pause =
    is_paused <- not is_paused;
    is_paused

  method is_paused = is_paused
  method set_pause pause =
    is_paused <- pause

  method update =
    let abs_curr_time = Int32.to_float (Sdl.get_ticks ()) /. 1000.0 in
    if not has_updated then begin
      abs_prev_time <- abs_curr_time;
      has_updated <- true;
    end;
    if is_paused then begin
      abs_prev_time <- abs_curr_time;
      false
    end else begin
      let delta_time = abs_curr_time -. abs_prev_time in
      sec_accum_time <- sec_accum_time +. delta_time;
      abs_prev_time <- abs_curr_time;
      if typ = TT_SINGLE then
        sec_accum_time > sec_duration
      else
        false
    end

  method rewind sec_rewind =
    sec_accum_time <- max (sec_accum_time -. sec_rewind) 0.0

  method fast_forward sec_ff =
    sec_accum_time <- sec_accum_time +. sec_ff

  method get_alpha =
    match typ with
    | TT_LOOP ->
      (mod_float sec_accum_time sec_duration) /. sec_duration
    | TT_SINGLE ->
      Gg.Float.clamp ~min:0.0 ~max:1.0 (sec_accum_time /. sec_duration)
    | TT_INFINITE ->
      -1.0 (* garbage *)

  method get_progression =
    match typ with
    | TT_LOOP ->
      mod_float sec_accum_time sec_duration
    | TT_SINGLE ->
      Gg.Float.clamp ~min:0.0 ~max:sec_duration sec_accum_time
    | TT_INFINITE ->
      -1.0 (* garbage *)

  method get_time_since_start = sec_accum_time
end

(* tsdl should provide these *)
let button_left = 1
let button_middle = 2
let button_right = 3

let () =
  assert (1 lsl (button_left-1) = Int32.to_int Sdl.Button.lmask);
  assert (1 lsl (button_middle-1) = Int32.to_int Sdl.Button.mmask);
  assert (1 lsl (button_right-1) = Int32.to_int Sdl.Button.rmask)

let printfps = ref false

let initialize defaults =
  let debug = ref false in
  let w = ref 500 in
  let h = ref 500 in
  let fullscreen = ref false in
  let vsync_interval = ref 1 in
  let test_mode = ref false in

  Arg.current := 0;
  Arg.(parse (align [
      "-debug", Set debug,
      " create a debug OpenGL context, and show verbose messages";
      "-w", Set_int w,
      Printf.sprintf " set window width (default: %d)" !w;
      "-h", Set_int h,
      Printf.sprintf " set window height (default: %d)" !h;
      "-fullscreen", Set fullscreen,
      " create a fullscreen window of same size as desktop (default: no)";
      "-printfps", Unit (fun () -> printfps := true),
      " print number of frames per second on the console periodically";
      "-test",Set test_mode,
      " render only3 frames and then quit";(* for valgrind testing *)
      (*          "-novsync", Unit (fun () -> vsync_interval := 0),
       * useless on fullscreen: SwapBuffersWait defaults to true, and it needs to be
       * set to false in order to completely disable vsync waits on fullscreen.
       * " do not wait for vsync when swapping buffers"
      *)
    ]) (fun _ -> raise (Bad "extra arguments passed")) "tutorial");

  let displayMode = [DOUBLE; ALPHA; DEPTH; STENCIL ] in
  check (Sdl.init Sdl.Init.video);
  init_display_mode (defaults displayMode);
  set Sdl.Gl.context_major_version gl_major;
  set Sdl.Gl.context_minor_version gl_minor;
  set Sdl.Gl.context_profile_mask Sdl.Gl.context_profile_core;
  if !debug then begin
    set Sdl.Gl.context_flags Sdl.Gl.context_debug_flag;
    Sdl.log_set_all_priority Sdl.Log.priority_verbose
  end;
  let title = Sys.argv.(0) in
  let is_fullscreen =
    if !fullscreen then Sdl.Window.fullscreen_desktop
    else Sdl.Window.windowed in
  let attrs = Sdl.Window.(opengl + resizable + is_fullscreen + shown) in
  let win = check (Sdl.create_window ~w:!w ~h:!h title attrs) in
  Sdl.log_debug Sdl.Log.category_application "Creating OpenGL %d.%d context"
    gl_major gl_minor;
  let ctx = check (Sdl.gl_create_context win) in
  check (Sdl.gl_make_current win ctx);
  (* wait for vsync on swap, because tutorials rely on post_redisplay to
   * loop *)
  check (Sdl.gl_set_swap_interval !vsync_interval);
  if !printfps && (!vsync_interval = 1) then
    Sdl.log_info Sdl.Log.category_application
      "Running synchronized to the vertical refresh rate, FPS won't exceed your monitor's refresh rate";

  if arb_debug_output_extension_supported () then begin
    Gl.enable debug_output_synchronous_arb;
    debug_message_callback_arb debug_func;
    debug_message_control_arb Gl.dont_care Gl.dont_care Gl.dont_care 0 None
      true;
    let msg = "Debug output initialized" in
    debug_message_insert_arb
      debug_source_application_arb debug_type_other_arb 42 debug_severity_low_arb
      (String.length msg) msg
  end;
  Sdl.log_verbose Sdl.Log.category_application "%a" pp_opengl_info ();

  if gl_minor < 2 && not (Sdl.gl_extension_supported "GL_ARB_draw_elements_base_vertex") then
    failwith "GL_ARB_draw_elements_base_vertex (or OpenGL >=3.2) required";
  win, ctx, !test_mode

class virtual framework defaults =
  let win, ctx, test_only = initialize defaults in
  object(self)
    (* Tutorial interface *)
    method virtual keyboard : int -> unit
    method virtual reshape : w:int -> h:int -> unit
    method virtual display : unit

    method mouse_motion (_x:int) (_y:int) = ()
    method mouse_button (_button:int) (_state:Sdl.button_state) (_x:int) (_y:int) = ()
    method mouse_wheel (_wheel:int) (_direction:int) (_x:int) (_y:int) = ()


    (* Glut emulation *)
    val mutable redisplay = true
    val mutable terminate = false
    val mutable t0 = 0.
    val mutable t1 = 0.
    val mutable frames = 0

    method post_redisplay =
      redisplay <- true

    method elapsed_time =
      Int32.to_float (Sdl.get_ticks ())

    method leave_main_loop =
      terminate <- true

    method swap_buffers =
      if Gl.get_error () <> Gl.no_error then
        Sdl.log_warn Sdl.Log.category_application "OpenGL error, run with -debug for details";
      Sdl.gl_swap_window win

    (* mouse *)
    method forward_mouse_motion (obj:mouse_pole) x y =
      obj#mouse_move (Gg.V2.v (float x) (float y))

    method private calc_glut_modifiers =
      let modifiers = Sdl.get_mod_state () in
      (if modifiers land Sdl.Kmod.shift > 0 then mm_key_shift else 0) lor
      (if modifiers land Sdl.Kmod.ctrl > 0 then mm_key_ctrl else 0) lor
      (if modifiers land Sdl.Kmod.alt > 0 then mm_key_alt else 0)

    method forward_mouse_button (obj:mouse_pole) button state x y =
      let modifiers = self#calc_glut_modifiers in
      let mouse_loc = Gg.V2.v (float x) (float y) in
      let is_pressed = state = Sdl.pressed in
      if button = button_left then
        obj#mouse_click MB_LEFT_BTN is_pressed modifiers mouse_loc
      else if button = button_middle then
        obj#mouse_click MB_MIDDLE_BTN is_pressed modifiers mouse_loc
      else if button = button_right then
        obj#mouse_click MB_RIGHT_BTN is_pressed modifiers mouse_loc

    method forward_mouse_wheel (obj:mouse_pole) (_wheel:int) direction x y =
      obj#mouse_wheel direction (self#calc_glut_modifiers)
        (Gg.V2.v (float x) (float y))

    method private delete =
      Sdl.gl_delete_context ctx;
      Sdl.destroy_window win;
      let print_exit_message =
        Sdl.log_get_priority Sdl.Log.category_application =
        Sdl.Log.priority_verbose in
      Sdl.quit ();
      if print_exit_message then
        prerr_endline "Exit successfully"

    method main_loop =
      let w, h = Sdl.get_window_size win in
      self#reshape ~w ~h;
      if test_only then begin
        self#display;
        self#display;
        self#display;
        self#delete
      end else begin
        let e = Sdl.Event.create () in

        let rec loop () =
          (* redisplay if needed *)
          if redisplay then begin
            redisplay <- false;
            self#display;(* display can force a redisplay *)
            if !printfps then begin
              frames <- frames + 1;
              t1 <- self#elapsed_time;
              let dt = (t1 -. t0) /. 1000. in
              if dt > 5. then begin
                let fps = float frames /. dt in
                Sdl.log_info Sdl.Log.category_application "%d frames in %.1fs = %.2f FPS"
                  frames dt fps;
                t0 <- t1;
                frames <- 0
              end
            end
          end;
          (* wait for an event to arrive, unless a redisplay was forced *)
          if not redisplay then
            check (Sdl.wait_event None);
          while Sdl.poll_event (Some e) && not terminate do
            match event e with
            | `Quit -> self#leave_main_loop
            | `Key_down -> self#keyboard (key_code e);
            | `Mouse_button_down | `Mouse_button_up ->
              let button = Sdl.Event.get e Sdl.Event.mouse_button_button
              and state = Sdl.Event.get e Sdl.Event.mouse_button_state
              and x = Sdl.Event.get e Sdl.Event.mouse_button_x
              and y = Sdl.Event.get e Sdl.Event.mouse_button_y in
              self#mouse_button button state x y
            | `Mouse_motion ->
              if Sdl.Event.get e Sdl.Event.mouse_motion_state > 0l then
                let x = Sdl.Event.get e Sdl.Event.mouse_motion_x
                and y = Sdl.Event.get e Sdl.Event.mouse_motion_y in
                self#mouse_motion x y (* dragging motion *)
            | `Mouse_wheel ->
              let direction = Sdl.Event.get e Sdl.Event.mouse_wheel_y in
              let _, (x,y) = Sdl.get_mouse_state () in
              self#mouse_wheel 0 direction x y
            | `Window_event ->
              begin match window_event e with
                | `Size_changed ->
                  let w, h = Sdl.get_window_size win in
                  self#reshape ~w ~h;
                  self#post_redisplay
                | `Exposed | `Hidden | `Shown ->
                  self#post_redisplay
                | _ -> ()
              end
            | _ -> ()
          done;
          if not terminate then loop()
        in
        t0 <- self#elapsed_time;
        loop ();
        self#delete
      end

  end

let load_shader typ filename =
  let name = File.find_or_throw filename in
  let shader_data = read_file name in
  Glutil.compile_shader typ shader_data

let create_program shader_list =
  with_resource ~release:(List.iter Typedgl.Shader.delete) shader_list (fun _l ->
      Typedgl.Program.to_int (Glutil.link_program shader_list)
    )
let mesh filename = new Mesh.mesh filename

let run obj =
  try
    obj#main_loop
  with e ->
    if Printexc.backtrace_status () then begin
      let backtrace = Printexc.get_backtrace () in
      Sdl.log_error Sdl.Log.category_application "%s@." backtrace;
    end;
    Sdl.log_error Sdl.Log.category_application "%s@." (Printexc.to_string e);
    exit 1

