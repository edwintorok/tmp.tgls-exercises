open Gg
module FloatMap = Map.Make(struct
    type t = float
    let compare (x:float) (y:float) = Pervasives.compare x y
  end)

module type S = sig
  type t
  val zero: t
  val add : t -> t -> t
  val sub : t -> t -> t
  val norm : t -> float
  val smul: float -> t -> t
end

module WeightedLinear(M:S) = struct
  type data = {
    data: M.t;
    mutable weight: float
  }

  type t = data array
  let num_segments values =
    if Array.length values = 0 then 0
    else Array.length values - 1

  let interpolate values alpha =
    if Array.length values = 0 then M.zero
    else
      let rec loop segment =
        if segment = Array.length values then
          values.(Array.length values - 1).data
        else if alpha < values.(segment).weight then
          let section_alpha = alpha -. values.(segment-1).weight in
          let section_alpha = section_alpha /.
                              (values.(segment).weight -. values.(segment-1).weight) in
          let inv_sec_alpha = 1.0 -. section_alpha in
          M.add (M.smul inv_sec_alpha values.(segment-1).data)
            (M.smul section_alpha values.(segment).data)
        else loop (segment + 1)
      in
      loop 1
end

module TimedLinear(M:S) = struct
  include WeightedLinear(M)
  type element = {
    value: M.t;
    time: float
  }

  let of_values ?(is_loop = true) data =
    if Array.length data = 0 then [||]
    else
      let values = Array.fold_left (fun accum curr ->
          assert (0.0 <= curr.time && curr.time <= 1.0);
          {
            data = curr.value;
            weight = curr.time;
          } :: accum
        ) [] data in
      let values = Array.of_list (
          if is_loop && values <> [] then
            { data = data.(0).value; weight = data.(0).time } :: values
          else
            values
        ) in
      if Array.length values > 0 then begin
        values.(0).weight <- 0.0;
        values.(Array.length values-1).weight <- 1.0
      end;
      values
end

module ConstVelLinear(M:V) : sig
  type t
  val of_values : ?is_loop:bool -> M.t array -> t
  val interpolate : t -> float -> M.t
end = struct
  include WeightedLinear(M)

  let distance a b = M.norm (M.sub b a)

  (* TODO: test it *)
  let of_values ?(is_loop = true) data =
    if Array.length data = 0 then [||]
    else
      let values = Array.fold_left (fun accum curr ->
          {
            data = curr;
            weight = 0.0;
          } :: accum
        ) [] data in
      let values = Array.of_list (
          if is_loop && values <> [] then
            { data = data.(0); weight = 0.0 } :: values
          else
            values
        ) in
      let rec loop total_dist i =
        if i < Array.length values then begin
          let total_dist = total_dist
                           +. (distance values.(i-1).data values.(i).data) in
          values.(i).weight <- total_dist;
          loop total_dist (i+1)
        end else total_dist
      in
      let total_dist = loop 0.0 1 in
      for i = 1 to Array.length values - 1 do
        values.(i).weight <- values.(i).weight /. total_dist
      done;
      values
end
