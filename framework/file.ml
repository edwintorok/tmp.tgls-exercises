let local_file_dir = "data"

let find_or_throw basename =
  let path1 = Filename.concat local_file_dir basename
  and path2 = Filename.concat Config.global_file_dir basename in
  if Sys.file_exists path1 then path1
  else if Sys.file_exists path2 then path2
  else failwith (Printf.sprintf "Could not find the file %s" basename)

