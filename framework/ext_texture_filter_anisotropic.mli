open Tgl3
val ext_texture_filter_anisotropic_extension_supported : unit -> bool
val gl_texture_max_anistropy_ext: Gl.enum
val gl_max_texture_max_anistropy_ext: Gl.enum

