open Framework
open Glutil
open Tgl3

let vertex_data = ba_float32_of_array [|
    0.75;  0.75; 0.; 1.;
    0.75; -0.75; 0.; 1.;
    -0.75; -0.75; 0.; 1.
  |]

let defaults display_mode = display_mode
let initialize_vertex_buffer () =
  let vertex_buffer_object = get_int (Gl.gen_buffers 1) in
  let bytes = Gl.bigarray_byte_size vertex_data in
  Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
  Gl.buffer_data Gl.array_buffer bytes (Some vertex_data) Gl.static_draw;
  Gl.bind_buffer Gl.array_buffer 0;
  vertex_buffer_object

let init () =
  let vao = get_int (Gl.gen_vertex_arrays 1) in
  Gl.bind_vertex_array vao;
  vao

class tutorial = object
  inherit framework defaults as framework
  val the_program =
    Framework.create_program [
      Framework.load_shader Gl.vertex_shader "02/FragPosition.vert";
      Framework.load_shader Gl.fragment_shader "02/FragPosition.frag"
    ]

  val vertex_buffer_object = initialize_vertex_buffer ()
  val vao = init ()

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear Gl.color_buffer_bit;

    Gl.use_program the_program;

    Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
    Gl.enable_vertex_attrib_array 0;
    Gl.vertex_attrib_pointer 0 4 Gl.float false 0 (`Offset 0);

    Gl.draw_arrays Gl.triangles 0 3;

    Gl.disable_vertex_attrib_array 0;
    Gl.use_program 0;
    framework#swap_buffers;

  method reshape ~w ~h =
    Gl.viewport 0 0 w h

  method keyboard keycode =
    if keycode = 27 then framework#leave_main_loop
end

let () = Framework.run (new tutorial)
