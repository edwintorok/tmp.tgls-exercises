open Framework
open Glutil
open Tgl3

let defaults display_mode = display_mode

let vertex_data = ba_float32_of_array [|
    (* vertex positions *)
    0.0;    0.5; 0.0; 1.0;
    0.5; -0.366; 0.0; 1.0;
    -0.5; -0.366; 0.0; 1.0;
    (* vertex colors *)
    1.0;    0.0; 0.0; 1.0;
    0.0;    1.0; 0.0; 1.0;
    0.0;    0.0; 1.0; 1.0
  |]

let initialize_vertex_buffer () =
  let vertex_buffer_object = get_int (Gl.gen_buffers 1) in
  let bytes = Gl.bigarray_byte_size vertex_data in
  Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
  Gl.buffer_data Gl.array_buffer bytes (Some vertex_data) Gl.static_draw;
  Gl.bind_buffer Gl.array_buffer 0;
  vertex_buffer_object

let init () =
  let vao = get_int (Gl.gen_vertex_arrays 1) in
  Gl.bind_vertex_array vao;
  vao

class tutorial = object
  inherit framework defaults as framework

  (* initialize_program *)
  val the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader "02/VertexColors.vert";
      Framework.load_shader Gl.fragment_shader "02/VertexColors.frag"
    ]

  val vertex_buffer_object = initialize_vertex_buffer ()
  val vao = init ()

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear Gl.color_buffer_bit;

    Gl.use_program the_program;

    Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
    Gl.enable_vertex_attrib_array 0;
    Gl.enable_vertex_attrib_array 1;
    Gl.vertex_attrib_pointer 0 4 Gl.float false 0 (`Offset 0);
    Gl.vertex_attrib_pointer 1 4 Gl.float false 0 (`Offset 48);

    Gl.draw_arrays Gl.triangles 0 3;

    Gl.disable_vertex_attrib_array 0;
    Gl.disable_vertex_attrib_array 1;
    Gl.use_program 0;
    framework#swap_buffers;
    framework#post_redisplay (* not stricly needed *)

  method reshape ~w ~h =
    Gl.viewport 0 0 w h

  method keyboard keycode =
    if keycode = 27 then framework#leave_main_loop

end
let () = Framework.run (new tutorial)
