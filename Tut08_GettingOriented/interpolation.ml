open Framework
open Glutil
open Tgl3
open Gg

type offset_relative = Model_relative | World_relative | Camera_relative

let orients = [|
  (* note: glm::fquat takes w first, but Quat.v takes w last *)
  Quat.v 0.0 0.7071 0.7071 0.0;
  Quat.v 0.5 0.5 0.5 (-0.5);
  Quat.v (-0.02514) (-0.4895) (-0.7892) (-0.3700);
  Quat.v 0.02514 0.4895 0.7892 0.3700;

  Quat.v (-0.4344) 0.3840 (-0.1591) (-0.799);
  Quat.v 0.0410 0.5537 0.5208 0.6483;
  Quat.v 0.0 0.0 1.0 0.0
|]

class animation = object
  val mutable final_orient = 0
  val mutable curr_timer = new Framework.timer TT_INFINITE 0.0

  method update_time =
    curr_timer#update

  method get_orient initial slerp =
    if slerp then
      (* note: this is always the slow slerp
       * TODO: automatically switch to lerp like original code if the angle is
       * small *)
      Quat.slerp initial orients.(final_orient) curr_timer#get_alpha
    else
      Quat.nlerp initial orients.(final_orient) curr_timer#get_alpha

  method start_animation destination duration =
    final_orient <- destination;
    curr_timer <- new Framework.timer TT_SINGLE duration

  method get_final_ix = final_orient
end

class orientation = object
  val mutable is_animating = false
  val mutable curr_orient = 0
  val mutable slerp = false
  val anim = new animation

  method toggle_serp =
    slerp <- not slerp;
    slerp

  method get_orient =
    if is_animating then
      anim#get_orient orients.(curr_orient) slerp
    else
      orients.(curr_orient)

  method is_animating = is_animating

  method update_time =
    if is_animating then begin
      let is_finished = anim#update_time in
      if is_finished then begin
        is_animating <- false;
        curr_orient <- anim#get_final_ix
      end
    end

  method animate_to_orient destination =
    if curr_orient <> destination then begin
      anim#start_animation destination 1.0;
      is_animating <- true
    end
end

let defaults display_mode = display_mode
class tutorial = object(self)
  inherit framework defaults as framework
  val the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader "08/PosColorLocalTransform.vert";
      Framework.load_shader Gl.fragment_shader "08/ColorMultUniform.frag"
    ]
  val mutable position_attrib = -1;
  val mutable color_attrib = -1;

  val mutable model_to_camera_matrix_unif = -1;
  val mutable camera_to_clip_matrix_unif = -1;
  val mutable base_color_unif = -1;

  method private initialize_program =
    position_attrib <- Gl.get_attrib_location the_program "position";
    color_attrib <- Gl.get_attrib_location the_program "position";

    model_to_camera_matrix_unif <- Gl.get_uniform_location the_program
        "modelToCameraMatrix";
    camera_to_clip_matrix_unif <- Gl.get_uniform_location the_program
        "cameraToClipMatrix";
    base_color_unif <- Gl.get_uniform_location the_program
        "baseColor";

  val ship = Framework.mesh "08/Ship.xml"

  val orient_keys = [|'q';'w';'e';'r';'t';'y';'u'|]

  initializer
    self#initialize_program;

    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range 0.0 1.0

  val mutable orientation = Quat.v 1.0 0.0 0.0 0.0
  val orient = new orientation

  method display =
    orient#update_time;
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    let curr_matrix = M4.mul
        (M4.move3 (V3.v 0.0 0.0 (-200.0)))
        (M4.of_quat orient#get_orient) in

    Gl.use_program the_program;

    let matrix = M4.mul curr_matrix (M4.mul
                                       (M4.scale3 (V3.v 3.0 3.0 3.0))
                                       (M4.rot3_zyx (V3.v (Float.rad_of_deg (-90.0)) 0.0 0.0))) in
    Gl.uniform4f base_color_unif 1.0 1.0 1.0 1.0;
    Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
      (value_ptr_m4 matrix);

    ship#render_mesh "tint";

    Gl.use_program 0;
    framework#swap_buffers;
    framework#post_redisplay

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 20.0) ~near:1.0 ~far:600.0 ~aspect in

    Gl.use_program the_program;
    Gl.uniform_matrix4fv camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 camera_to_clip_matrix);
    Gl.use_program 0;
    Gl.viewport 0 0 w h


  method private apply_orientation index =
    if not orient#is_animating then
      orient#animate_to_orient index

  method keyboard keycode =
    if keycode = 27 then begin
      ship#delete;
      framework#leave_main_loop
    end
    else if keycode = 32 then
      if orient#toggle_serp then
        print_endline "Slerp"
      else
        print_endline "Lerp"
    else
      Array.iteri (fun orient key ->
          if Char.code key = keycode then
            self#apply_orientation orient
        ) orient_keys
end
let () = Framework.run (new tutorial)
