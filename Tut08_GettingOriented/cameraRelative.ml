open Framework
open Glutil
open Tgl3
open Gg

type offset_relative = Model_relative | World_relative | Camera_relative
let defaults display_mode = display_mode
class tutorial = object(self)
  inherit framework defaults as framework
  val the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader "08/PosColorLocalTransform.vert";
      Framework.load_shader Gl.fragment_shader "08/ColorMultUniform.frag"
    ]
  val mutable position_attrib = -1;
  val mutable color_attrib = -1;

  val mutable model_to_camera_matrix_unif = -1;
  val mutable camera_to_clip_matrix_unif = -1;
  val mutable base_color_unif = -1;

  method private initialize_program =
    position_attrib <- Gl.get_attrib_location the_program "position";
    color_attrib <- Gl.get_attrib_location the_program "position";

    model_to_camera_matrix_unif <- Gl.get_uniform_location the_program
        "modelToCameraMatrix";
    camera_to_clip_matrix_unif <- Gl.get_uniform_location the_program
        "cameraToClipMatrix";
    base_color_unif <- Gl.get_uniform_location the_program
        "baseColor"

  val ship = Framework.mesh "08/Ship.xml"
  val plane = Framework.mesh "08/UnitPlane.xml"

  initializer
    self#initialize_program;

    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range 0.0 1.0

  val mutable gimbal_angles = V3.v 0.0 0.0 0.0
  val mutable cam_target = V3.v 0.0 10.0 0.0
  val mutable orientation = Quat.v 1.0 0.0 0.0 0.0

  (* in spherical coordinates *)
  val mutable sphere_cam_rel_pos = V3.v 67.5 (-46.0) 150.0

  method private resolve_cam_position =
    let phi = Float.rad_of_deg (V3.x sphere_cam_rel_pos) in
    let theta = Float.rad_of_deg (V3.y sphere_cam_rel_pos +. 90.0) in

    let sin_theta = sin theta in
    let cos_theta = cos theta in
    let cos_phi = cos phi in
    let sin_phi = sin phi in

    let dir_to_camera =
      V3.v (sin_theta *. cos_phi) cos_theta (sin_theta *. sin_phi) in
    V3.add
      (V3.smul (V3.z sphere_cam_rel_pos) dir_to_camera)
      cam_target

  method private calc_look_at_matrix camera_pt look_pt up_pt =
    let look_dir = V3.unit (V3.sub look_pt camera_pt) in
    let up_dir = V3.unit up_pt in

    (* TODO: use M4.rot_map *)

    let right_dir = V3.unit (V3.cross look_dir up_dir) in
    let perp_up_dir = V3.cross right_dir look_dir in

    let rot_mat = M4.of_rows
        (V4.of_v3 right_dir ~w:0.)
        (V4.of_v3 perp_up_dir ~w:0.)
        (V4.of_v3 (V3.neg look_dir) ~w:0.)
        V4.ow
    in

    let trans_mat = M4.of_cols
        V4.ox
        V4.oy
        V4.oz
        (V4.of_v3 (V3.neg camera_pt) ~w:1.0)
    in

    M4.mul rot_mat trans_mat

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    let cam_pos = self#resolve_cam_position in
    let curr_matrix = self#calc_look_at_matrix cam_pos cam_target
        (V3.v 0.0 1.0 0.0) in

    Gl.use_program the_program;

    let matrix = M4.mul curr_matrix (M4.scale3 (V3.v 100.0 1.0 100.0)) in
    Gl.uniform4f base_color_unif 0.2 0.5 0.2 1.0;
    Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
      (value_ptr_m4 matrix);
    plane#render;

    let matrix = M4.mul curr_matrix (M4.mul (M4.mul
                                               (M4.move3 cam_target)
                                               (M4.of_quat orientation))
                                       (M4.rot3_zyx (V3.v (Float.rad_of_deg (-90.0)) 0.0 0.0))) in
    Gl.uniform4f base_color_unif 1.0 1.0 1.0 1.0;
    Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
      (value_ptr_m4 matrix);

    ship#render_mesh "tint";

    Gl.use_program 0;
    framework#swap_buffers;
    framework#post_redisplay

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 20.0) ~near:1.0 ~far:600.0 ~aspect in

    Gl.use_program the_program;
    Gl.uniform_matrix4fv camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 camera_to_clip_matrix);
    Gl.use_program 0;
    Gl.viewport 0 0 w h


  val small_angle_increment = 9.0
  val mutable ioffset = Model_relative

  method private offset_orientation axis ang_deg =
    let ang_rad = Float.rad_of_deg ang_deg in
    let axis = V3.unit axis in
    let axis = V3.smul (sin (ang_rad /. 2.0)) axis in
    let scalar = cos (ang_rad /. 2.0) in

    let offset = Quat.v (V3.x axis) (V3.y axis) (V3.z axis) scalar in
    begin match ioffset with
      | Model_relative ->
        orientation <- Quat.mul orientation offset
      | World_relative ->
        orientation <- Quat.mul offset orientation
      | Camera_relative ->
        let cam_pos = self#resolve_cam_position in
        let cam_mat = self#calc_look_at_matrix cam_pos cam_target (V3.v 0.0 1.0 0.0) in

        let view_quat = Quat.of_m4 cam_mat in
        let inv_view_quat = Quat.conj view_quat in

        let world_quat = Quat.mul (Quat.mul inv_view_quat offset) view_quat in
        orientation <- Quat.mul world_quat orientation
    end;
    orientation <- Quat.unit orientation

  method keyboard keycode =
    if keycode = 27 then begin
      ship#delete;
      plane#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then begin match Char.chr keycode with
      | 'w' -> self#offset_orientation (V3.v 1.0 0.0 0.0) small_angle_increment
      | 's' -> self#offset_orientation (V3.v 1.0 0.0 0.0) (-.small_angle_increment)
      | 'a' -> self#offset_orientation (V3.v 0.0 0.0 1.0) small_angle_increment
      | 'd' -> self#offset_orientation (V3.v 0.0 0.0 1.0) (-.small_angle_increment)
      | 'q' -> self#offset_orientation (V3.v 0.0 1.0 0.0) small_angle_increment
      | 'e' -> self#offset_orientation (V3.v 0.0 1.0 0.0) (-.small_angle_increment)
      | ' ' -> begin
          ioffset <- match ioffset with
            | Model_relative ->
              print_endline "World Relative";
              World_relative
            | World_relative ->
              print_endline "Camera Relative";
              Camera_relative
            | Camera_relative ->
              print_endline "Model Relative";
              Model_relative
        end
      | 'i' ->
        sphere_cam_rel_pos <-
          V3.add sphere_cam_rel_pos (V3.v 0.0 (-11.25) 0.0)
      | 'k' ->
        sphere_cam_rel_pos <-
          V3.add sphere_cam_rel_pos (V3.v 0.0 (11.25) 0.0)
      | 'j' ->
        sphere_cam_rel_pos <-
          V3.add sphere_cam_rel_pos (V3.v (-11.25) 0.0 0.0)
      | 'l' ->
        sphere_cam_rel_pos <-
          V3.add sphere_cam_rel_pos (V3.v (11.25) 0.0 0.0)
      | 'I' ->
        sphere_cam_rel_pos <-
          V3.add sphere_cam_rel_pos (V3.v 0.0 (-1.125) 0.0)
      | 'K' ->
        sphere_cam_rel_pos <-
          V3.add sphere_cam_rel_pos (V3.v 0.0 (1.125) 0.0)
      | 'J' ->
        sphere_cam_rel_pos <-
          V3.add sphere_cam_rel_pos (V3.v (-1.125) 0.0 0.0)
      | 'L' ->
        sphere_cam_rel_pos <-
          V3.add sphere_cam_rel_pos (V3.v (1.125) 0.0 0.0)
      | _ -> ()
    end;
    sphere_cam_rel_pos <- V3.v
        (V3.x sphere_cam_rel_pos)
        (Float.clamp ~min:(-78.75) ~max:10.0 (V3.y sphere_cam_rel_pos))
        (V3.z sphere_cam_rel_pos)
end
let () = Framework.run (new tutorial)
