open Framework
open Glutil
open Tgl3
open Gg

type gimbal_axis = Gimbal_x_axis | Gimbal_y_axis | Gimbal_z_axis
let defaults display_mode = display_mode
class tutorial = object(self)
  inherit framework defaults as framework
  val the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader "08/PosColorLocalTransform.vert";
      Framework.load_shader Gl.fragment_shader "08/ColorMultUniform.frag"
    ]
  val mutable position_attrib = -1;
  val mutable color_attrib = -1;

  val mutable model_to_camera_matrix_unif = -1;
  val mutable camera_to_clip_matrix_unif = -1;
  val mutable base_color_unif = -1;

  method private initialize_program =
    position_attrib <- Gl.get_attrib_location the_program "position";
    color_attrib <- Gl.get_attrib_location the_program "position";

    model_to_camera_matrix_unif <- Gl.get_uniform_location the_program
        "modelToCameraMatrix";
    camera_to_clip_matrix_unif <- Gl.get_uniform_location the_program
        "cameraToClipMatrix";
    base_color_unif <- Gl.get_uniform_location the_program
        "baseColor";

  val gimbals = [|
    Framework.mesh "08/LargeGimbal.xml";
    Framework.mesh "08/MediumGimbal.xml";
    Framework.mesh "08/SmallGimbal.xml"
  |]

  method private gimbals_render = function
    | Gimbal_x_axis ->
      gimbals.(0)#render
    | Gimbal_y_axis ->
      gimbals.(1)#render
    | Gimbal_z_axis ->
      gimbals.(2)#render

  val mutable draw_gimbals = true

  method private draw_gimbal curr_matrix axis base_color =
    if draw_gimbals then begin
      let matrix = begin match axis with
        | Gimbal_x_axis -> curr_matrix
        | Gimbal_y_axis ->
          M4.mul curr_matrix (M4.rot3_zyx (V3.v
                                             (Float.rad_of_deg 90.0) 0.0 (Float.rad_of_deg 90.0)))
        | Gimbal_z_axis ->
          M4.mul curr_matrix (M4.mul
                                (M4.rot3_zyx (V3.v 0. (Float.rad_of_deg 90.0) 0.))
                                (M4.rot3_zyx (V3.v (Float.rad_of_deg 90.0) 0. 0.)))
      end in

      Gl.use_program the_program;
      Gl.uniform4fv base_color_unif 1 (value_ptr_v4 base_color);
      Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
        (value_ptr_m4 matrix);
      self#gimbals_render axis;
      Gl.use_program 0
    end

  val pobject = Framework.mesh "08/Ship.xml"

  initializer
    self#initialize_program;

    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range 0.0 1.0

  val mutable gimbal_angles = V3.v 0.0 0.0 0.0

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    let curr_matrix = M4.mul
        (M4.move3 (V3.v 0.0 0.0 (-200.0)))
        (M4.rot3_zyx (V3.v (Float.rad_of_deg (V3.x gimbal_angles)) 0.0 0.0)) in
    self#draw_gimbal curr_matrix Gimbal_x_axis (V4.v 0.4 0.4 1.0 1.0);
    let curr_matrix = M4.mul curr_matrix
        (M4.rot3_zyx (V3.v 0. (Float.rad_of_deg (V3.y gimbal_angles)) 0.)) in
    self#draw_gimbal curr_matrix Gimbal_y_axis (V4.v 0.0 1.0 0.0 1.0);
    let curr_matrix = M4.mul curr_matrix
        (M4.rot3_zyx (V3.v 0. 0. (Float.rad_of_deg (V3.z gimbal_angles)))) in
    self#draw_gimbal curr_matrix Gimbal_z_axis (V4.v 1.0 0.3 0.3 1.0);

    Gl.use_program the_program;
    let curr_matrix = M4.mul curr_matrix (M4.mul
                                            (M4.scale3 (V3.v 3.0 3.0 3.0))
                                            (M4.rot3_zyx (V3.v (Float.rad_of_deg (-90.0)) 0.0 0.0))) in
    Gl.uniform4f base_color_unif 1.0 1.0 1.0 1.0;
    Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
      (value_ptr_m4 curr_matrix);

    pobject#render_mesh "tint";
    Gl.use_program 0;
    framework#swap_buffers

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 20.0) ~near:1.0 ~far:600.0 ~aspect in

    Gl.use_program the_program;
    Gl.uniform_matrix4fv camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 camera_to_clip_matrix);
    Gl.use_program 0;
    Gl.viewport 0 0 w h

  val standard_angle_increment = 11.25
  val small_angle_increment = 9.0
  method keyboard keycode =
    if keycode = 27 then begin
      Array.iter (fun m -> m#delete) gimbals;
      pobject#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then begin
      let gimbal_add = match Char.chr keycode with
        | 'w' -> V3.v small_angle_increment 0.0 0.0
        | 's' -> V3.v (-.small_angle_increment) 0.0 0.0
        | 'a' -> V3.v 0. small_angle_increment 0.0
        | 'd' -> V3.v 0. (-.small_angle_increment) 0.0
        | 'q' -> V3.v 0. 0.0 small_angle_increment
        | 'e' -> V3.v 0. 0.0 (-.small_angle_increment)
        | ' ' ->
          draw_gimbals <- not draw_gimbals;
          V3.zero
        | _ -> V3.zero in
      gimbal_angles <- V3.add gimbal_angles gimbal_add
    end;
    framework#post_redisplay
end
let () = Framework.run (new tutorial)
