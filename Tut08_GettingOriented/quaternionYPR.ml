open Framework
open Glutil
open Tgl3
open Gg

let defaults display_mode = display_mode
class tutorial = object(self)
  inherit framework defaults as framework
  val the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader "08/PosColorLocalTransform.vert";
      Framework.load_shader Gl.fragment_shader "08/ColorMultUniform.frag"
    ]
  val mutable position_attrib = -1;
  val mutable color_attrib = -1;

  val mutable model_to_camera_matrix_unif = -1;
  val mutable camera_to_clip_matrix_unif = -1;
  val mutable base_color_unif = -1;

  method private initialize_program =
    position_attrib <- Gl.get_attrib_location the_program "position";
    color_attrib <- Gl.get_attrib_location the_program "position";

    model_to_camera_matrix_unif <- Gl.get_uniform_location the_program
        "modelToCameraMatrix";
    camera_to_clip_matrix_unif <- Gl.get_uniform_location the_program
        "cameraToClipMatrix";
    base_color_unif <- Gl.get_uniform_location the_program
        "baseColor";

  val ship = Framework.mesh "08/Ship.xml"

  initializer
    self#initialize_program;

    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range 0.0 1.0

  val mutable gimbal_angles = V3.v 0.0 0.0 0.0
  val mutable orientation = Quat.v 1.0 0.0 0.0 0.0

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    let curr_matrix = M4.mul
        (M4.move3 (V3.v 0.0 0.0 (-200.0)))
        (M4.of_quat orientation) in

    Gl.use_program the_program;
    let curr_matrix = M4.mul curr_matrix (M4.mul
                                            (M4.scale3 (V3.v 3.0 3.0 3.0))
                                            (M4.rot3_zyx (V3.v (Float.rad_of_deg (-90.0)) 0.0 0.0))) in
    Gl.uniform4f base_color_unif 1.0 1.0 1.0 1.0;
    Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
      (value_ptr_m4 curr_matrix);

    ship#render_mesh "tint";
    Gl.use_program 0;
    framework#swap_buffers

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 20.0) ~near:1.0 ~far:600.0 ~aspect in

    Gl.use_program the_program;
    Gl.uniform_matrix4fv camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 camera_to_clip_matrix);
    Gl.use_program 0;
    Gl.viewport 0 0 w h

  val small_angle_increment = 9.0
  val mutable right_multiply = true

  method private offset_orientation axis ang_deg =
    let ang_rad = Float.rad_of_deg ang_deg in
    let axis = V3.unit axis in
    let axis = V3.smul (sin (ang_rad /. 2.0)) axis in
    let scalar = cos (ang_rad /. 2.0) in

    let offset = Quat.v (V3.x axis) (V3.y axis) (V3.z axis) scalar in
    if right_multiply then
      orientation <- Quat.mul orientation offset
    else
      orientation <- Quat.mul offset orientation;

    orientation <- Quat.unit orientation

  method keyboard keycode =
    if keycode = 27 then begin
      ship#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then begin match Char.chr keycode with
      | 'w' -> self#offset_orientation (V3.v 1.0 0.0 0.0) small_angle_increment
      | 's' -> self#offset_orientation (V3.v 1.0 0.0 0.0) (-.small_angle_increment)
      | 'a' -> self#offset_orientation (V3.v 0.0 0.0 1.0) small_angle_increment
      | 'd' -> self#offset_orientation (V3.v 0.0 0.0 1.0) (-.small_angle_increment)
      | 'q' -> self#offset_orientation (V3.v 0.0 1.0 0.0) small_angle_increment
      | 'e' -> self#offset_orientation (V3.v 0.0 1.0 0.0) (-.small_angle_increment)
      | ' ' ->
        right_multiply <- not right_multiply;
        Printf.printf "%s-multiply\n%!" (if right_multiply then "Right" else
                                           "Left")
      | _ -> ()
    end;
    framework#post_redisplay
end
let () = Framework.run (new tutorial)
