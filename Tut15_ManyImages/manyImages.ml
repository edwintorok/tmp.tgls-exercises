open Framework
open Glutil
open Tgl3
open Gg
open Ext_texture_filter_anisotropic

type program_data = {
  the_program: int;
  model_to_camera_matrix_unif: int;
}

let z_near = 1.0
let z_far = 1000.0

let defaults display_mode = display_mode
let projection_block_index = 0
let color_tex_unit = 0

let load_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let projection_block = Gl.get_uniform_block_index the_program "Projection"
  in
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  let color_texture_unif =
    Gl.get_uniform_location the_program "colorTexture" in
  Gl.use_program the_program;
  Gl.uniform1i color_texture_unif color_tex_unit;
  Gl.use_program 0;
  {
    the_program;
    model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "modelToCameraMatrix";
  }

let num_samplers = 6
class tutorial = object(self)
  inherit framework defaults as framework
  val program =
    load_program "15/PT.vert" "15/Tex.frag"
  val mutable checker_texture = -1
  val mutable mipmap_test_texture = -1
  val samplers = get_int_array num_samplers Gl.gen_samplers

  method private create_samplers =
    for i = 0 to num_samplers - 1 do
      Gl.sampler_parameteri samplers.(i) Gl.texture_wrap_s Gl.repeat;
      Gl.sampler_parameteri samplers.(i) Gl.texture_wrap_t Gl.repeat;
    done;
    (* Nearest *)
    Gl.sampler_parameteri samplers.(0) Gl.texture_mag_filter Gl.nearest;
    Gl.sampler_parameteri samplers.(0) Gl.texture_min_filter Gl.nearest;
    (* Linear *)
    Gl.sampler_parameteri samplers.(1) Gl.texture_mag_filter Gl.linear;
    Gl.sampler_parameteri samplers.(1) Gl.texture_min_filter Gl.linear;
    (* Linear mipmap Nearest *)
    Gl.sampler_parameteri samplers.(2) Gl.texture_mag_filter Gl.linear;
    Gl.sampler_parameteri samplers.(2) Gl.texture_min_filter Gl.linear_mipmap_nearest;
    (* Linear mipmap linear *)
    Gl.sampler_parameteri samplers.(3) Gl.texture_mag_filter Gl.linear;
    Gl.sampler_parameteri samplers.(3) Gl.texture_min_filter Gl.linear_mipmap_linear;
    if not (ext_texture_filter_anisotropic_extension_supported ()) then
      failwith "GL_EXT_texture_filter_anisotropic is not supported";
    (* Low anisotropic *)
    Gl.sampler_parameteri samplers.(4) Gl.texture_mag_filter Gl.linear;
    Gl.sampler_parameteri samplers.(4) Gl.texture_min_filter Gl.linear_mipmap_linear;
    Gl.sampler_parameterf samplers.(4) gl_texture_max_anistropy_ext 4.0;
    (* Max anisotropic *)
    let max_aniso = get_float (Gl.get_floatv gl_max_texture_max_anistropy_ext)
    in
    Printf.printf "Maximum anisotropy: %f\n" max_aniso;
    Gl.sampler_parameteri samplers.(5) Gl.texture_mag_filter Gl.linear;
    Gl.sampler_parameteri samplers.(5) Gl.texture_min_filter Gl.linear_mipmap_linear;
    Gl.sampler_parameterf samplers.(5) gl_texture_max_anistropy_ext max_aniso

  method private fill_with_color red green blue width height =
    let num_texels = width * height in
    let ba = Bigarray.(Array1.create int8_unsigned c_layout (num_texels * 3)) in
    for i = 0 to num_texels - 1 do
      ba.{i*3} <- red;
      ba.{i*3 + 1} <- green;
      ba.{i*3 + 2} <- blue
    done;
    ba

  val mipmap_colors = [|
    [|0xff; 0xff; 0x00|];
    [|0xff; 0x00; 0xff|];
    [|0x00; 0xff; 0xff|];
    [|0xff; 0x00; 0x00|];
    [|0x00; 0xff; 0x00|];
    [|0x00; 0x00; 0xff|];
    [|0x00; 0x00; 0x00|];
    [|0xff; 0xff; 0xff|];
  |]

  method private load_mipmap_texture =
    mipmap_test_texture <- get_int (Gl.gen_textures 1);
    Gl.bind_texture Gl.texture_2d mipmap_test_texture;
    let old_align = get_int (Gl.get_integerv Gl.unpack_alignment) in
    Gl.pixel_storei Gl.unpack_alignment 1;
    for mipmap_level = 0 to 7 do
      let width = 128 lsr mipmap_level
      and height = 128 lsr mipmap_level in
      let curr_color = mipmap_colors.(mipmap_level) in
      let buffer =
        self#fill_with_color curr_color.(0) curr_color.(1) curr_color.(2) width height in
      Gl.tex_image2d Gl.texture_2d mipmap_level Gl.rgb8 width height 0
        Gl.rgb Gl.unsigned_byte (`Data buffer)
    done;
    Gl.pixel_storei Gl.unpack_alignment old_align;
    Gl.tex_parameteri Gl.texture_2d Gl.texture_base_level 0;
    Gl.tex_parameteri Gl.texture_2d Gl.texture_max_level 7;
    Gl.bind_texture Gl.texture_2d 0

  method private load_checker_texture =
    let image_set = Dds.load_from_file (File.find_or_throw "15/checker.dds") in
    checker_texture <- get_int (Gl.gen_textures 1);
    Gl.bind_texture Gl.texture_2d checker_texture;
    Array.iteri (fun mipmap_level (image, w, h) ->
        Gl.tex_image2d Gl.texture_2d mipmap_level Gl.rgb8 w h 0 Gl.bgra
          Gl.unsigned_int_8_8_8_8_rev (`Data image)
      ) image_set;
    Gl.tex_parameteri Gl.texture_2d Gl.texture_base_level 0;
    Gl.tex_parameteri Gl.texture_2d Gl.texture_max_level (Array.length image_set - 1)

  val corridor = Framework.mesh "15/Corridor.xml"
  val plane = Framework.mesh "15/BigPlane.xml"
  val mutable projection_uniform_buffer = -1
  val sizeof_projection_block = 16 * 4
  initializer
    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    let depth_z_near = 0.0
    and depth_z_far = 1.0 in

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range depth_z_near depth_z_far;
    Gl.enable Gl.depth_clamp;

    projection_uniform_buffer <- get_int (Gl.gen_buffers 1);
    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer sizeof_projection_block None
      Gl.dynamic_draw;

    Gl.bind_buffer_range Gl.uniform_buffer projection_block_index
      projection_uniform_buffer 0 sizeof_projection_block;
    Gl.bind_buffer Gl.uniform_buffer 0;
    self#load_checker_texture;
    self#load_mipmap_texture;
    self#create_samplers

  val cam_timer = new Framework.timer Framework.TT_LOOP 5.0
  val mutable curr_sampler = 0
  val mutable use_mipmap_texture = false
  val mutable draw_corridor = false

  method private calc_look_at_matrix camera_pt look_pt up_pt =
    let look_dir = V3.unit (V3.sub look_pt camera_pt) in
    let up_dir = V3.unit up_pt in

    (* TODO: use M4.rot_map *)

    let right_dir = V3.unit (V3.cross look_dir up_dir) in
    let perp_up_dir = V3.cross right_dir look_dir in

    let rot_mat = M4.of_rows
        (V4.of_v3 right_dir ~w:0.)
        (V4.of_v3 perp_up_dir ~w:0.)
        (V4.of_v3 (V3.neg look_dir) ~w:0.)
        V4.ow
    in

    let trans_mat = M4.of_cols
        V4.ox
        V4.oy
        V4.oz
        (V4.of_v3 (V3.neg camera_pt) ~w:1.0)
    in

    M4.mul rot_mat trans_mat

  method display =
    Gl.clear_color 0.75 0.75 1.0 1.0;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    ignore (cam_timer#update);
    let cyclic_angle = cam_timer#get_alpha *. 6.28 in
    let h_offset = (cos cyclic_angle) *. 0.25 in
    let v_offset = (sin cyclic_angle) *. 0.25 in

    let world_to_cam_mat = self#calc_look_at_matrix
        (V3.v h_offset 1.0 (-64.0))
        (V3.v h_offset (-5.0 +. v_offset) (-44.0))
        (V3.v 0.0 1.0 0.0) in
    let model_matrix = world_to_cam_mat in
    Gl.use_program program.the_program;
    Gl.uniform_matrix4fv program.model_to_camera_matrix_unif 1 false
      (value_ptr_m4 model_matrix);

    Gl.active_texture (Gl.texture0 + color_tex_unit);
    Gl.bind_texture Gl.texture_2d
      (if use_mipmap_texture then mipmap_test_texture else checker_texture);
    Gl.bind_sampler color_tex_unit samplers.(curr_sampler);

    if draw_corridor then
      corridor#render_mesh "tex"
    else
      plane#render_mesh "tex";
    Gl.bind_sampler color_tex_unit 0;
    Gl.bind_texture Gl.texture_2d 0;
    Gl.use_program 0;
    framework#post_redisplay;
    framework#swap_buffers

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 90.0) ~near:z_near ~far:z_far ~aspect in

    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_sub_data Gl.uniform_buffer 0 sizeof_projection_block
      (Some (value_ptr_m4 camera_to_clip_matrix));
    Gl.bind_buffer Gl.uniform_buffer 0;
    Gl.viewport 0 0 w h;
    framework#post_redisplay

  val sampler_names = [|
    "Nearest";
    "Linear";
    "Linear with nearest mipmaps";
    "Linear with linear mipmaps";
    "Low anisotropic";
    "Max anisotropic"
  |]

  method keyboard keycode =
    if keycode = 27 then begin
      plane#delete;
      corridor#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then begin match Char.chr keycode with
      | ' ' ->
        use_mipmap_texture <- not use_mipmap_texture
      | 'y' ->
        draw_corridor <- not draw_corridor
      | 'p' ->
        ignore (cam_timer#toggle_pause)
      | c when ('1' <= c && c <= '9') ->
        let number = keycode - Char.code '1' in
        if number < num_samplers then begin
          Printf.printf "Sampler: %s\n%!" sampler_names.(number);
          curr_sampler <- number
        end
      | _ -> ()
    end;
    framework#post_redisplay
end
let () = Framework.run (new tutorial)
