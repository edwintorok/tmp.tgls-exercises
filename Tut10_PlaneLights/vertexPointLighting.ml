open Framework
open Glutil
open Tgl3
open Gg
open MousePoles

type program_data = {
  the_program: int;
  light_pos_unif: int;
  light_intensity_unif: int;
  ambient_intensity_unif: int;
  model_to_camera_matrix_unif: int;
  normal_model_to_camera_matrix_unif: int
}

type unlit_prog_data = {
  the_program: int;
  object_color_unif: int;
  model_to_camera_matrix_unif: int;
}

let z_near = 1.0
let z_far = 1000.0

let initial_view_data = {
  target_pos = V3.v 0.0 0.5 0.0;
  orient = Quat.v 0.3826834 0.0 0.0 0.92387953;
  radius = 5.0;
  deg_spin_rotation = 0.0;
}

let view_scale = {
  min_radius = 3.0; max_radius = 20.0;
  large_radius_delta = 1.5; small_radius_delta = 0.5;
  large_pos_offset = 0.0; small_pos_offset = 0.0; (* no camera movement *)
  rotation_scale = 90.0 /. 250.0
}

let initial_object_data = {
  position = V3.v 0.0 0.5 0.0;
  orientation = Quat.v 0.0 0.0 0.0 1.0
}

let defaults display_mode = display_mode
let projection_block_index = 2
let load_lit_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let projection_block = Gl.get_uniform_block_index the_program "Projection"
  in
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  {
    the_program;
    model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "modelToCameraMatrix";
    normal_model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "normalModelToCameraMatrix";
    light_pos_unif =
      Gl.get_uniform_location the_program "lightPos";
    light_intensity_unif =
      Gl.get_uniform_location the_program "lightIntensity";
    ambient_intensity_unif =
      Gl.get_uniform_location the_program "ambientIntensity";
  }

let load_unlit_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let projection_block = Gl.get_uniform_block_index the_program "Projection" in
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  {
    the_program;
    model_to_camera_matrix_unif = Gl.get_uniform_location the_program
        "modelToCameraMatrix";
    object_color_unif = Gl.get_uniform_location the_program "objectColor";
  }

class tutorial = object(self)
  inherit framework defaults as framework
  val white_diffuse_color =
    load_lit_program "10/PosVertexLighting_PN.vert" "09/ColorPassthrough.frag"
  val vertex_diffuse_color =
    load_lit_program "10/PosVertexLighting_PCN.vert" "09/ColorPassthrough.frag"
  val unlit =
    load_unlit_program "10/PosTransform.vert" "10/UniformColor.frag"

  val view_pole = new view_pole initial_view_data view_scale ~action_button:MB_LEFT_BTN
  val objt_pole = new object_pole initial_object_data (90.0/.250.0) ~action_button:MB_RIGHT_BTN None

  method! mouse_motion x y =
    framework#forward_mouse_motion (view_pole :> mouse_pole) x y;
    framework#forward_mouse_motion (objt_pole :> mouse_pole) x y;
    framework#post_redisplay

  method! mouse_button button state x y =
    framework#forward_mouse_button (view_pole :> mouse_pole) button state x y;
    framework#forward_mouse_button (objt_pole :> mouse_pole) button state x y;
    framework#post_redisplay

  method! mouse_wheel wheel direction x y =
    framework#forward_mouse_wheel (view_pole :> mouse_pole) wheel direction x y;
    framework#forward_mouse_wheel (objt_pole :> mouse_pole) wheel direction x y;
    framework#post_redisplay

  val mutable projection_uniform_buffer =0

  val cylinder_mesh = Framework.mesh "10/UnitCylinder.xml"
  val plane_mesh = Framework.mesh "10/LargePlane.xml"
  val cube_mesh = Framework.mesh "10/UnitCube.xml"

  val sizeof_projection_block = 16 * 4 (* size of m4 *)

  initializer
    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range 0.0 1.0;
    Gl.enable Gl.depth_clamp;

    projection_uniform_buffer <- get_int (Gl.gen_buffers 1);
    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer sizeof_projection_block None
      Gl.dynamic_draw;

    Gl.bind_buffer_range Gl.uniform_buffer projection_block_index
      projection_uniform_buffer 0 sizeof_projection_block;
    Gl.bind_buffer Gl.uniform_buffer 0

  val mutable light_height = 1.5
  val mutable light_radius = 1.0
  val light_timer = new timer TT_LOOP 5.0

  method private calc_light_position =
    let curr_time_through_loop = light_timer#get_alpha in
    V4.v
      ((cos (curr_time_through_loop *. Float.pi *. 2.0)) *. light_radius)
      light_height
      ((sin (curr_time_through_loop *. Float.pi *. 2.0)) *. light_radius)
      1.0

  val mutable draw_colored_cyl = false
  val mutable draw_light = false

  method display =
    ignore (light_timer#update);
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    let model_matrix = view_pole#calc_matrix in
    let world_light_pos = self#calc_light_position in
    let light_pos_camera_space = V4.ltr model_matrix world_light_pos in

    Gl.use_program white_diffuse_color.the_program;
    Gl.uniform3fv white_diffuse_color.light_pos_unif 1
      (value_ptr_v3 (V3.of_v4 light_pos_camera_space));
    Gl.use_program vertex_diffuse_color.the_program;
    Gl.uniform3fv vertex_diffuse_color.light_pos_unif 1
      (value_ptr_v3 (V3.of_v4 light_pos_camera_space));

    Gl.use_program white_diffuse_color.the_program;
    Gl.uniform4f white_diffuse_color.light_intensity_unif
      0.8 0.8 0.8 1.0;
    Gl.uniform4f white_diffuse_color.ambient_intensity_unif 0.2 0.2 0.2 1.0;
    Gl.use_program vertex_diffuse_color.the_program;
    Gl.uniform4f vertex_diffuse_color.light_intensity_unif
      0.8 0.8 0.8 1.0;
    Gl.uniform4f vertex_diffuse_color.ambient_intensity_unif 0.2 0.2 0.2 1.0;
    Gl.use_program 0;

    begin
      (* render the ground plane *)
      begin
        Gl.use_program white_diffuse_color.the_program;
        Gl.uniform_matrix4fv white_diffuse_color.model_to_camera_matrix_unif 1
          false (value_ptr_m4 model_matrix);
        let norm_matrix = M3.of_m4 model_matrix in
        Gl.uniform_matrix3fv
          white_diffuse_color.normal_model_to_camera_matrix_unif 1 false
          (value_ptr_m3 norm_matrix);
        plane_mesh#render;
        Gl.use_program 0
      end;

      (* render the cylinder *)
      begin
        let model_matrix = M4.mul model_matrix (objt_pole#calc_matrix) in
        if draw_colored_cyl then begin
          Gl.use_program vertex_diffuse_color.the_program;
          Gl.uniform_matrix4fv
            vertex_diffuse_color.model_to_camera_matrix_unif 1 false
            (value_ptr_m4 model_matrix);
          let norm_matrix = M3.of_m4 model_matrix in
          Gl.uniform_matrix3fv
            vertex_diffuse_color.normal_model_to_camera_matrix_unif 1 false
            (value_ptr_m3 norm_matrix);
          cylinder_mesh#render_mesh "lit-color"
        end else begin
          Gl.use_program white_diffuse_color.the_program;
          Gl.uniform_matrix4fv white_diffuse_color.model_to_camera_matrix_unif
            1 false (value_ptr_m4 model_matrix);
          let norm_matrix = M3.of_m4 model_matrix in
          Gl.uniform_matrix3fv
            white_diffuse_color.normal_model_to_camera_matrix_unif 1 false
            (value_ptr_m3 norm_matrix);
          cylinder_mesh#render_mesh "lit"
        end;
        Gl.use_program 0;
      end;

      (* Render the light *)
      if draw_light then begin
        let model_matrix = M4.mul model_matrix (M4.mul
                                                  (M4.move3 (V3.of_v4 world_light_pos))
                                                  (M4.scale3 (V3.v 0.1 0.1 0.1))) in
        Gl.use_program unlit.the_program;
        Gl.uniform_matrix4fv unlit.model_to_camera_matrix_unif 1 false
          (value_ptr_m4 model_matrix);
        Gl.uniform4f unlit.object_color_unif 0.8078 0.8706 0.9922 1.0;
        cube_mesh#render_mesh "flat";
        Gl.use_program 0
      end
    end;
    framework#post_redisplay;
    framework#swap_buffers

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 45.0) ~near:z_near ~far:z_far ~aspect in

    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_sub_data Gl.uniform_buffer 0 sizeof_projection_block
      (Some (value_ptr_m4 camera_to_clip_matrix));
    Gl.bind_buffer Gl.uniform_buffer 0;
    Gl.viewport 0 0 w h;
    framework#post_redisplay

  method keyboard keycode =
    if keycode = 27 then begin
      cylinder_mesh#delete;
      plane_mesh#delete;
      cube_mesh#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then begin match Char.chr keycode with
      | 'i' -> light_height <- light_height +. 0.2
      | 'k' -> light_height <- light_height -. 0.2
      | 'l' -> light_radius <- light_radius +. 0.2
      | 'j' -> light_radius <- light_radius -. 0.2

      | 'I' -> light_height <- light_height +. 0.05
      | 'K' -> light_height <- light_height -. 0.05
      | 'L' -> light_radius <- light_radius +. 0.05
      | 'J' -> light_radius <- light_radius -. 0.05

      | 'y' -> draw_light <- not draw_light
      | 'b' -> ignore (light_timer#toggle_pause)
      | ' ' -> draw_colored_cyl <- not draw_colored_cyl
      | _ -> ()
    end;
    if light_radius < 0.2 then
      light_radius <- 0.2;
    framework#post_redisplay
end
let () = Framework.run (new tutorial)
