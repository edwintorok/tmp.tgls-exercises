open Framework
open Glutil
open Tgl3
open Gg
open MousePoles

type program_data = {
  the_program: int;
  model_to_camera_matrix_unif: int;
  light_intensity_unif: int;
  ambient_intensity_unif: int;
  normal_model_to_camera_matrix_unif: int;
  camera_space_light_pos_unif: int;
  light_attenuation_unif: int;
  use_r_square_unif: int
}

type unlit_prog_data = {
  the_program: int;
  object_color_unif: int;
  model_to_camera_matrix_unif: int;
}

let z_near = 1.0
let z_far = 1000.0
let depth_z_near = 0.0
let depth_z_far = 1.0

let sizeof_m4 = 16 * 4
let sizeof_i2 = 2 * 4
let sizeof_projection_block = sizeof_m4 (* size of m4 *)
let sizeof_unprojection_block = sizeof_m4 + sizeof_i2 (* m4, v2 *)

let initial_view_data = {
  target_pos = V3.v 0.0 0.5 0.0;
  orient = Quat.v 0.3826834 0.0 0.0 0.92387953;
  radius = 5.0;
  deg_spin_rotation = 0.0;
}

let view_scale = {
  min_radius = 3.0; max_radius = 20.0;
  large_radius_delta = 1.5; small_radius_delta = 0.5;
  large_pos_offset = 0.0; small_pos_offset = 0.0; (* no camera movement *)
  rotation_scale = 90.0 /. 250.0
}

let initial_object_data = {
  position = V3.v 0.0 0.5 0.0;
  orientation = Quat.v 0.0 0.0 0.0 1.0
}

let defaults display_mode = display_mode
let projection_block_index = 2
let unprojection_block_index = 1
let load_lit_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let projection_block = Gl.get_uniform_block_index the_program "Projection"
  in
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  let unprojection_block = Gl.get_uniform_block_index the_program "UnProjection"
  in
  Gl.uniform_block_binding the_program unprojection_block unprojection_block_index;
  {
    the_program;
    model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "modelToCameraMatrix";
    light_intensity_unif =
      Gl.get_uniform_location the_program "lightIntensity";
    ambient_intensity_unif =
      Gl.get_uniform_location the_program "ambientIntensity";
    normal_model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "normalModelToCameraMatrix";
    camera_space_light_pos_unif =
      Gl.get_uniform_location the_program "cameraSpaceLightPos";
    light_attenuation_unif =
      Gl.get_uniform_location the_program "lightAttenuation";
    use_r_square_unif =
      Gl.get_uniform_location the_program "bUseRSquare"
  }

let load_unlit_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let projection_block = Gl.get_uniform_block_index the_program "Projection" in
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  {
    the_program;
    model_to_camera_matrix_unif = Gl.get_uniform_location the_program
        "modelToCameraMatrix";
    object_color_unif = Gl.get_uniform_location the_program "objectColor";
  }

class tutorial = object(self)
  inherit framework defaults as framework
  val frag_white_diffuse_color =
    load_lit_program "10/FragLightAtten_PN.vert" "10/FragLightAtten.frag"
  val frag_vertex_diffuse_color =
    load_lit_program "10/FragLightAtten_PCN.vert" "10/FragLightAtten.frag"
  val unlit =
    load_unlit_program "10/PosTransform.vert" "10/UniformColor.frag"

  val view_pole = new view_pole initial_view_data view_scale ~action_button:MB_LEFT_BTN
  val objt_pole = new object_pole initial_object_data (90.0/.250.0) ~action_button:MB_RIGHT_BTN None

  method! mouse_motion x y =
    framework#forward_mouse_motion (view_pole :> mouse_pole) x y;
    framework#forward_mouse_motion (objt_pole :> mouse_pole) x y;
    framework#post_redisplay

  method! mouse_button button state x y =
    framework#forward_mouse_button (view_pole :> mouse_pole) button state x y;
    framework#forward_mouse_button (objt_pole :> mouse_pole) button state x y;
    framework#post_redisplay

  method! mouse_wheel wheel direction x y =
    framework#forward_mouse_wheel (view_pole :> mouse_pole) wheel direction x y;
    framework#forward_mouse_wheel (objt_pole :> mouse_pole) wheel direction x y;
    framework#post_redisplay

  val mutable projection_uniform_buffer =0
  val mutable unprojection_uniform_buffer =0

  val cylinder_mesh = Framework.mesh "10/UnitCylinder.xml"
  val plane_mesh = Framework.mesh "10/LargePlane.xml"
  val cube_mesh = Framework.mesh "10/UnitCube.xml"

  initializer
    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range depth_z_near depth_z_far;
    Gl.enable Gl.depth_clamp;

    projection_uniform_buffer <- get_int (Gl.gen_buffers 1);
    unprojection_uniform_buffer <- get_int (Gl.gen_buffers 1);
    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer sizeof_projection_block None
      Gl.dynamic_draw;

    Gl.bind_buffer Gl.uniform_buffer unprojection_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer sizeof_unprojection_block None
      Gl.dynamic_draw;

    Gl.bind_buffer_range Gl.uniform_buffer projection_block_index
      projection_uniform_buffer 0 sizeof_projection_block;
    Gl.bind_buffer_range Gl.uniform_buffer unprojection_block_index
      unprojection_uniform_buffer 0 sizeof_unprojection_block;
    Gl.bind_buffer Gl.uniform_buffer 0

  val mutable light_height = 1.5
  val mutable light_radius = 1.0
  val light_timer = new timer TT_LOOP 5.0

  method private calc_light_position =
    let curr_time_through_loop = light_timer#get_alpha in
    V4.v
      ((cos (curr_time_through_loop *. Float.pi *. 2.0)) *. light_radius)
      light_height
      ((sin (curr_time_through_loop *. Float.pi *. 2.0)) *. light_radius)
      1.0

  val mutable use_fragment_lighting = true
  val mutable draw_colored_cyl = false
  val mutable draw_light = false
  val mutable scale_cyl = false
  val mutable use_r_square = false
  val mutable light_attenuation = 1.0

  method display =
    ignore (light_timer#update);
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    let model_matrix = view_pole#calc_matrix in
    let world_light_pos = self#calc_light_position in
    let light_pos_camera_space = V4.ltr model_matrix world_light_pos in

    Gl.use_program frag_white_diffuse_color.the_program;
    Gl.uniform4f frag_white_diffuse_color.light_intensity_unif
      0.8 0.8 0.8 1.0;
    Gl.uniform4f frag_white_diffuse_color.ambient_intensity_unif 0.2 0.2 0.2 1.0;
    Gl.uniform3fv frag_white_diffuse_color.camera_space_light_pos_unif 1
      (value_ptr_v3 (V3.of_v4 light_pos_camera_space));
    Gl.uniform1f frag_white_diffuse_color.light_attenuation_unif
      light_attenuation;
    Gl.uniform1i frag_white_diffuse_color.use_r_square_unif
      (if use_r_square then 1 else 0);

    Gl.use_program frag_vertex_diffuse_color.the_program;
    Gl.uniform4f frag_vertex_diffuse_color.light_intensity_unif
      0.8 0.8 0.8 1.0;
    Gl.uniform4f frag_vertex_diffuse_color.ambient_intensity_unif 0.2 0.2 0.2 1.0;
    Gl.uniform3fv frag_vertex_diffuse_color.camera_space_light_pos_unif 1
      (value_ptr_v3 (V3.of_v4 light_pos_camera_space));
    Gl.uniform1f frag_vertex_diffuse_color.light_attenuation_unif
      light_attenuation;
    Gl.uniform1i frag_vertex_diffuse_color.use_r_square_unif
      (if use_r_square then 1 else 0);
    Gl.use_program 0;

    begin
      (* render the ground plane *)
      begin
        let norm_matrix = M3.transpose (M3.inv (M3.of_m4 model_matrix)) in
        Gl.use_program frag_white_diffuse_color.the_program;
        Gl.uniform_matrix4fv frag_white_diffuse_color.model_to_camera_matrix_unif 1
          false (value_ptr_m4 model_matrix);
        Gl.uniform_matrix3fv
          frag_white_diffuse_color.normal_model_to_camera_matrix_unif 1 false
          (value_ptr_m3 norm_matrix);
        (*        plane_mesh#render;*)
        Gl.use_program 0
      end;

      (* render the cylinder *)
      begin
        let model_matrix = M4.mul model_matrix (objt_pole#calc_matrix) in
        let model_matrix =
          if scale_cyl then
            M4.mul model_matrix (M4.scale3 (V3.v 1.0 1.0 0.2))
          else
            model_matrix in
        let norm_matrix = M3.transpose (M3.inv (M3.of_m4 model_matrix)) in
        if draw_colored_cyl then begin
          Gl.use_program frag_vertex_diffuse_color.the_program;
          Gl.uniform_matrix4fv
            frag_vertex_diffuse_color.model_to_camera_matrix_unif 1 false
            (value_ptr_m4 model_matrix);
          Gl.uniform_matrix3fv
            frag_vertex_diffuse_color.normal_model_to_camera_matrix_unif 1 false
            (value_ptr_m3 norm_matrix);
          cylinder_mesh#render_mesh "lit-color"
        end else begin
          Gl.use_program frag_white_diffuse_color.the_program;
          Gl.uniform_matrix4fv frag_white_diffuse_color.model_to_camera_matrix_unif
            1 false (value_ptr_m4 model_matrix);
          Gl.uniform_matrix3fv
            frag_white_diffuse_color.normal_model_to_camera_matrix_unif 1 false
            (value_ptr_m3 norm_matrix);
          cylinder_mesh#render_mesh "lit"
        end;
        Gl.use_program 0;
      end;

      (* Render the light *)
      if draw_light then begin
        let model_matrix = M4.mul model_matrix (M4.mul
                                                  (M4.move3 (V3.of_v4 world_light_pos))
                                                  (M4.scale3 (V3.v 0.1 0.1 0.1))) in
        Gl.use_program unlit.the_program;
        Gl.uniform_matrix4fv unlit.model_to_camera_matrix_unif 1 false
          (value_ptr_m4 model_matrix);
        Gl.uniform4f unlit.object_color_unif 0.8078 0.8706 0.9922 1.0;
        cube_mesh#render_mesh "flat";
        Gl.use_program 0
      end
    end;
    framework#post_redisplay;
    framework#swap_buffers

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 45.0) ~near:z_near ~far:z_far ~aspect in
    let clip_to_camera_matrix = M4.inv camera_to_clip_matrix in

    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_sub_data Gl.uniform_buffer 0 sizeof_projection_block
      (Some (value_ptr_m4 camera_to_clip_matrix));
    Gl.bind_buffer Gl.uniform_buffer unprojection_uniform_buffer;
    Gl.buffer_sub_data Gl.uniform_buffer 0 sizeof_m4
      (Some (value_ptr_m4 clip_to_camera_matrix));
    Gl.buffer_sub_data Gl.uniform_buffer sizeof_m4 sizeof_i2
      (Some (value_ptr_i2 w h));
    Gl.bind_buffer Gl.uniform_buffer 0;
    Gl.viewport 0 0 w h;
    framework#post_redisplay

  method keyboard keycode =
    if keycode = 27 then begin
      cylinder_mesh#delete;
      plane_mesh#delete;
      cube_mesh#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then begin match Char.chr keycode with
      | 'i' -> light_height <- light_height +. 0.2
      | 'k' -> light_height <- light_height -. 0.2
      | 'l' -> light_radius <- light_radius +. 0.2
      | 'j' -> light_radius <- light_radius -. 0.2

      | 'I' -> light_height <- light_height +. 0.05
      | 'K' -> light_height <- light_height -. 0.05
      | 'L' -> light_radius <- light_radius +. 0.05
      | 'J' -> light_radius <- light_radius -. 0.05

      | 'o' ->
        light_attenuation <- light_attenuation *. 1.5;
        Printf.printf "Atten: %f\n%!" light_attenuation
      | 'u' ->
        light_attenuation <- light_attenuation /. 1.5;
        Printf.printf "Atten: %f\n%!" light_attenuation
      | 'O' ->
        light_attenuation <- light_attenuation *. 1.1;
        Printf.printf "Atten: %f\n%!" light_attenuation
      | 'U' ->
        light_attenuation <- light_attenuation /. 1.1;
        Printf.printf "Atten: %f\n%!" light_attenuation

      | 'y' -> draw_light <- not draw_light
      | 't' -> scale_cyl <- not scale_cyl
      | 'b' -> ignore (light_timer#toggle_pause)
      | ' ' -> draw_colored_cyl <- not draw_colored_cyl
      | 'h' ->
        use_r_square <- not use_r_square;
        if use_r_square then
          print_endline "Inverse Square Attenuation"
        else
          print_endline "Plain Inverse Attenuation"
      | _ -> ()
    end;
    if light_radius < 0.2 then
      light_radius <- 0.2;
    if light_attenuation < 0.1 then
      light_attenuation <- 0.1;
end
let () = Framework.run (new tutorial)
