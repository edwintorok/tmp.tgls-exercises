(* OASIS_START *)
(* DO NOT EDIT (digest: 780d1017aa93cb33d279d15c09c0d57d) *)

tgls-gltut - gltut tutorials translated to OCaml
================================================

The http://arcsynthesis.org/gltut tutorials translated to OCaml, using the
'tgls' OCaml bindings. Differences: - it uses the OCaml 'tsdl' bindings to
SDL 2.x, instead of FreeGLUT - it uses the pure OCaml 'Gg' library, instead
of GLM - it is installable, all executable names are prefixed with gltut - it
attempts to support OpenGL 3.1 too - it accepts different command-line
parameters Conventions: - convert function names to lowercase, and add an
underscore before uppercase (for consistency with tgls/tsdl) - the OCaml
module name should try to match the C++ file name without spaces - global
variables are turned into mutable object fields - each tutorial is one object
that inherits from the framework object - the type prefix on variable names
(b,e,f,str, ...) is dropped - the data/ directory is shared among all
tutorials (makes installing/finding easier) - try to allocate bigarrays only
once - mutable matrixes are bigarrays, with values computed by Gg where
appropriate - the actual tutorials try to stay close to the original C++
code, no attempts are made to make it safer or more functional in style - the
framework only retains the interface, the implementation is different -
constant arrays are created with Framework.ba_float32_of_array - code is
indented by ocp-indent

See the file [INSTALL.txt](INSTALL.txt) for building and installation
instructions.

Copyright and license
---------------------

tgls-gltut is distributed under the terms of the MIT License.

See [COPYING](COPYING) for more information.

(* OASIS_STOP *)
