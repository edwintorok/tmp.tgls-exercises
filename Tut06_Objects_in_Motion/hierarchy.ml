open Framework
open Glutil
open Tgl3
open Gg

class matrix_stack = object(self)
  val mutable curr_mat = M4.id
  val matrices = Stack.create ()

  method top =
    value_ptr_m4 curr_mat

  method private xform m =
    curr_mat <- M4.mul curr_mat m

  method rotate_x ang_deg =
    self#xform (M4.rot3_zyx (V3.v (Float.rad_of_deg ang_deg) 0.0 0.0))

  method rotate_y ang_deg =
    self#xform (M4.rot3_zyx (V3.v 0. (Float.rad_of_deg ang_deg) 0.0))

  method rotate_z ang_deg =
    self#xform (M4.rot3_zyx (V3.v 0. 0. (Float.rad_of_deg ang_deg)))

  method scale scale_vec =
    self#xform (M4.scale3 scale_vec)

  method translate offset_vec =
    self#xform (M4.move3 offset_vec)

  method push =
    Stack.push curr_mat matrices

  method pop =
    curr_mat <- Stack.pop matrices
end

let defaults display_mode = display_mode
class tutorial = object(self)
  inherit framework defaults as framework
  val the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader "06/PosColorLocalTransform.vert";
      Framework.load_shader Gl.fragment_shader "06/ColorPassthrough.frag"
    ];

  val mutable position_attrib = -1
  val mutable color_attrib = -1;

  val mutable model_to_camera_matrix_unif = -1
  val mutable camera_to_clip_matrix_unif = -1

  method private initialize_program =
    position_attrib <- Gl.get_attrib_location the_program "position";
    color_attrib <- Gl.get_attrib_location the_program "color";

    model_to_camera_matrix_unif <-
      Gl.get_uniform_location the_program "modelToCameraMatrix";
    camera_to_clip_matrix_unif <-
      Gl.get_uniform_location the_program "cameraToClipMatrix"

  val number_of_vertices = 24

  val vertex_data = ba_float32_of_array [|
      (* Front *)
      +1.0; +1.0; +1.0;
      +1.0; -1.0; +1.0;
      -1.0; -1.0; +1.0;
      -1.0; +1.0; +1.0;

      (* Top *)
      +1.0; +1.0; +1.0;
      -1.0; +1.0; +1.0;
      -1.0; +1.0; -1.0;
      +1.0; +1.0; -1.0;

      (* Left *)
      +1.0; +1.0; +1.0;
      +1.0; +1.0; -1.0;
      +1.0; -1.0; -1.0;
      +1.0; -1.0; +1.0;

      (* Back *)
      +1.0; +1.0; -1.0;
      -1.0; +1.0; -1.0;
      -1.0; -1.0; -1.0;
      +1.0; -1.0; -1.0;

      (* Bottom *)
      +1.0; -1.0; +1.0;
      +1.0; -1.0; -1.0;
      -1.0; -1.0; -1.0;
      -1.0; -1.0; +1.0;

      (* Right *)
      -1.0; +1.0; +1.0;
      -1.0; -1.0; +1.0;
      -1.0; -1.0; -1.0;
      -1.0; +1.0; -1.0;

      (* green *)
      0.0; 1.0; 0.0; 1.0;
      0.0; 1.0; 0.0; 1.0;
      0.0; 1.0; 0.0; 1.0;
      0.0; 1.0; 0.0; 1.0;

      (* blue *)
      0.0; 0.0; 1.0; 1.0;
      0.0; 0.0; 1.0; 1.0;
      0.0; 0.0; 1.0; 1.0;
      0.0; 0.0; 1.0; 1.0;

      (* red *)
      1.0; 0.0; 0.0; 1.0;
      1.0; 0.0; 0.0; 1.0;
      1.0; 0.0; 0.0; 1.0;
      1.0; 0.0; 0.0; 1.0;

      (* yellow *)
      1.0; 1.0; 0.0; 1.0;
      1.0; 1.0; 0.0; 1.0;
      1.0; 1.0; 0.0; 1.0;
      1.0; 1.0; 0.0; 1.0;

      (* cyan *)
      0.0; 1.0; 1.0; 1.0;
      0.0; 1.0; 1.0; 1.0;
      0.0; 1.0; 1.0; 1.0;
      0.0; 1.0; 1.0; 1.0;

      (* magenta *)
      1.0; 0.0; 1.0; 1.0;
      1.0; 0.0; 1.0; 1.0;
      1.0; 0.0; 1.0; 1.0;
      1.0; 0.0; 1.0; 1.0;
    |]

  val index_data = ba_ushort_of_array [|
      0; 1; 2;
      2; 3; 0;

      4; 5; 6;
      6; 7; 4;

      8; 9; 10;
      10; 11; 8;

      12; 13; 14;
      14; 15; 12;

      16; 17; 18;
      18; 19; 16;

      20; 21; 22;
      22; 23; 20
    |]

  val mutable vertex_buffer_object = -1
  val mutable index_buffer_object = -1
  val mutable vao = -1
  val sizeof_float = 4

  method private initialize_vao =
    vertex_buffer_object <- get_int (Gl.gen_buffers 1);
    let bytes = Gl.bigarray_byte_size vertex_data in
    Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
    Gl.buffer_data Gl.array_buffer bytes (Some vertex_data) Gl.static_draw;
    Gl.bind_buffer Gl.array_buffer 0;

    index_buffer_object <- get_int (Gl.gen_buffers 1);
    let bytes = Gl.bigarray_byte_size index_data in
    Gl.bind_buffer Gl.element_array_buffer index_buffer_object;
    Gl.buffer_data Gl.element_array_buffer bytes (Some index_data) Gl.static_draw;
    Gl.bind_buffer Gl.element_array_buffer 0;

    vao <- get_int (Gl.gen_vertex_arrays 1);
    Gl.bind_vertex_array vao;

    let color_data_offset = sizeof_float * 3 * number_of_vertices in
    Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
    Gl.enable_vertex_attrib_array 0;
    Gl.enable_vertex_attrib_array 1;
    Gl.vertex_attrib_pointer 0 3 Gl.float false 0 (`Offset 0);
    Gl.vertex_attrib_pointer 1 4 Gl.float false 0 (`Offset color_data_offset);
    Gl.bind_buffer Gl.element_array_buffer index_buffer_object;
    Gl.bind_vertex_array 0

  method private hierarchy = object(hier)
    val pos_base = V3.v 3.0 (-5.0) (-40.0)
    val mutable ang_base = -45.0
    val pos_base_left = V3.v 2.0 0.0 0.0
    val pos_base_right = V3.v (-2.0) 0.0 0.0
    val scale_base_z = 3.0
    val mutable ang_upper_arm = -33.75
    val size_upper_arm = 9.0
    val pos_lower_arm = V3.v 0.0 0.0 8.0
    val mutable ang_lower_arm = 146.25
    val len_lower_arm = 5.0
    val width_lower_arm = 1.5
    val pos_wrist = V3.v 0.0 0.0 5.0
    val mutable ang_wrist_roll = 0.0
    val mutable ang_wrist_pitch = 67.5
    val len_wrist = 2.0
    val width_wrist = 2.0
    val pos_left_finger = V3.v 1.0 0.0 1.0
    val pos_right_finger = V3.v (-1.0) 0.0 1.0
    val mutable ang_finger_open = 180.0
    val len_finger = 2.0
    val width_finger = 0.5
    val ang_lower_finger = 45.0

    method draw =
      let model_to_camera_stack = new matrix_stack in

      Gl.use_program the_program;
      Gl.bind_vertex_array vao;

      model_to_camera_stack#translate pos_base;
      model_to_camera_stack#rotate_y ang_base;

      (* draw left base *)
      begin
        model_to_camera_stack#push;
        model_to_camera_stack#translate pos_base_left;
        model_to_camera_stack#scale (V3.v 1.0 1.0 scale_base_z);
        Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
          model_to_camera_stack#top;
        Gl.draw_elements Gl.triangles (Bigarray.Array1.dim index_data)
          Gl.unsigned_short (`Offset 0);
        model_to_camera_stack#pop
      end;

      (* draw right base *)
      begin
        model_to_camera_stack#push;
        model_to_camera_stack#translate pos_base_right;
        model_to_camera_stack#scale (V3.v 1.0 1.0 scale_base_z);
        Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
          model_to_camera_stack#top;
        Gl.draw_elements Gl.triangles (Bigarray.Array1.dim index_data)
          Gl.unsigned_short (`Offset 0);
        model_to_camera_stack#pop
      end;

      (* draw main arm *)
      hier#draw_upper_arm model_to_camera_stack;

      Gl.bind_vertex_array 0;
      Gl.use_program 0

    val standard_angle_increment = 11.25
    val small_angle_increment = 9.0

    method adj_base increment =
      ang_base <-
        mod_float (ang_base
                   +. (if increment then standard_angle_increment
                       else -.standard_angle_increment)) 360.0

    method adj_upper_arm increment =
      ang_upper_arm <-
        Float.clamp ~min:(-90.0) ~max:0.0 (ang_upper_arm
                                           +. (if increment then standard_angle_increment
                                               else -.standard_angle_increment))

    method adj_lower_arm increment =
      ang_lower_arm <-
        Float.clamp ~min:0.0 ~max:146.25 (ang_lower_arm
                                          +. (if increment then standard_angle_increment
                                              else -.standard_angle_increment))

    method adj_wrist_pitch increment =
      ang_wrist_pitch <-
        Float.clamp ~min:0.0 ~max:90.0 (ang_wrist_pitch
                                        +. (if increment then standard_angle_increment
                                            else -.standard_angle_increment))

    method adj_wrist_roll increment =
      ang_wrist_roll <-
        mod_float (ang_wrist_roll
                   +. (if increment then standard_angle_increment
                       else -.standard_angle_increment)) 360.0

    method adj_finger_open increment =
      ang_finger_open <-
        Float.clamp ~min:9.0 ~max:180.0 (ang_finger_open
                                         +. (if increment then small_angle_increment
                                             else -.small_angle_increment))

    method write_pose =
      Format.printf "@[<v>";
      Format.printf "angBase:\t%f@," ang_base;
      Format.printf "angUpperArm:\t%f@," ang_upper_arm;
      Format.printf "angLowerArm:\t%f@," ang_lower_arm;
      Format.printf "angWristPitch:\t%f@," ang_wrist_pitch;
      Format.printf "angWristRoll:\t%f@," ang_wrist_roll;
      Format.printf "angFingerOpen:\t%f@," ang_finger_open;
      Format.printf "@]@."

    method private draw_fingers model_to_camera_stack =
      (* draw left finger *)
      model_to_camera_stack#push;
      model_to_camera_stack#translate pos_left_finger;
      model_to_camera_stack#rotate_y ang_finger_open;

      model_to_camera_stack#push;
      model_to_camera_stack#translate (V3.v 0.0 0.0 (len_finger /. 2.0));
      model_to_camera_stack#scale (V3.v
                                     (width_finger /. 2.0) (width_finger /. 2.0) (len_finger /. 2.0));
      Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
        model_to_camera_stack#top;
      Gl.draw_elements Gl.triangles (Bigarray.Array1.dim index_data)
        Gl.unsigned_short (`Offset 0);
      model_to_camera_stack#pop;

      (* draw left lower finger *)
      begin
        model_to_camera_stack#push;
        model_to_camera_stack#translate (V3.v 0.0 0.0 len_finger);
        model_to_camera_stack#rotate_y (-.ang_lower_finger);

        model_to_camera_stack#push;
        model_to_camera_stack#translate (V3.v 0.0 0.0 (len_finger /. 2.0));
        model_to_camera_stack#scale (V3.v
                                       (width_finger /. 2.0) (width_finger /. 2.0) (len_finger /. 2.0));
        Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
          model_to_camera_stack#top;
        Gl.draw_elements Gl.triangles (Bigarray.Array1.dim index_data)
          Gl.unsigned_short (`Offset 0);
        model_to_camera_stack#pop;

        model_to_camera_stack#pop;
      end;
      model_to_camera_stack#pop;

      (* draw right finger *)
      model_to_camera_stack#push;
      model_to_camera_stack#translate pos_right_finger;
      model_to_camera_stack#rotate_y (-.ang_finger_open);

      model_to_camera_stack#push;
      model_to_camera_stack#translate (V3.v 0.0 0.0 (len_finger /. 2.0));
      model_to_camera_stack#scale (V3.v
                                     (width_finger /. 2.0) (width_finger /. 2.0) (len_finger /. 2.0));
      Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
        model_to_camera_stack#top;
      Gl.draw_elements Gl.triangles (Bigarray.Array1.dim index_data)
        Gl.unsigned_short (`Offset 0);
      model_to_camera_stack#pop;

      (* draw right lower finger *)
      begin
        model_to_camera_stack#push;
        model_to_camera_stack#translate (V3.v 0.0 0.0 len_finger);
        model_to_camera_stack#rotate_y ang_lower_finger;

        model_to_camera_stack#push;
        model_to_camera_stack#translate (V3.v 0.0 0.0 (len_finger /. 2.0));
        model_to_camera_stack#scale (V3.v
                                       (width_finger /. 2.0) (width_finger /. 2.0) (len_finger /. 2.0));
        Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
          model_to_camera_stack#top;
        Gl.draw_elements Gl.triangles (Bigarray.Array1.dim index_data)
          Gl.unsigned_short (`Offset 0);
        model_to_camera_stack#pop;

        model_to_camera_stack#pop;
      end;
      model_to_camera_stack#pop

    method private draw_wrist model_to_camera_stack =
      model_to_camera_stack#push;
      model_to_camera_stack#translate pos_wrist;
      model_to_camera_stack#rotate_z ang_wrist_roll;
      model_to_camera_stack#rotate_x ang_wrist_pitch;

      model_to_camera_stack#push;
      model_to_camera_stack#scale (V3.v
                                     (width_wrist /. 2.0) (width_wrist /. 2.0) (len_wrist /. 2.0));
      Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
        model_to_camera_stack#top;
      Gl.draw_elements Gl.triangles (Bigarray.Array1.dim index_data)
        Gl.unsigned_short (`Offset 0);
      model_to_camera_stack#pop;

      hier#draw_fingers model_to_camera_stack;
      model_to_camera_stack#pop

    method private draw_lower_arm model_to_camera_stack =
      model_to_camera_stack#push;
      model_to_camera_stack#translate pos_lower_arm;
      model_to_camera_stack#rotate_x ang_lower_arm;

      model_to_camera_stack#push;
      model_to_camera_stack#translate (V3.v 0.0 0.0 (len_lower_arm /. 2.0));
      model_to_camera_stack#scale (V3.v
                                     (width_lower_arm /. 2.0) (width_lower_arm /. 2.0) (len_lower_arm /. 2.0));
      Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
        model_to_camera_stack#top;
      Gl.draw_elements Gl.triangles (Bigarray.Array1.dim index_data)
        Gl.unsigned_short (`Offset 0);
      model_to_camera_stack#pop;

      hier#draw_wrist model_to_camera_stack;

      model_to_camera_stack#pop

    method private draw_upper_arm model_to_camera_stack =
      model_to_camera_stack#push;
      model_to_camera_stack#rotate_x ang_upper_arm;

      begin
        model_to_camera_stack#push;
        model_to_camera_stack#translate (V3.v
                                           0.0 0.0 (size_upper_arm /. 2.0 -. 1.0));
        model_to_camera_stack#scale (V3.v 1.0 1.0 (size_upper_arm /. 2.0));
        Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
          model_to_camera_stack#top;
        Gl.draw_elements Gl.triangles (Bigarray.Array1.dim index_data)
          Gl.unsigned_short (`Offset 0);
        model_to_camera_stack#pop;
      end;

      hier#draw_lower_arm model_to_camera_stack;

      model_to_camera_stack#pop
  end

  val mutable armature_ref = None
  method private armature =
    match armature_ref with
    | None -> assert false
    | Some a -> a

  initializer
    armature_ref <- Some self#hierarchy;
    self#initialize_program;
    self#initialize_vao;

    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;
    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range 0.0 1.0

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    self#armature#draw;

    framework#swap_buffers;
    framework#post_redisplay

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 45.) ~near:1.0 ~far:100.0 ~aspect in

    Gl.use_program the_program;
    Gl.uniform_matrix4fv camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 camera_to_clip_matrix);
    Gl.use_program 0;
    Gl.viewport 0 0 w h

  method keyboard keycode =
    if keycode = 27 then framework#leave_main_loop
    else if keycode < 255 then match Char.chr keycode with
      | 'a' -> self#armature#adj_base true
      | 'd' -> self#armature#adj_base false
      | 'w' -> self#armature#adj_upper_arm false
      | 's' -> self#armature#adj_upper_arm true
      | 'r' -> self#armature#adj_lower_arm false
      | 'f' -> self#armature#adj_lower_arm true
      | 't' -> self#armature#adj_wrist_pitch false
      | 'g' -> self#armature#adj_wrist_pitch true
      | 'z' -> self#armature#adj_wrist_roll true
      | 'c' -> self#armature#adj_wrist_roll false
      | 'q' -> self#armature#adj_finger_open true
      | 'e' -> self#armature#adj_finger_open false
      | ' ' -> self#armature#write_pose
      | _ -> ()
end
let () = Framework.run (new tutorial)
