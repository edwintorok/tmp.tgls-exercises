open Framework
open Glutil
open Tgl3
open Gg

let calc_lerp_factor elapsed_time loop_duration =
  let fValue = (mod_float elapsed_time loop_duration) /. loop_duration in
  if fValue > 0.5 then
    2.0 *. (1.0 -. fValue)
  else
    2.0 *. fValue

let null_rotation _elapsed_time =
  M3.id

let compute_angle_rad elapsed_time loop_duration =
  let scale = Float.pi *. 2.0 /. loop_duration in
  let curr_time_through_loop = mod_float elapsed_time loop_duration in
  curr_time_through_loop *. scale

let rotate_x elapsed_time =
  let fAngRad = compute_angle_rad elapsed_time 3.0 in
  let fCos = cos fAngRad
  and fSin = sin fAngRad in

  M3.of_rows
    V3.ox
    (V3.v 0. fCos (-.fSin))
    (V3.v 0. fSin fCos)

let rotate_y elapsed_time =
  let fAngRad = compute_angle_rad elapsed_time 3.0 in
  let fCos = cos fAngRad
  and fSin = sin fAngRad in

  M3.of_rows
    (V3.v     fCos 0. fSin)
    V3.oy
    (V3.v (-.fSin) 0. fCos)

let rotate_z elapsed_time =
  let fAngRad = compute_angle_rad elapsed_time 3.0 in
  let fCos = cos fAngRad
  and fSin = sin fAngRad in

  M3.of_rows
    (V3.v fCos (-.fSin) 0.)
    (V3.v fSin fCos     0.)
    V3.oz

let sq x = x *. x

let rotate_axis elapsed_time =
  let fAngRad = compute_angle_rad elapsed_time 2.0 in
  let fCos = cos fAngRad in
  let fInvCos = 1.0 -. fCos in
  let fSin = sin fAngRad in

  let axis = V3.unit (V3.v 1.0 1.0 1.0) in
  let x2 = sq (V3.x axis) and y2 = sq (V3.y axis) and z2 = sq (V3.z axis) in

  M3.v
    (x2 +. (1.0 -. x2) *. fCos)
    ((V3.x axis) *. (V3.y axis) *. fInvCos -. (V3.z axis) *. fSin)
    ((V3.x axis) *. (V3.z axis) *. fInvCos +. (V3.y axis) *. fSin)

    ((V3.x axis) *. (V3.y axis) *. fInvCos +. (V3.z axis) *. fSin)
    (y2 +. (1.0 -. y2) *. fCos)
    ((V3.y axis) *. (V3.z axis) *. fInvCos -. (V3.x axis) *. fSin)

    ((V3.x axis) *. (V3.z axis) *. fInvCos -. (V3.y axis) *. fSin)
    ((V3.y axis) *. (V3.z axis) *. fInvCos +. (V3.x axis) *. fSin)
    (z2 +. (1.0 -. z2) *. fCos)

type rotation_func = float -> m3
type instance = { calc_rotation: rotation_func; offset: v3 }
let instance calc_rotation offset = { calc_rotation; offset }

let construct_matrix i elapsed_time =
  let rot_matrix = i.calc_rotation elapsed_time in
  M4.of_m3_v3 rot_matrix i.offset

let instance_list = [
  instance null_rotation (V3.v 0.0 0.0 (-25.0));
  instance rotate_x (V3.v (-5.0) (-5.0) (-25.0));
  instance rotate_y (V3.v (-5.0) 5.0 (-25.0));
  instance rotate_z (V3.v 5.0 5.0 (-25.0));
  instance rotate_axis (V3.v 5.0 (-5.0) (-25.0))
]

let defaults display_mode = display_mode
class tutorial = object(self)
  inherit framework defaults as framework
  val the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader "06/PosColorLocalTransform.vert";
      Framework.load_shader Gl.fragment_shader "06/ColorPassthrough.frag"
    ];

  val mutable model_to_camera_matrix_unif = -1
  val mutable camera_to_clip_matrix_unif = -1

  method private initialize_program =
    model_to_camera_matrix_unif <-
      Gl.get_uniform_location the_program "modelToCameraMatrix";
    camera_to_clip_matrix_unif <-
      Gl.get_uniform_location the_program "cameraToClipMatrix"

  val number_of_vertices = 8

  val vertex_data = ba_float32_of_array [|
      +1.0; +1.0; +1.0;
      -1.0; -1.0; +1.0;
      -1.0; +1.0; -1.0;
      +1.0; -1.0; -1.0;

      -1.0; -1.0; -1.0;
      +1.0; +1.0; -1.0;
      +1.0; -1.0; +1.0;
      -1.0; +1.0; +1.0;

      (* green *)
      0.0; 1.0; 0.0; 1.0;
      (* blue *)
      0.0; 0.0; 1.0; 1.0;
      (* red *)
      1.0; 0.0; 0.0; 1.0;
      (* brown *)
      0.5; 0.5; 0.0; 1.0;

      (* green *)
      0.0; 1.0; 0.0; 1.0;
      (* blue *)
      0.0; 0.0; 1.0; 1.0;
      (* red *)
      1.0; 0.0; 0.0; 1.0;
      (* brown *)
      0.5; 0.5; 0.0; 1.0;

    |]

  val index_data = ba_ushort_of_array [|
      0; 1; 2;
      1; 0; 3;
      2; 3; 0;
      3; 2; 1;

      5; 4; 6;
      4; 5; 7;
      7; 6; 4;
      6; 7; 5
    |]

  val mutable vertex_buffer_object = -1
  val mutable index_buffer_object = -1
  val mutable vao = -1

  method private initialize_vertex_buffer =
    vertex_buffer_object <- get_int (Gl.gen_buffers 1);
    let bytes = Gl.bigarray_byte_size vertex_data in
    Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
    Gl.buffer_data Gl.array_buffer bytes (Some vertex_data) Gl.static_draw;
    Gl.bind_buffer Gl.array_buffer 0;

    index_buffer_object <- get_int (Gl.gen_buffers 1);
    let bytes = Gl.bigarray_byte_size index_data in
    Gl.bind_buffer Gl.element_array_buffer index_buffer_object;
    Gl.buffer_data Gl.element_array_buffer bytes (Some index_data) Gl.static_draw;
    Gl.bind_buffer Gl.element_array_buffer 0

  val sizeof_float = 4

  initializer
    self#initialize_program;
    self#initialize_vertex_buffer;

    vao <- get_int (Gl.gen_vertex_arrays 1);
    Gl.bind_vertex_array vao;

    let color_data_offset = sizeof_float * 3 * number_of_vertices in
    Gl.bind_buffer Gl.array_buffer vertex_buffer_object;
    Gl.enable_vertex_attrib_array 0;
    Gl.enable_vertex_attrib_array 1;
    Gl.vertex_attrib_pointer 0 3 Gl.float false 0 (`Offset 0);
    Gl.vertex_attrib_pointer 1 4 Gl.float false 0 (`Offset color_data_offset);
    Gl.bind_buffer Gl.element_array_buffer index_buffer_object;
    Gl.bind_vertex_array 0;

    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range 0.0 1.0

  method display =
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    Gl.use_program the_program;

    Gl.bind_vertex_array vao;

    let elapsed_time = framework#elapsed_time /. 1000. in
    List.iter (fun currInst ->
        let transform_matrix = construct_matrix currInst elapsed_time in
        Gl.uniform_matrix4fv model_to_camera_matrix_unif 1 false
          (value_ptr_m4 transform_matrix);
        Gl.draw_elements Gl.triangles (Bigarray.Array1.dim index_data)
          Gl.unsigned_short (`Offset 0)
      ) instance_list;

    Gl.bind_vertex_array 0;
    Gl.use_program 0;
    framework#swap_buffers;
    framework#post_redisplay

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 45.) ~near:1.0 ~far:61.0 ~aspect in

    Gl.use_program the_program;
    Gl.uniform_matrix4fv camera_to_clip_matrix_unif 1 false
      (value_ptr_m4 camera_to_clip_matrix);
    Gl.viewport 0 0 w h

  method keyboard keycode =
    if keycode = 27 then framework#leave_main_loop
end
let () = Framework.run (new tutorial)
