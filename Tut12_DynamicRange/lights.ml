open Gg
open Framework
module StringMap = Map.Make(String)
type per_light = {
  camera_space_light_pos: V4.t;
  light_intensity: V4.t
}

let number_of_lights = 4
let number_of_point_lights = number_of_lights - 1

type light_block = {
  ambient_intensity: V4.t;
  light_attenuation: float;
  (* padding: float[3] *)
  lights: per_light array
}

let sizeof_float = 4
let sizeof_vec4 = 4 * sizeof_float
let sizeof_per_light = 2 * sizeof_vec4

let sizeof_light_block =
  sizeof_vec4 + sizeof_float*4 + sizeof_per_light * number_of_lights


type light_block_hdr = {
  ambient_intensity: V4.t;
  light_attenuation: float;
  max_intensity: float;
  (* padding: float[2] *)
  lights: per_light array
}

type light_block_gamma = {
  ambient_intensity: V4.t;
  light_attenuation: float;
  max_intensity: float;
  gamma: float;
  (* padding: float[1] *)

  lights: per_light array
}

type sun_light_value_hdr = {
  norm_time: float;
  ambient: V4.t;
  sunlight_intensity: V4.t;
  background_color: V4.t;
  max_intensity: float;
}

type timer_types =
  | TIMER_SUN
  | TIMER_LIGHTS
  | TIMER_ALL

let light_height = 10.5
let light_radius = 70.0

let calc_light_position timer alpha_offset =
  let scale = Float.pi *. 2.0 in
  let time_through_loop = timer#get_alpha +. alpha_offset in
  V4.v
    (cos (time_through_loop *. scale) *. light_radius)
    light_height
    (sin (time_through_loop *. scale) *. light_radius)
    1.0

let half_light_distance = 70.0
let light_attenuation = 1.0 /. (half_light_distance *. half_light_distance)

let distance lhs rhs =
  V3.norm (V3.sub rhs lhs)

module LightInterpolator = Interpolators.ConstVelLinear(V3)
module TimeInterpolatorV4 = Interpolators.TimedLinear(V4)
module TimeInterpolator = Interpolators.TimedLinear(struct
    type t = float
    let add x y = x +. y
    let sub x y = x -. y
    let smul s v = s *. v
    let norm v = v
    let zero = 0.
  end)

class light_manager = object(self)
  val sun_timer = new Framework.timer TT_LOOP 30.0
  val light_intensity = Array.init number_of_point_lights (fun _ ->
      V4.v 0.2 0.2 0.2 1.0)

  val lights = [|
    LightInterpolator.of_values [|
      V3.v (-50.0) 30.0 70.0;
      V3.v (-70.0) 30.0 50.0;
      V3.v (-70.0) 30.0 (-50.0);
      V3.v (-50.0) 30.0 (-70.0);
      V3.v (50.0) 30.0 (-70.0);
      V3.v (70.0) 30.0 (-50.0);
      V3.v (70.0) 30.0 50.0;
      V3.v (50.0) 30.0 70.0;
    |], new Framework.timer TT_LOOP 15.0;
    LightInterpolator.of_values [|
      V3.v 100.0 6.0 75.0;
      V3.v 90.0 8.0 90.0;
      V3.v 75.0 10.0 100.0;
      V3.v 60.0 12.0 90.0;
      V3.v 50.0 14.0 75.0;
      V3.v 60.0 16.0 60.0;
      V3.v 75.0 18.0 50.0;
      V3.v 90.0 20.0 60.0;
      V3.v 100.0 22.0 75.0;
      V3.v 90.0 24.0 90.0;
      V3.v 75.0 26.0 100.0;
      V3.v 60.0 28.0 90.0;
      V3.v 50.0 30.0 75.0;
      V3.v 105.0 9.0 (-70.0);
      V3.v 105.0 10.0 (-90.0);
      V3.v 72.0 20.0 (-90.0);
      V3.v 72.0 22.0 (-70.0);
      V3.v 105.0 32.0 (-70.0);
      V3.v 105.0 34.0 (-90.0);
      V3.v 72.0 44.0 (-90.0);
    |], new Framework.timer TT_LOOP 25.0;
    LightInterpolator.of_values [|
      V3.v (-7.0) 35.0 1.0;
      V3.v 8.0 40.0 (-14.0);
      V3.v (-7.0) 45.0 (-29.0);
      V3.v (-22.0) 50.0 (-14.0);
      V3.v (-7.0) 55.0 1.0;
      V3.v 8.0 60.0 (-14.0);
      V3.v (-7.0) 65.0 (-29.0);
      V3.v (-83.0) 30.0 (-92.0);
      V3.v (-98.0) 27.0 (-77.0);
      V3.v (-83.0) 24.0 (-62.0);
      V3.v (-68.0) 21.0 (-77.0);
      V3.v (-83.0) 18.0 (-92.0);
      V3.v (-98.0) 15.0 (-77.0);
      V3.v (-50.0) 8.0 25.0;
      V3.v (-59.5) 4.0 65.0;
      V3.v (-59.5) 4.0 78.0;
      V3.v (-45.0) 4.0 82.0;
      V3.v (-40.0) 4.0 50.0;
      V3.v (-70.0) 20.0 40.0;
      V3.v (-60.0) 20.0 90.0;
      V3.v (-40.0) 25.0 90.0;
    |], new Framework.timer TT_LOOP 15.0
  |]

  val mutable ambient_interpolator = TimeInterpolatorV4.of_values [||]
  val mutable sunlight_interpolator = TimeInterpolatorV4.of_values [||]
  val mutable background_interpolator = TimeInterpolatorV4.of_values [||]
  val mutable max_intensity_interpolator = TimeInterpolator.of_values [||]

  method set_sunlight_values values size =
    let data = Array.sub values 0 size in
    ambient_interpolator <- TimeInterpolatorV4.(of_values (Array.map (fun value ->
        { value = value.ambient; time = value.norm_time }
      )  data));
    sunlight_interpolator <- TimeInterpolatorV4.(of_values (Array.map (fun value ->
        { value = value.sunlight_intensity; time = value.norm_time }
      )  data));
    background_interpolator <- TimeInterpolatorV4.(of_values (Array.map (fun value ->
        { value = value.background_color; time = value.norm_time }
      )  data));
    max_intensity_interpolator <- TimeInterpolator.(of_values ~is_loop:false [|
        { value = 1.0; time = 0.0 };
      |])

  method set_sunlight_values_hdr values size =
    let data = Array.sub values 0 size in
    ambient_interpolator <- TimeInterpolatorV4.(of_values (Array.map (fun value ->
        { value = value.ambient; time = value.norm_time }
      )  data));
    sunlight_interpolator <- TimeInterpolatorV4.(of_values (Array.map (fun value ->
        { value = value.sunlight_intensity; time = value.norm_time }
      )  data));
    background_interpolator <- TimeInterpolatorV4.(of_values (Array.map (fun value ->
        { value = value.background_color; time = value.norm_time }
      )  data));
    max_intensity_interpolator <- TimeInterpolator.(of_values (Array.map (fun value ->
        { value = value.max_intensity;
          time = value.norm_time }
      ) data))

  val mutable extra_timers = StringMap.empty

  method update_time =
    ignore (sun_timer#update);
    Array.iter (fun (_, timer) -> ignore (timer#update)) lights;
    StringMap.iter (fun _ timer -> ignore (timer#update)) extra_timers

  method toggle_pause timer =
    self#set_pause timer ~pause:(not (self#is_paused timer))

  method set_pause ?(pause = true) timer =
    if timer = TIMER_ALL || timer = TIMER_LIGHTS then begin
      Array.iter (fun (_, timer) -> timer#set_pause pause) lights;
      StringMap.iter (fun _ timer -> timer#set_pause pause) extra_timers
    end;
    if timer = TIMER_ALL || timer = TIMER_SUN then
      ignore (sun_timer#toggle_pause)

  method is_paused = function
    | TIMER_ALL | TIMER_SUN ->
      sun_timer#is_paused
    | TIMER_LIGHTS ->
      let _, timer = lights.(0) in
      timer#is_paused

  method rewind_time timer sec_rewind =
    if timer = TIMER_ALL || timer = TIMER_SUN then
      sun_timer#rewind sec_rewind;
    if timer = TIMER_ALL || timer = TIMER_LIGHTS then begin
      Array.iter (fun (_, timer) -> timer#rewind sec_rewind) lights;
      StringMap.iter (fun _ timer -> timer#rewind sec_rewind) extra_timers
    end

  method fast_forward_time timer sec_ff =
    if timer = TIMER_ALL || timer = TIMER_SUN then
      sun_timer#fast_forward sec_ff;
    if timer = TIMER_ALL || timer = TIMER_LIGHTS then begin
      Array.iter (fun (_, timer) -> timer#fast_forward sec_ff) lights;
      StringMap.iter (fun _ timer -> timer#fast_forward sec_ff) extra_timers
    end

  method get_light_information =
    let ba = Glutil.ba_create (sizeof_light_block / sizeof_float) in
    fun world_to_camera_mat ->
      let lights = Array.init (number_of_point_lights + 1) (fun i ->
          if i = 0 then
            {
              camera_space_light_pos =
                V4.ltr world_to_camera_mat self#get_sunlight_direction;
              light_intensity = TimeInterpolatorV4.interpolate
                  sunlight_interpolator sun_timer#get_alpha
            }
          else
            {
              camera_space_light_pos =
                (let world_light_pos =
                   let interpolator, timer = lights.(i-1) in
                   V4.of_v3 (LightInterpolator.interpolate interpolator
                               timer#get_alpha) ~w:1.0 in
                 V4.ltr world_to_camera_mat world_light_pos);
              light_intensity = light_intensity.(i-1)
            }
        ) in
      let light_data = {
        ambient_intensity = TimeInterpolatorV4.interpolate
            ambient_interpolator (sun_timer#get_alpha);
        light_attenuation = light_attenuation;
        lights
      } in
      V4.iteri (fun i v -> ba.{i} <- v) light_data.ambient_intensity;
      ba.{sizeof_vec4 / 4} <- light_attenuation;
      for lightix = 0 to number_of_lights-1 do
        let light = light_data.lights.(lightix) in
        V4.iteri (fun i v ->  ba.{sizeof_vec4/4 + 4 + lightix * sizeof_per_light / sizeof_float + i} <- v)
          light.camera_space_light_pos;
        V4.iteri (fun i v ->  ba.{(2*sizeof_vec4)/4 + lightix * sizeof_per_light / sizeof_float + 4 + i} <- v)
          light.light_intensity;
      done;
      ba

  method get_light_information_hdr world_to_camera_mat =
    let lights = Array.init (number_of_point_lights + 1) (fun i ->
        if i = 0 then
          {
            camera_space_light_pos =
              V4.ltr world_to_camera_mat self#get_sunlight_direction;
            light_intensity = TimeInterpolatorV4.interpolate
                sunlight_interpolator sun_timer#get_alpha
          }
        else
          {
            camera_space_light_pos =
              (let world_light_pos =
                 let interpolator, timer = lights.(i-1) in
                 V4.of_v3 (LightInterpolator.interpolate interpolator
                             timer#get_alpha) ~w:1.0 in
               V4.ltr world_to_camera_mat world_light_pos);
            light_intensity = light_intensity.(i-1)
          }
      ) in
    {
      ambient_intensity = TimeInterpolatorV4.interpolate
          ambient_interpolator (sun_timer#get_alpha);
      light_attenuation = light_attenuation;
      max_intensity = TimeInterpolator.interpolate
          max_intensity_interpolator (sun_timer#get_alpha);
      lights
    }

  method get_light_information_gamma world_to_camera_mat =
    self#get_light_information_hdr world_to_camera_mat

  method get_sunlight_direction =
    let angle = 2.0 *. Float.pi *. sun_timer#get_alpha in
    V4.ltr
      (M4.rot3_axis (V3.v 0.0 1.0 0.0) 5.0)
      (V4.v
         (sin angle)
         (cos angle)
         0.0
         0.0)

  method get_sunlight_intensity =
    TimeInterpolatorV4.interpolate sunlight_interpolator
      sun_timer#get_alpha

  method get_number_of_point_lights = Array.length lights

  method get_world_light_position light_ix =
    let light, timer = lights.(light_ix) in
    LightInterpolator.interpolate light timer#get_alpha

  method set_point_light_intensity light_ix intensity =
    light_intensity.(light_ix) <- intensity

  method get_point_light_intensity light_ix =
    light_intensity.(light_ix)

  method create_timer name typ duration =
    extra_timers <- StringMap.add name (new Framework.timer typ duration)
        extra_timers

  method get_timer_value name =
    try
      (StringMap.find name extra_timers)#get_alpha
    with Not_found -> -1.0

  method get_background_color =
    TimeInterpolatorV4.interpolate background_interpolator
      sun_timer#get_alpha

  method get_max_intensity =
    TimeInterpolator.interpolate max_intensity_interpolator
      sun_timer#get_alpha

  method get_sun_time = sun_timer#get_alpha

end
