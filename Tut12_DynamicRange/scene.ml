open Gg
open Glutil
open Tgl3

type program_data = {
  the_program: int;
  model_to_camera_matrix_unif: int;
  normal_model_to_camera_matrix_unif: int;
}

type material_block = {
  diffuse_color: V4.t;
  specular_color: V4.t;
  specular_shininess: float;
  (* padding: float[3] *)
}

let sizeof_float = 4
let sizeof_v4 = 4 * sizeof_float
let sizeof_material_block = 3 * sizeof_v4

type lighting_program_types =
  | LP_VERT_COLOR_DIFFUSE_SPECULAR
  | LP_VERT_COLOR_DIFFUSE
  | LP_MTL_COLOR_DIFFUSE_SPECULAR
  | LP_MTL_COLOR_DIFFUSE

let materials = [|
  (* Ground *)
  {
    diffuse_color = V4.v 1.0 1.0 1.0 1.0;
    specular_color = V4.v 0.5 0.5 0.5 1.0;
    specular_shininess = 0.6
  };
  (* Tetrahedron *)
  {
    diffuse_color = V4.v 0.5 0.5 0.5 0.5;
    specular_color = V4.v 0.5 0.5 0.5 1.0;
    specular_shininess = 0.05
  };
  (* Monolith *)
  {
    diffuse_color = V4.v 0.05 0.05 0.05 0.05;
    specular_color = V4.v 0.95 0.95 0.95 1.0;
    specular_shininess = 0.4
  };
  (* Cube *)
  {
    diffuse_color = V4.v 0.5 0.5 0.5 0.5;
    specular_color = V4.v 0.3 0.3 0.3 1.0;
    specular_shininess = 0.1
  };
  (* Cylinder *)
  {
    diffuse_color = V4.v 0.5 0.5 0.5 0.5;
    specular_color = V4.v 0.0 0.0 0.0 1.0;
    specular_shininess = 0.6
  };
  (* Sphere *)
  {
    diffuse_color = V4.v 0.63 0.60 0.02 1.0;
    specular_color = V4.v 0.22 0.2 0.0 1.0;
    specular_shininess = 0.3
  };
|]

class virtual scene = object(self)
  val terrain_mesh = Framework.mesh "12/Ground.xml"
  val cube_mesh = Framework.mesh "12/UnitCube.xml"
  val tetra_mesh = Framework.mesh "12/UnitTetrahedron.xml"
  val cyl_mesh = Framework.mesh "12/UnitCylinder.xml"
  val sphere_mesh = Framework.mesh "12/UnitSphere.xml"

  val mutable size_material_block = 0
  val mutable material_uniform_buffer = 0

  method virtual get_program : lighting_program_types -> program_data

  initializer
    let uniform_buffer_align_size =
      get_int (Gl.get_integerv Gl.uniform_buffer_offset_alignment) in
    size_material_block <-
      sizeof_material_block + uniform_buffer_align_size -
      (sizeof_material_block mod uniform_buffer_align_size);
    let size_material_uniform_buffer =
      size_material_block * (Array.length materials) in
    let ba = ba_create (size_material_uniform_buffer / sizeof_float) in
    Array.iteri (fun mtl material ->
        let idx = mtl * size_material_block / sizeof_float in
        V4.iteri (fun i v ->
            ba.{idx + i} <- v
          ) material.diffuse_color;
        V4.iteri (fun i v ->
            ba.{idx + sizeof_v4/sizeof_float + i} <- v
          ) material.specular_color;
        ba.{idx + 2*sizeof_v4/sizeof_float} <- material.specular_shininess
      ) materials;
    material_uniform_buffer <- get_int (Gl.gen_buffers 1);
    Gl.bind_buffer Gl.uniform_buffer material_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer size_material_uniform_buffer
      (Some ba) Gl.static_draw;
    Gl.bind_buffer Gl.uniform_buffer 0

  method draw model_matrix material_block_index alpha_tetra =
    begin
      (* Render the ground scene. *)
      let model_matrix = M4.mul model_matrix
          (M4.rot3_zyx (V3.v (Float.rad_of_deg (-90.0)) 0.0 0.0)) in
      self#draw_object terrain_mesh (self#get_program LP_VERT_COLOR_DIFFUSE)
        material_block_index 0 model_matrix;
    end;

    (* Render the tetrahedron object. *)
    begin
      let model_matrix = M4.mul model_matrix (M4.mul (M4.mul (M4.mul (M4.mul
                                                                        (M4.move3 (V3.v 75.0 5.0 75.0))
                                                                        (M4.rot3_zyx (V3.v 0.0 (Float.rad_of_deg (360.0 *. alpha_tetra)) 0.0)))
                                                                (M4.scale3 (V3.v 10.0 10.0 10.0)))
                                                        (M4.move3 (V3.v 0.0 (sqrt 2.0) 0.0)))
                                                (M4.rot3_axis (V3.v (-0.707) 0.0 (-0.707)) (Float.rad_of_deg 54.735))) in
      self#draw_object_mesh tetra_mesh "lit-color"
        (self#get_program LP_VERT_COLOR_DIFFUSE_SPECULAR) material_block_index 1
        model_matrix;
    end;

    (* Render the monolith object. *)
    begin
      let model_matrix = M4.mul model_matrix (M4.mul (M4.mul (M4.mul
                                                                (M4.move3 (V3.v 88.0 5.0 (-80.0)))
                                                                (M4.scale3 (V3.v 4.0 4.0 4.0)))
                                                        (M4.scale3 (V3.v 4.0 9.0 1.0)))
                                                (M4.move3 (V3.v 0.0 0.5 0.0))) in
      self#draw_object_mesh cube_mesh "lit"
        (self#get_program LP_MTL_COLOR_DIFFUSE_SPECULAR) material_block_index 2
        model_matrix;
    end;

    (* Render the cube object *)
    begin
      let model_matrix = M4.mul model_matrix (M4.mul (M4.mul (M4.mul
                                                                (M4.move3 (V3.v (-52.5) 14.0 65.0))
                                                                (M4.rot3_zyx (V3.v 0.0 0.0 (Float.rad_of_deg 50.0))))
                                                        (M4.rot3_zyx (V3.v 0.0 (Float.rad_of_deg (-10.0)) 0.0)))
                                                (M4.scale3 (V3.v 20.0 20.0 20.0))) in
      self#draw_object_mesh cube_mesh "lit-color"
        (self#get_program LP_VERT_COLOR_DIFFUSE_SPECULAR) material_block_index 3
        model_matrix;
    end;

    (* Render the cylinder *)
    begin
      let model_matrix = M4.mul model_matrix (M4.mul (M4.mul
                                                        (M4.move3 (V3.v (-7.0) 30.0 (-14.0)))
                                                        (M4.scale3 (V3.v 15.0 55.0 15.0)))
                                                (M4.move3 (V3.v 0.0 0.5 0.0))) in
      self#draw_object_mesh cyl_mesh "lit-color"
        (self#get_program LP_VERT_COLOR_DIFFUSE_SPECULAR) material_block_index 4
        model_matrix;
    end;

    (* Render the sphere *)
    begin
      let model_matrix = M4.mul model_matrix (M4.mul
                                                (M4.move3 (V3.v (-83.0) 14.0 (-77.0)))
                                                (M4.scale3 (V3.v 20.0 20.0 20.0))) in
      self#draw_object_mesh sphere_mesh "lit"
        (self#get_program LP_MTL_COLOR_DIFFUSE_SPECULAR) material_block_index 5
        model_matrix
    end

  method get_cube_mesh = cube_mesh
  method get_sphere_mesh = sphere_mesh

  method private draw_object mesh prog material_block_index mtl_ix model_matrix =
    Gl.bind_buffer_range Gl.uniform_buffer material_block_index
      material_uniform_buffer (mtl_ix * size_material_block)
      sizeof_material_block;
    let norm_matrix = M3.transpose (M3.inv (M3.of_m4 model_matrix)) in

    Gl.use_program prog.the_program;
    Gl.uniform_matrix4fv prog.model_to_camera_matrix_unif 1 false
      (value_ptr_m4 model_matrix);
    Gl.uniform_matrix3fv prog.normal_model_to_camera_matrix_unif 1 false
      (value_ptr_m3 norm_matrix);
    mesh#render;
    Gl.use_program 0;
    Gl.bind_buffer_base Gl.uniform_buffer material_block_index 0

  method private draw_object_mesh mesh name prog material_block_index mtl_ix model_matrix =
    Gl.bind_buffer_range Gl.uniform_buffer material_block_index
      material_uniform_buffer (mtl_ix * size_material_block)
      sizeof_material_block;
    let norm_matrix = M3.transpose (M3.inv (M3.of_m4 model_matrix)) in

    Gl.use_program prog.the_program;
    Gl.uniform_matrix4fv prog.model_to_camera_matrix_unif 1 false
      (value_ptr_m4 model_matrix);
    Gl.uniform_matrix3fv prog.normal_model_to_camera_matrix_unif 1 false
      (value_ptr_m3 norm_matrix);
    mesh#render_mesh name;
    Gl.use_program 0;
    Gl.bind_buffer_base Gl.uniform_buffer material_block_index 0

  method delete =
    terrain_mesh#delete;
    cube_mesh#delete;
    tetra_mesh#delete;
    cyl_mesh#delete;
    sphere_mesh#delete
end
