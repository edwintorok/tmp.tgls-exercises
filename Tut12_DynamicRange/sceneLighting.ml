open Scene
open Framework
open Glutil
open Tgl3
open Gg
open MousePoles

type unlit_prog_data = {
  the_program: int;
  object_color_unif: int;
  model_to_camera_matrix_unif: int;
}

let z_near = 1.0
let z_far = 1000.0

type shaders = {
  vertex_shader: string;
  fragment_shader: string;
}

let shader_files = function
  | LP_VERT_COLOR_DIFFUSE_SPECULAR ->
    {
      vertex_shader = "12/PCN.vert";
      fragment_shader = "12/DiffuseSpecular.frag"
    }
  | LP_VERT_COLOR_DIFFUSE ->
    {
      vertex_shader = "12/PCN.vert";
      fragment_shader = "12/DiffuseOnly.frag"
    }
  | LP_MTL_COLOR_DIFFUSE_SPECULAR ->
    {
      vertex_shader = "12/PN.vert";
      fragment_shader = "12/DiffuseSpecularMtl.frag"
    }
  | LP_MTL_COLOR_DIFFUSE ->
    {
      vertex_shader = "12/PN.vert";
      fragment_shader = "12/DiffuseOnlyMtl.frag"
    }

let defaults display_mode = display_mode
let material_block_index = 0
let light_block_index = 1
let projection_block_index = 2

let load_unlit_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let projection_block = Gl.get_uniform_block_index the_program "Projection" in
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  {
    the_program;
    model_to_camera_matrix_unif = Gl.get_uniform_location the_program
        "modelToCameraMatrix";
    object_color_unif = Gl.get_uniform_location the_program "objectColor";
  }

let load_lit_program { vertex_shader; fragment_shader } =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let material_block = Gl.get_uniform_block_index the_program "Material" in
  let light_block = Gl.get_uniform_block_index the_program "Light" in
  let projection_block = Gl.get_uniform_block_index the_program "Projection"
  in
  if Int32.of_int material_block <> Gl.invalid_index then (* can be optimized out *)
    Gl.uniform_block_binding the_program material_block material_block_index;
  Gl.uniform_block_binding the_program light_block light_block_index;
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  {
    the_program;
    model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "modelToCameraMatrix";
    normal_model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "normalModelToCameraMatrix";
  }


let initial_view_data = {
  target_pos = V3.v (-59.5) 44.0 95.0;
  orient = Quat.v 0.3826834 0.0 0.0 0.92387953;
  radius = 50.0;
  deg_spin_rotation = 0.0;
}

let view_scale = {
  min_radius = 3.0; max_radius = 80.0;
  large_radius_delta = 4.; small_radius_delta = 1.;
  large_pos_offset = 5.0; small_pos_offset = 1.0;
  rotation_scale = 90.0 /. 250.0
}

class tutorial = object(self)
  inherit framework defaults as framework

  val programs =
    let diffuse_specular = load_lit_program
        (shader_files LP_VERT_COLOR_DIFFUSE_SPECULAR)
    and diffuse_only = load_lit_program
        (shader_files LP_VERT_COLOR_DIFFUSE)
    and mtl_diffuse_specular = load_lit_program
        (shader_files LP_MTL_COLOR_DIFFUSE_SPECULAR)
    and mtl_diffuse_only = load_lit_program
        (shader_files LP_MTL_COLOR_DIFFUSE) in
    function
    | LP_VERT_COLOR_DIFFUSE_SPECULAR -> diffuse_specular
    | LP_VERT_COLOR_DIFFUSE -> diffuse_only
    | LP_MTL_COLOR_DIFFUSE_SPECULAR -> mtl_diffuse_specular
    | LP_MTL_COLOR_DIFFUSE -> mtl_diffuse_only
  val unlit =
    load_unlit_program "12/PosTransform.vert" "12/UniformColor.frag"

  val view_pole = new view_pole initial_view_data view_scale ~action_button:MB_LEFT_BTN

  method! mouse_motion x y =
    framework#forward_mouse_motion (view_pole :> mouse_pole) x y;
    framework#post_redisplay

  method! mouse_button button state x y =
    framework#forward_mouse_button (view_pole :> mouse_pole) button state x y;
    framework#post_redisplay

  method! mouse_wheel wheel direction x y =
    framework#forward_mouse_wheel (view_pole :> mouse_pole) wheel direction x y;
    framework#post_redisplay

  val mutable projection_uniform_buffer =0
  val mutable light_uniform_buffer=1
  val sky_daylight_color = V4.v 0.65 0.65 1.0 1.0

  val lights = new Lights.light_manager;

  method private setup_daytime_lighting =
    lights#set_sunlight_values Lights.([|
        { norm_time = 0.0/.24.0; ambient = V4.v 0.2 0.2 0.2 1.0;
          sunlight_intensity= V4.v 0.6 0.6 0.6 1.0;
          background_color = sky_daylight_color;
          max_intensity = 0.0};
        { norm_time = 4.5/.24.0; ambient = V4.v 0.2 0.2 0.2 1.0;
          sunlight_intensity= V4.v 0.6 0.6 0.6 1.0;
          background_color = sky_daylight_color;
          max_intensity = 0.0};
        { norm_time = 6.5/.24.0; ambient = V4.v 0.15 0.05 0.05 1.0;
          sunlight_intensity= V4.v 0.3 0.1 0.1 1.0;
          background_color = V4.v 0.5 0.1 0.1 1.0;
          max_intensity = 0.0};
        { norm_time = 8.0/.24.0; ambient = V4.v 0.0 0.0 0.0 1.0;
          sunlight_intensity= V4.v 0.0 0.0 0.0 1.0;
          background_color = V4.v 0.0 0.0 0.0 1.0;
          max_intensity = 0.0};
        { norm_time = 18.0/.24.0; ambient = V4.v 0.0 0.0 0.0 1.0;
          sunlight_intensity= V4.v 0.0 0.0 0.0 1.0;
          background_color = V4.v 0.0 0.0 0.0 1.0;
          max_intensity = 0.0};
        { norm_time = 19.5/.24.0; ambient = V4.v 0.15 0.05 0.05 1.0;
          sunlight_intensity= V4.v 0.3 0.1 0.1 1.0;
          background_color = V4.v 0.5 0.1 0.1 1.0;
          max_intensity = 0.0};
        { norm_time = 20.5/.24.0; ambient = V4.v 0.2 0.2 0.2 1.0;
          sunlight_intensity= V4.v 0.6 0.6 0.6 1.0;
          background_color = sky_daylight_color;
          max_intensity = 0.0};
      |]) 7;
    lights#set_point_light_intensity 0 (V4.v 0.2 0.2 0.2 1.0);
    lights#set_point_light_intensity 1 (V4.v 0.0 0.0 0.3 1.0);
    lights#set_point_light_intensity 2 (V4.v 0.3 0.0 0.0 1.0)

  method private setup_nighttime_lighting =
    lights#set_sunlight_values Lights.([|
        { norm_time = 0.0/.24.0; ambient = V4.v 0.2 0.2 0.2 1.0;
          sunlight_intensity= V4.v 0.6 0.6 0.6 1.0;
          background_color = sky_daylight_color;
          max_intensity = 0.0};
        { norm_time = 4.5/.24.0; ambient = V4.v 0.2 0.2 0.2 1.0;
          sunlight_intensity= V4.v 0.6 0.6 0.6 1.0;
          background_color = sky_daylight_color;
          max_intensity = 0.0};
        { norm_time = 6.5/.24.0; ambient = V4.v 0.15 0.05 0.05 1.0;
          sunlight_intensity= V4.v 0.3 0.1 0.1 1.0;
          background_color = V4.v 0.5 0.1 0.1 1.0;
          max_intensity = 0.0};
        { norm_time = 8.0/.24.0; ambient = V4.v 0.0 0.0 0.0 1.0;
          sunlight_intensity= V4.v 0.0 0.0 0.0 1.0;
          background_color = V4.v 0.0 0.0 0.0 1.0;
          max_intensity = 0.0};
        { norm_time = 18.0/.24.0; ambient = V4.v 0.0 0.0 0.0 1.0;
          sunlight_intensity= V4.v 0.0 0.0 0.0 1.0;
          background_color = V4.v 0.0 0.0 0.0 1.0;
          max_intensity = 0.0};
        { norm_time = 19.5/.24.0; ambient = V4.v 0.15 0.05 0.05 1.0;
          sunlight_intensity= V4.v 0.3 0.1 0.1 1.0;
          background_color = V4.v 0.5 0.1 0.1 1.0;
          max_intensity = 0.0};
        { norm_time = 20.5/.24.0; ambient = V4.v 0.2 0.2 0.2 1.0;
          sunlight_intensity= V4.v 0.6 0.6 0.6 1.0;
          background_color = sky_daylight_color;
          max_intensity = 0.0};
      |]) 7;
    lights#set_point_light_intensity 0 (V4.v 0.6 0.6 0.6 1.0);
    lights#set_point_light_intensity 1 (V4.v 0.0 0.0 0.7 1.0);
    lights#set_point_light_intensity 2 (V4.v 0.7 0.0 0.0 1.0)

  val sizeof_projection_block = 16 * 4 (* size of m4 *)
  inherit Scene.scene as scene
  method private get_program typ = programs typ

  initializer
    self#setup_daytime_lighting;
    lights#create_timer "tetra" Framework.TT_LOOP 2.5;
    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    let depth_z_near = 0.0
    and depth_z_far = 1.0 in

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range depth_z_near depth_z_far;
    Gl.enable Gl.depth_clamp;

    light_uniform_buffer <- get_int (Gl.gen_buffers 1);
    Gl.bind_buffer Gl.uniform_buffer light_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer Lights.sizeof_light_block None
      Gl.dynamic_draw;

    projection_uniform_buffer <- get_int (Gl.gen_buffers 1);
    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer sizeof_projection_block None
      Gl.dynamic_draw;

    Gl.bind_buffer_range Gl.uniform_buffer light_block_index
      light_uniform_buffer 0 Lights.sizeof_light_block;
    Gl.bind_buffer_range Gl.uniform_buffer projection_block_index
      projection_uniform_buffer 0 sizeof_projection_block;
    Gl.bind_buffer Gl.uniform_buffer 0

  val mutable draw_camera_pos = false
  val mutable draw_lights = true

  method display =
    ignore (lights#update_time);
    let bkg = lights#get_background_color in
    Gl.clear_color (V4.x bkg) (V4.y bkg) (V4.z bkg) (V4.w bkg);
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    let model_matrix = view_pole#calc_matrix in
    let world_to_cam_mat = model_matrix in
    let light_data = lights#get_light_information world_to_cam_mat in

    Gl.bind_buffer Gl.uniform_buffer light_uniform_buffer;
    Gl.buffer_sub_data Gl.uniform_buffer 0 Lights.sizeof_light_block
      (Some light_data);
    Gl.bind_buffer Gl.uniform_buffer 0;

    scene#draw model_matrix material_block_index (lights#get_timer_value
                                                    "tetra");
    begin
      (* render the sun *)
      begin
        let sunlight_dir = lights#get_sunlight_direction in
        let model_matrix = M4.mul model_matrix (M4.mul
                                                  (M4.move3 (V3.of_v4 (V4.smul 500.0 sunlight_dir)))
                                                  (M4.scale3 (V3.v 30.0 30.0 30.0))) in

        Gl.use_program unlit.the_program;
        Gl.uniform_matrix4fv unlit.model_to_camera_matrix_unif 1 false
          (value_ptr_m4 model_matrix);

        let light_color = lights#get_sunlight_intensity in
        Gl.uniform4fv unlit.object_color_unif 1 (value_ptr_v4 light_color);
        scene#get_sphere_mesh#render_mesh "flat";
      end;

      if draw_lights then begin
        for light = 0 to lights#get_number_of_point_lights - 1 do
          let model_matrix = M4.mul model_matrix
              (M4.move3 (lights#get_world_light_position light)) in
          Gl.use_program unlit.the_program;
          Gl.uniform_matrix4fv unlit.model_to_camera_matrix_unif 1 false
            (value_ptr_m4 model_matrix);

          let light_color = lights#get_point_light_intensity light in
          Gl.uniform4fv unlit.object_color_unif 1 (value_ptr_v4 light_color);
          scene#get_cube_mesh#render_mesh "flat";
        done
      end;

      if draw_camera_pos then begin
        let model_matrix = M4.move3 (V3.v 0.0 0.0 (-.view_pole#get_view.radius))
        in
        Gl.disable Gl.depth_test;
        Gl.depth_mask false;
        Gl.use_program unlit.the_program;
        Gl.uniform_matrix4fv unlit.model_to_camera_matrix_unif 1 false
          (value_ptr_m4 model_matrix);
        Gl.uniform4f unlit.object_color_unif 0.25 0.25 0.25 1.0;
        scene#get_cube_mesh#render_mesh "flat";
        Gl.depth_mask true;
        Gl.enable Gl.depth_test;
        Gl.uniform4f unlit.object_color_unif 1.0 1.0 1.0 1.0;
        scene#get_cube_mesh#render_mesh "flat";
      end
    end;
    framework#post_redisplay;
    framework#swap_buffers

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 45.0) ~near:z_near ~far:z_far ~aspect in

    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_sub_data Gl.uniform_buffer 0 sizeof_projection_block
      (Some (value_ptr_m4 camera_to_clip_matrix));
    Gl.bind_buffer Gl.uniform_buffer 0;
    Gl.viewport 0 0 w h;
    framework#post_redisplay

  val mutable timer_mode = Lights.TIMER_ALL
  method keyboard keycode =
    if keycode = 27 then begin
      scene#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then begin match Char.chr keycode with
      | 'p' -> lights#toggle_pause timer_mode
      | '-' -> lights#rewind_time timer_mode 1.0
      | '=' -> lights#fast_forward_time timer_mode 1.0
      | 't' -> draw_camera_pos <- not draw_camera_pos
      | '1' -> timer_mode <- Lights.TIMER_ALL; print_endline "All"
      | '2' -> timer_mode <- Lights.TIMER_SUN; print_endline "Sun"
      | '3' -> timer_mode <- Lights.TIMER_LIGHTS; print_endline "Lights"
      | 'l' -> self#setup_daytime_lighting
      | 'L' -> self#setup_nighttime_lighting
      | ' ' ->
        let sun_alpha = lights#get_sun_time in
        let sun_time_hours = mod_float (sun_alpha *. 24.0 +. 12.0) 24.0 in
        let sun_hours = int_of_float sun_time_hours in
        let sun_time_minutes = (sun_time_hours -. float sun_hours) *. 60.0 in
        let sun_minutes = int_of_float sun_time_minutes in
        Printf.printf "%f %02i:%02i\n%!" sun_alpha sun_hours sun_minutes
      | key ->
        view_pole#char_press key;
    end;
    framework#post_redisplay
end
let () = Framework.run (new tutorial)
