open Framework
open Glutil
open Tgl3
open Gg
open MousePoles

type program_data = {
  the_program: int;
  model_to_camera_matrix_unif: int;
  light_intensity_unif: int;
  ambient_intensity_unif: int;
  normal_model_to_camera_matrix_unif: int;
  camera_space_light_pos_unif: int;
  light_attenuation_unif: int;
  shininess_factor_unif: int;
  base_diffuse_color_unif: int;
}

type unlit_prog_data = {
  the_program: int;
  object_color_unif: int;
  model_to_camera_matrix_unif: int;
}

type lighting_model =
    LM_PHONG_SPECULAR |
    LM_PHONG_ONLY |
    LM_BLINN_SPECULAR |
    LM_BLINN_ONLY |
    LM_GAUSSIAN_SPECULAR |
    LM_GAUSSIAN_ONLY

type program_pair = {
  white_prog: program_data;
  color_prog: program_data;
}

type shader_pairs = {
  white_vert_shader: string;
  color_vert_shader: string;
  fragment_shader: string
}

let z_near = 1.0
let z_far = 1000.0

let initial_view_data = {
  target_pos = V3.v 0.0 0.5 0.0;
  orient = Quat.v 0.3826834 0.0 0.0 0.92387953;
  radius = 5.0;
  deg_spin_rotation = 0.0;
}

let view_scale = {
  min_radius = 3.0; max_radius = 20.0;
  large_radius_delta = 1.5; small_radius_delta = 0.5;
  large_pos_offset = 0.0; small_pos_offset = 0.0; (* no camera movement *)
  rotation_scale = 90.0 /. 250.0
}

let initial_object_data = {
  position = V3.v 0.0 0.5 0.0;
  orientation = Quat.v 0.0 0.0 0.0 1.0
}

let defaults display_mode = display_mode
let projection_block_index = 2
let load_lit_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let projection_block = Gl.get_uniform_block_index the_program "Projection"
  in
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  {
    the_program;
    model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "modelToCameraMatrix";
    light_intensity_unif =
      Gl.get_uniform_location the_program "lightIntensity";
    ambient_intensity_unif =
      Gl.get_uniform_location the_program "ambientIntensity";
    normal_model_to_camera_matrix_unif =
      Gl.get_uniform_location the_program "normalModelToCameraMatrix";
    camera_space_light_pos_unif =
      Gl.get_uniform_location the_program "cameraSpaceLightPos";
    light_attenuation_unif =
      Gl.get_uniform_location the_program "lightAttenuation";
    shininess_factor_unif =
      Gl.get_uniform_location the_program "shininessFactor";
    base_diffuse_color_unif =
      Gl.get_uniform_location the_program "baseDiffuseColor"
  }

let load_unlit_program vertex_shader fragment_shader =
  let the_program = Framework.create_program [
      Framework.load_shader Gl.vertex_shader vertex_shader;
      Framework.load_shader Gl.fragment_shader fragment_shader
    ] in
  let projection_block = Gl.get_uniform_block_index the_program "Projection" in
  Gl.uniform_block_binding the_program projection_block projection_block_index;
  {
    the_program;
    model_to_camera_matrix_unif = Gl.get_uniform_location the_program
        "modelToCameraMatrix";
    object_color_unif = Gl.get_uniform_location the_program "objectColor";
  }

let shader_files = function
  | LM_PHONG_SPECULAR ->
    {
      white_vert_shader = "11/PN.vert";
      color_vert_shader = "11/PCN.vert";
      fragment_shader = "11/PhongLighting.frag"
    }
  | LM_PHONG_ONLY ->
    {
      white_vert_shader = "11/PN.vert";
      color_vert_shader = "11/PCN.vert";
      fragment_shader = "11/PhongOnly.frag"
    }
  | LM_BLINN_SPECULAR ->
    {
      white_vert_shader = "11/PN.vert";
      color_vert_shader = "11/PCN.vert";
      fragment_shader = "11/BlinnLighting.frag"
    }
  | LM_BLINN_ONLY ->
    {
      white_vert_shader = "11/PN.vert";
      color_vert_shader = "11/PCN.vert";
      fragment_shader = "11/BlinnOnly.frag"
    }
  | LM_GAUSSIAN_SPECULAR ->
    {
      white_vert_shader = "11/PN.vert";
      color_vert_shader = "11/PCN.vert";
      fragment_shader = "11/GaussianLighting.frag"
    }
  | LM_GAUSSIAN_ONLY ->
    {
      white_vert_shader = "11/PN.vert";
      color_vert_shader = "11/PCN.vert";
      fragment_shader = "11/GaussianOnly.frag"
    }


let load_programs model =
  let files = shader_files model in
  {
    white_prog = load_lit_program files.white_vert_shader files.fragment_shader;
    color_prog = load_lit_program files.color_vert_shader files.fragment_shader;
  }

class material_params initial_light_model = object(self)
  val phong_exponent = ref 4.0
  val blinn_exponent = ref 4.0
  val gaussian_roughness = ref 0.5
  val mutable light_model = initial_light_model

  method switch_light_model =
    light_model <- match light_model with
      | LM_PHONG_SPECULAR ->
        LM_BLINN_SPECULAR
      | LM_PHONG_ONLY ->
        LM_BLINN_ONLY
      | LM_BLINN_SPECULAR ->
        LM_GAUSSIAN_SPECULAR
      | LM_BLINN_ONLY ->
        LM_GAUSSIAN_ONLY
      | LM_GAUSSIAN_SPECULAR ->
        LM_PHONG_SPECULAR
      | LM_GAUSSIAN_ONLY ->
        LM_PHONG_ONLY

  method toggle_specular =
    light_model <- match light_model with
      | LM_PHONG_SPECULAR ->
        LM_PHONG_ONLY
      | LM_PHONG_ONLY ->
        LM_PHONG_SPECULAR
      | LM_BLINN_SPECULAR ->
        LM_BLINN_ONLY
      | LM_BLINN_ONLY ->
        LM_BLINN_SPECULAR
      | LM_GAUSSIAN_SPECULAR ->
        LM_GAUSSIAN_ONLY
      | LM_GAUSSIAN_ONLY ->
        LM_GAUSSIAN_SPECULAR

  method light_model_name = match light_model with
    | LM_PHONG_SPECULAR ->
      "Phong Specular.";
    | LM_PHONG_ONLY ->
      "Phong only.";
    | LM_BLINN_SPECULAR ->
      "Blinn Specular.";
    | LM_BLINN_ONLY ->
      "Blinn only.";
    | LM_GAUSSIAN_SPECULAR ->
      "Gaussian Specular.";
    | LM_GAUSSIAN_ONLY ->
      "Gaussian only.";

  method is_gaussian_light_model =
    light_model == LM_GAUSSIAN_SPECULAR || light_model == LM_GAUSSIAN_ONLY

  method light_model = light_model

  method value = !(self#get_specular_value)

  method increment is_large =
    let the_param = self#get_specular_value in
    if self#is_gaussian_light_model then begin
      if is_large then
        the_param := !the_param +. 0.1
      else
        the_param := !the_param +. 0.01
    end else begin
      if is_large then
        the_param := !the_param +. 0.5
      else
        the_param := !the_param +. 0.1
    end;
    self#clamp_param

  method decrement is_large =
    let the_param = self#get_specular_value in
    if self#is_gaussian_light_model then begin
      if is_large then
        the_param := !the_param -. 0.1
      else
        the_param := !the_param -. 0.01
    end else begin
      if is_large then
        the_param := !the_param -. 0.5
      else
        the_param := !the_param -. 0.1
    end;
    self#clamp_param

  method private get_specular_value =
    match light_model with
    | LM_PHONG_SPECULAR
    | LM_PHONG_ONLY ->
      phong_exponent
    | LM_BLINN_SPECULAR
    | LM_BLINN_ONLY ->
      blinn_exponent
    | LM_GAUSSIAN_SPECULAR
    | LM_GAUSSIAN_ONLY ->
      gaussian_roughness

  method private clamp_param =
    let the_param = self#get_specular_value in
    if self#is_gaussian_light_model then
      the_param := Float.clamp ~min:0.00001 ~max:1.0 !the_param
    else
    if !the_param <= 0.0 then
      the_param := 0.0001
end

class tutorial = object(self)
  inherit framework defaults as framework
  val programs =
    let phong = load_programs LM_PHONG_SPECULAR
    and phong_only = load_programs LM_PHONG_ONLY
    and blinn = load_programs LM_BLINN_SPECULAR
    and blinn_only = load_programs LM_BLINN_ONLY
    and gauss = load_programs LM_GAUSSIAN_SPECULAR
    and gauss_only = load_programs LM_GAUSSIAN_ONLY in
    function
    | LM_PHONG_SPECULAR -> phong
    | LM_PHONG_ONLY -> phong_only
    | LM_BLINN_SPECULAR -> blinn
    | LM_BLINN_ONLY -> blinn_only
    | LM_GAUSSIAN_SPECULAR -> gauss
    | LM_GAUSSIAN_ONLY -> gauss_only

  val unlit =
    load_unlit_program "10/PosTransform.vert" "10/UniformColor.frag"

  val view_pole = new view_pole initial_view_data view_scale ~action_button:MB_LEFT_BTN
  val objt_pole = new object_pole initial_object_data (90.0/.250.0) ~action_button:MB_RIGHT_BTN None

  method! mouse_motion x y =
    framework#forward_mouse_motion (view_pole :> mouse_pole) x y;
    framework#forward_mouse_motion (objt_pole :> mouse_pole) x y;
    framework#post_redisplay

  method! mouse_button button state x y =
    framework#forward_mouse_button (view_pole :> mouse_pole) button state x y;
    framework#forward_mouse_button (objt_pole :> mouse_pole) button state x y;
    framework#post_redisplay

  method! mouse_wheel wheel direction x y =
    framework#forward_mouse_wheel (view_pole :> mouse_pole) wheel direction x y;
    framework#forward_mouse_wheel (objt_pole :> mouse_pole) wheel direction x y;
    framework#post_redisplay

  val mutable projection_uniform_buffer =0

  val cylinder_mesh = Framework.mesh "10/UnitCylinder.xml"
  val plane_mesh = Framework.mesh "10/LargePlane.xml"
  val cube_mesh = Framework.mesh "10/UnitCube.xml"

  val sizeof_projection_block = 16 * 4 (* size of m4 *)

  initializer
    Gl.enable Gl.cull_face_enum;
    Gl.cull_face Gl.back;
    Gl.front_face Gl.cw;

    let depth_z_near = 0.0
    and depth_z_far = 1.0 in

    Gl.enable Gl.depth_test;
    Gl.depth_mask true;
    Gl.depth_func Gl.lequal;
    Gl.depth_range depth_z_near depth_z_far;
    Gl.enable Gl.depth_clamp;

    projection_uniform_buffer <- get_int (Gl.gen_buffers 1);
    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_data Gl.uniform_buffer sizeof_projection_block None
      Gl.dynamic_draw;

    Gl.bind_buffer_range Gl.uniform_buffer projection_block_index
      projection_uniform_buffer 0 sizeof_projection_block;
    Gl.bind_buffer Gl.uniform_buffer 0

  val mutable light_height = 1.5
  val mutable light_radius = 1.0
  val light_timer = new timer TT_LOOP 5.0

  method private calc_light_position =
    let curr_time_through_loop = light_timer#get_alpha in
    V4.v
      ((cos (curr_time_through_loop *. Float.pi *. 2.0)) *. light_radius)
      light_height
      ((sin (curr_time_through_loop *. Float.pi *. 2.0)) *. light_radius)
      1.0

  val mutable use_fragment_lighting = true
  val mutable draw_colored_cyl = false
  val mutable draw_light_source = false
  val mutable scale_cyl = false
  val mutable draw_dark = false

  val mutable light_attenuation = 1.2

  val dark_color = V4.v 0.2 0.2 0.2 1.0
  val light_color = V4.v 1.0 1.0 1.0 1.0
  val mat_params = new material_params LM_GAUSSIAN_SPECULAR

  method display =
    ignore (light_timer#update);
    Gl.clear_color 0. 0. 0. 0.;
    Gl.clear_depth 1.0;
    Gl.clear Gl.(color_buffer_bit + depth_buffer_bit);

    let model_matrix = view_pole#calc_matrix in
    let world_light_pos = self#calc_light_position in
    let light_pos_camera_space = V4.ltr model_matrix world_light_pos in

    let p = programs mat_params#light_model in
    let white_prog, color_prog = p.white_prog, p.color_prog in
    Gl.use_program white_prog.the_program;
    Gl.uniform4f white_prog.light_intensity_unif  0.8 0.8 0.8 1.0;
    Gl.uniform4f white_prog.ambient_intensity_unif 0.2 0.2 0.2 1.0;
    Gl.uniform3fv white_prog.camera_space_light_pos_unif 1
      (value_ptr_v3 (V3.of_v4 light_pos_camera_space));
    Gl.uniform1f white_prog.light_attenuation_unif light_attenuation;
    Gl.uniform1f white_prog.shininess_factor_unif mat_params#value;
    Gl.uniform4fv white_prog.base_diffuse_color_unif 1
      (value_ptr_v4 (if draw_dark then dark_color else light_color));

    Gl.use_program color_prog.the_program;
    Gl.uniform4f color_prog.light_intensity_unif  0.8 0.8 0.8 1.0;
    Gl.uniform4f color_prog.ambient_intensity_unif 0.2 0.2 0.2 1.0;
    Gl.uniform3fv color_prog.camera_space_light_pos_unif 1
      (value_ptr_v3 (V3.of_v4 light_pos_camera_space));
    Gl.uniform1f color_prog.light_attenuation_unif light_attenuation;
    Gl.uniform1f color_prog.shininess_factor_unif mat_params#value;
    Gl.use_program 0;

    begin
      (* render the ground plane *)
      begin
        let norm_matrix = M3.transpose (M3.inv (M3.of_m4 model_matrix)) in
        Gl.use_program white_prog.the_program;
        Gl.uniform_matrix4fv white_prog.model_to_camera_matrix_unif 1
          false (value_ptr_m4 model_matrix);
        Gl.uniform_matrix3fv white_prog.normal_model_to_camera_matrix_unif 1
          false (value_ptr_m3 norm_matrix);
        plane_mesh#render;
        Gl.use_program 0
      end;

      (* render the cylinder *)
      begin
        let model_matrix = M4.mul model_matrix (objt_pole#calc_matrix) in
        let model_matrix =
          if scale_cyl then
            M4.mul model_matrix (M4.scale3 (V3.v 1.0 1.0 0.2))
          else
            model_matrix in
        let norm_matrix = M3.transpose (M3.inv (M3.of_m4 model_matrix)) in
        let prog = if draw_colored_cyl then color_prog else white_prog in
        Gl.use_program prog.the_program;
        Gl.uniform_matrix4fv prog.model_to_camera_matrix_unif 1 false
          (value_ptr_m4 model_matrix);
        Gl.uniform_matrix3fv prog.normal_model_to_camera_matrix_unif 1 false
          (value_ptr_m3 norm_matrix);
        if draw_colored_cyl then begin
          cylinder_mesh#render_mesh "lit-color"
        end else begin
          cylinder_mesh#render_mesh "lit"
        end;
        Gl.use_program 0;
      end;

      (* Render the light *)
      if draw_light_source then begin
        let model_matrix = M4.mul model_matrix (M4.mul
                                                  (M4.move3 (V3.of_v4 world_light_pos))
                                                  (M4.scale3 (V3.v 0.1 0.1 0.1))) in
        Gl.use_program unlit.the_program;
        Gl.uniform_matrix4fv unlit.model_to_camera_matrix_unif 1 false
          (value_ptr_m4 model_matrix);
        Gl.uniform4f unlit.object_color_unif 0.8078 0.8706 0.9922 1.0;
        cube_mesh#render_mesh "flat";
        Gl.use_program 0
      end
    end;
    framework#post_redisplay;
    framework#swap_buffers

  method reshape ~w ~h =
    let aspect = float w /. float h in
    let camera_to_clip_matrix =
      persp_fov ~fov:(Float.rad_of_deg 45.0) ~near:z_near ~far:z_far ~aspect in

    Gl.bind_buffer Gl.uniform_buffer projection_uniform_buffer;
    Gl.buffer_sub_data Gl.uniform_buffer 0 sizeof_projection_block
      (Some (value_ptr_m4 camera_to_clip_matrix));
    Gl.bind_buffer Gl.uniform_buffer 0;
    Gl.viewport 0 0 w h;
    framework#post_redisplay

  method keyboard keycode =
    if keycode = 27 then begin
      cylinder_mesh#delete;
      plane_mesh#delete;
      cube_mesh#delete;
      framework#leave_main_loop
    end
    else if keycode < 255 then begin match Char.chr keycode with
      | 'i' -> light_height <- light_height +. 0.2
      | 'k' -> light_height <- light_height -. 0.2
      | 'l' -> light_radius <- light_radius +. 0.2
      | 'j' -> light_radius <- light_radius -. 0.2

      | 'I' -> light_height <- light_height +. 0.05
      | 'K' -> light_height <- light_height -. 0.05
      | 'L' -> light_radius <- light_radius +. 0.05
      | 'J' -> light_radius <- light_radius -. 0.05

      | 'o' ->
        mat_params#increment true;
        Printf.printf "Shiny: %f\n%!" mat_params#value
      | 'u' ->
        mat_params#decrement true;
        Printf.printf "Shiny: %f\n%!" mat_params#value
      | 'O' ->
        mat_params#increment false;
        Printf.printf "Shiny: %f\n%!" mat_params#value
      | 'U' ->
        mat_params#decrement false;
        Printf.printf "Shiny: %f\n%!" mat_params#value

      | 'y' -> draw_light_source <- not draw_light_source
      | 't' -> scale_cyl <- not scale_cyl
      | 'b' -> ignore (light_timer#toggle_pause)
      | 'g' -> draw_dark <- not draw_dark
      | 'h' ->
        mat_params#switch_light_model;
        print_endline (mat_params#light_model_name);
      | 'H' ->
        mat_params#toggle_specular;
        print_endline (mat_params#light_model_name);
      | ' ' -> draw_colored_cyl <- not draw_colored_cyl
      | _ -> ()
    end;
    if light_radius < 0.2 then
      light_radius <- 0.2;
    framework#post_redisplay
end
let () = Framework.run (new tutorial)
