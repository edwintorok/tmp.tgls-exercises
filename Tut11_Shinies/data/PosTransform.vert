#version 140
#extension GL_ARB_explicit_attrib_location: require

layout(location = 0) in vec3 position;

uniform mat4 modelToCameraMatrix;

uniform Projection
{
	mat4 cameraToClipMatrix;
};

void main()
{
	gl_Position = cameraToClipMatrix * (modelToCameraMatrix * vec4(position, 1.0));
}
