#version 140
#extension GL_ARB_explicit_attrib_location: require

uniform vec4 objectColor;

out vec4 outputColor;

void main()
{
	outputColor = objectColor;
}
